package nh.core.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.function.Function;
import java.util.stream.Stream;
import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ReflectionUtilsTest {

  @ArgumentsSource(LocateAnnotation.class)
  @ParameterizedTest(name = "{2}")
  <C, A extends Annotation, V> void locateAnnotation(
      AnnotationLookUpInput<C, A, V> input,
      Object output,
      String name) {
    // act and assert
    assertThat(ReflectionUtils.locateAnnotation(input.type,
        input.annotationType,
        input.valueExtractor)).isEqualTo(output);
  }

  @ArgumentsSource(ExceptionallyRequireAnnotation.class)
  @ParameterizedTest(name = "{2}")
  <C, A extends Annotation, V> void requireAnnotation(
      AnnotationLookUpInput<C, A, V> input,
      Class<? extends Throwable> exceptionType,
      String name) {
    // act and assert
    assertThatThrownBy(() ->
        ReflectionUtils.requireAnnotation(input.type,
            input.annotationType))
        .isInstanceOf(exceptionType);
  }

  @Test
  @DisplayName("with present annotation, should return the annotation")
  void requiredAnnotation_withPresentedAnnotation_shouldReturnTheAnnotation() {
    // act and assert
    assertThat(ReflectionUtils.requireAnnotation(Animal.class, Named.class))
        .isEqualTo(Animal.class.getDeclaredAnnotation(Named.class));
  }

  private record AnnotationLookUpInput<C, A extends Annotation, V>(
      Class<C> type,
      Class<A> annotationType,
      Function<A, V> valueExtractor) {

  }

  private static class ExceptionallyRequireAnnotation
      implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      return Stream.of(
          Arguments.of(
              new AnnotationLookUpInput<>(null,
                  Named.class,
                  Named::length),
              IllegalArgumentException.class,
              "with null owning type, should throw IllegalArgumentException"),
          Arguments.of(
              new AnnotationLookUpInput<>(Animal.class,
                  null,
                  Named::length),
              IllegalArgumentException.class,
              "with null annotation type, should throw IllegalArgumentException"),
          Arguments.of(
              new AnnotationLookUpInput<>(Animal.class,
                  Behavior.class,
                  Behavior::name),
              IllegalArgumentException.class,
              "with undeclared annotation type, should throw IllegalArgumentException"),
          Arguments.of(
              new AnnotationLookUpInput<>(Animal.class,
                  Food.class,
                  Function.identity()),
              IllegalArgumentException.class,
              "with non-runtime annotation type, should throw IllegalArgumentException")
      );
    }
  }

  private static class LocateAnnotation implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      return Stream.of(
          Arguments.of(
              new AnnotationLookUpInput<>(Animal.class,
                  Named.class,
                  Named::length),
              255,
              "with Named being located from Animal, should return 2"),
          Arguments.of(
              new AnnotationLookUpInput<>(null,
                  Named.class,
                  Named::length),
              null,
              "with null owning type, should return null"),
          Arguments.of(
              new AnnotationLookUpInput<>(Animal.class,
                  null,
                  Named::length),
              null,
              "with null annotation type, should return null"),
          Arguments.of(
              new AnnotationLookUpInput<>(Animal.class,
                  Named.class,
                  Function.identity()),
              Animal.class.getDeclaredAnnotation(Named.class),
              "without transforming the result, should return the annotation"),
          Arguments.of(
              new AnnotationLookUpInput<>(Animal.class,
                  Food.class,
                  Function.identity()),
              null,
              "with non-runtime annotation type, should return null")
      );
    }
  }

  @Retention(RetentionPolicy.RUNTIME)
  @Target(ElementType.TYPE)
  public @interface Named {

    int length() default 255;

  }

  @Retention(RetentionPolicy.RUNTIME)
  @Target(ElementType.TYPE)
  public @interface Behavior {

    String name() default Strings.EMPTY;

  }

  @Retention(RetentionPolicy.CLASS)
  @Target(ElementType.TYPE)
  @SuppressWarnings("unused")
  public @interface Food {

    Class<?> type();

  }

  @Named
  public static class Animal {

  }

}
