/**
 *
 */
package nh.core.util;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * @author Ngoc Huy
 *
 */
class TypeUtilsTest {

  @Test
  @DisplayName("with cat class, get class stack, should return animal, four-leg animal and cat classes orderly")
  void withCatClass_getClassStack_shouldReturnAnimalFourLegCat() {
    // arrange
    final Class<Cat> catClass = Cat.class;

    // act
    final Deque<Class<? super Cat>> actual = TypeUtils.getClassStack(catClass);

    // assert
    Assertions.assertIterableEquals(
        actual, new ArrayDeque<>(List.of(Animal.class, FourLegAnimal.class, Cat.class)));
  }

  @Test
  @DisplayName("with cat class, get class queue, should return cat, four-leg animal and animal classes orderly")
  void withCatClass_getClassQueue_shouldReturnCatFourLegAnimal() {
    // arrange
    final Class<Cat> catClass = Cat.class;
    // act
    final Deque<Class<? super Cat>> actual = TypeUtils.getClassQueue(catClass);
    // assert
    Assertions.assertIterableEquals(
        actual, new ArrayDeque<>(List.of(Cat.class, FourLegAnimal.class, Animal.class)));
  }

  private class Animal {

  }

  private class FourLegAnimal extends Animal {

  }

  private class Cat extends FourLegAnimal {

  }

}
