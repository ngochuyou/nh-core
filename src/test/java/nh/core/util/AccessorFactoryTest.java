package nh.core.util;

import nh.core.reflect.Accessor;
import nh.core.reflect.impl.AccessorFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * @author Ngoc Huy
 */
class AccessorFactoryTest {

  @Test
  @DisplayName("with private fields whose getters and setters present, should be able to get")
  void produce_withPrivateField_shouldBeAbleToGet() {
    // arrange
    final String name = "Cat";
    final Animal animal = new Animal();

    animal.setName(name);

    // act
    final Accessor basicAccessor = AccessorFactory.basic(Animal.class, "name");
    final String reflectedName = (String) basicAccessor.get(animal);

    // assert
    Assertions.assertThat(reflectedName)
        .isEqualTo(name);
  }

  @Test
  @DisplayName("with private fields whose getters and setters present, should be able to set")
  void produce_withPrivateField_shouldBeAbleToSet() {
    // arrange
    final String name = "Cat";
    final Animal animal = new Animal();

    animal.setName(null);

    // act
    final Accessor basicAccessor = AccessorFactory.basic(Animal.class, "name");

    basicAccessor.set(animal, name);

    // assert
    Assertions.assertThat(animal.getName())
        .isEqualTo(name);
  }

  @Test
  @DisplayName("with private fields whose getters are in 'isXXX()' format, should be able to get")
  void produce_withPrivateFieldWhoseGettersAreInIsXXXFormat_shouldBeAbleToGet() {
    // arrange
    final boolean isMammal = true;
    final Animal animal = new Animal();

    animal.setMammal(isMammal);

    // act
    final Accessor basicAccessor = AccessorFactory.basic(Animal.class, "isMammal");
    final boolean actual = (boolean) basicAccessor.get(animal);

    // assert
    Assertions.assertThat(actual)
        .isEqualTo(isMammal);
  }

  @Test
  @DisplayName("with private fields whose getters are in snake-cased 'isXXX()' format, should be able to get")
  void produce_withPrivateFieldWhoseGettersAreInSnakeCasedIsXXXFormat_shouldBeAbleToGet() {
    // arrange
    final boolean isMammal_In_Snake_Case = true;
    final Animal animal = new Animal();

    animal.setMammal_In_Snake_Case(isMammal_In_Snake_Case);

    // act
    final Accessor basicAccessor = AccessorFactory.basic(Animal.class, "isMammal_In_Snake_Case");
    final boolean actual = (boolean) basicAccessor.get(animal);

    // assert
    Assertions.assertThat(actual)
        .isEqualTo(isMammal_In_Snake_Case);
  }

  @Test
  @DisplayName("with private fields in 'isXXX()' format that contains numeric characters, should be able to get")
  void produce_withPrivateFieldInIsXXXFormatThatContainsNumericCharacters_shouldBeAbleToGet() {
    // arrange
    final boolean isMammal212121 = true;
    final Animal animal = new Animal();

    animal.setMammal212121(isMammal212121);

    // act
    final Accessor basicAccessor = AccessorFactory.basic(Animal.class, "isMammal212121");
    final boolean actual = (boolean) basicAccessor.get(animal);

    // assert
    Assertions.assertThat(actual)
        .isEqualTo(isMammal212121);
  }

  @Test
  @DisplayName("with boxed private fields in 'isXXX()' format, should be able to get")
  void produce_withBoxedPrivateFieldInIsXXXFormat_shouldBeAbleToGet() {
    // arrange
    final boolean isBoxedMammal = true;
    final Animal animal = new Animal();

    animal.setBoxedMammal(isBoxedMammal);

    // act
    final Accessor basicAccessor = AccessorFactory.basic(Animal.class, "isBoxedMammal");
    final boolean actual = (boolean) basicAccessor.get(animal);

    // assert
    Assertions.assertThat(actual)
        .isEqualTo(isBoxedMammal);
  }

  @SuppressWarnings("unused")
  public static class Animal {

    public int amountOfLegs;
    private String name;
    private boolean isMammal;
    private boolean isMammal_In_Snake_Case;
    private boolean isMammal212121;
    private Boolean isBoxedMammal;

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public boolean isMammal() {
      return isMammal;
    }

    public void setMammal(boolean isMammal) {
      this.isMammal = isMammal;
    }

    public int getAmountOfLegs() {
      return amountOfLegs;
    }

    public void setAmountOfLegs(int amountOfLegs) {
      this.amountOfLegs = amountOfLegs;
    }

    public boolean isMammal_In_Snake_Case() {
      return isMammal_In_Snake_Case;
    }

    public void setMammal_In_Snake_Case(boolean isMammal_In_Snake_Case) {
      this.isMammal_In_Snake_Case = isMammal_In_Snake_Case;
    }

    public boolean isMammal212121() {
      return isMammal212121;
    }

    public void setMammal212121(boolean isMammal212121) {
      this.isMammal212121 = isMammal212121;
    }

    public Boolean isBoxedMammal() {
      return isBoxedMammal;
    }

    public void setBoxedMammal(Boolean isBoxedMammal) {
      this.isBoxedMammal = isBoxedMammal;
    }

  }

}
