package nh.core.constants;

import java.util.Locale;

public abstract class Locales {

  private Locales() {
    throw new UnsupportedOperationException();
  }

  public static final Locale VI = new Locale("vi_VN");

}
