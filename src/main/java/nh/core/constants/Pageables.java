package nh.core.constants;

import org.springframework.data.domain.Pageable;

public abstract class Pageables {

  private Pageables() {
    throw new UnsupportedOperationException();
  }

  public static final Pageable TEN = Pageable.ofSize(10);
  public static final Pageable ONE = Pageable.ofSize(1);

}
