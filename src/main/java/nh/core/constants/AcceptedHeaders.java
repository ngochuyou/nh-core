package nh.core.constants;

public abstract class AcceptedHeaders {

  private AcceptedHeaders() {
    throw new UnsupportedOperationException();
  }

  public static final String LANG = "accept-lang";

}
