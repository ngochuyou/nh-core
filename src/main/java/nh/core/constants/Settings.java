/**
 *
 */
package nh.core.constants;

/**
 * @author Ngoc Huy
 *
 */
public abstract class Settings {

  public static final String CORE_PACKAGE = "nh.core";

  private Settings() {
    throw new UnsupportedOperationException();
  }

}
