/**
 *
 */
package nh.core.constants;

/**
 * @author Ngoc Huy
 *
 */
public abstract class Configurations {

  public static final String CONTEXT_BASE_PACKAGE = "context.base-package";
  public static final String CONTEXT_BASE_PACKAGE_INJECTION = "${context.base-package}";
  public static final String CONTEXT_ENTITY_PACKAGE = "context.entity-package";
  public static final String REPO_CONTEXT_PREFIX =
      "domain.repository";
  public static final String REPO_CONTEXT_PAGEABLE_PREFIX =
      "domain.repository.pageable";
  public static final String REPO_CONTEXT_LOCK_PREFIX =
      "domain.repository.lock-mode";
  public static final String REDIS_SECURITY_CONTEXT_PREFIX =
      "domain.security.context.redis";
  public static final String SERVICE_ACCOUNT_CONTEXT_PREFIX =
      "domain.security.service.account";

  private Configurations() {
    throw new UnsupportedOperationException();
  }

}
