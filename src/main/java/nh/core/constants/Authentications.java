package nh.core.constants;

import java.util.Collection;
import java.util.List;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public abstract class Authentications {

  private Authentications() {
    throw new UnsupportedOperationException();
  }

  private static final String ANONYMOUS_ID = "ANONYMOUS";
  private static final List<GrantedAuthority> ANONYMOUS_AUTHORITIES = List.of(
      new SimpleGrantedAuthority(
          ANONYMOUS_ID));
  private static final UserDetails ANONYMOUS_PRINCIPAL = new UserDetails() {
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
      return ANONYMOUS_AUTHORITIES;
    }

    @Override
    public String getPassword() {
      return null;
    }

    @Override
    public String getUsername() {
      return ANONYMOUS_ID;
    }

    @Override
    public boolean isAccountNonExpired() {
      return false;
    }

    @Override
    public boolean isAccountNonLocked() {
      return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
      return false;
    }

    @Override
    public boolean isEnabled() {
      return false;
    }
  };

  public static final Authentication ANONYMOUS = new AnonymousAuthenticationToken(
      ANONYMOUS_PRINCIPAL.getUsername(), ANONYMOUS_PRINCIPAL, ANONYMOUS_PRINCIPAL.getAuthorities());
}
