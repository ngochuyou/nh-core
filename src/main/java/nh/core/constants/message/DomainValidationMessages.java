package nh.core.constants.message;

public abstract class DomainValidationMessages {

  private DomainValidationMessages() {
    throw new UnsupportedOperationException();
  }

  public static final String DV001 = "DV001";
  public static final String DV002 = "DV002";

}
