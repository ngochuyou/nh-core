package nh.core.constants.message;

public abstract class DomainAccessMessages {

  private DomainAccessMessages() {
    throw new UnsupportedOperationException();
  }

  public static final String DA001 = "DA001";

}
