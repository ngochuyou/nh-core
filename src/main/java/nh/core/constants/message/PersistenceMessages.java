package nh.core.constants.message;

public abstract class PersistenceMessages {

  private PersistenceMessages() {
    throw new UnsupportedOperationException();
  }

  public static final String PS001 = "PS001";
  public static final String PS002 = "PS002";

}
