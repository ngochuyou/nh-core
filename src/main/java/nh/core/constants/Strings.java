/**
 *
 */
package nh.core.constants;

/**
 * @author Ngoc Huy
 *
 */
public abstract class Strings {

  public static final String NULL = "null";
  public static final String COMMON_JOINER = ", ";
  public static final String SPACE = "\s";
  public static final String COLON = ":";
  public static final String DOT = ".";
  public static final String EMPTY = "";

  private Strings() {
    throw new UnsupportedOperationException();
  }

}
