/**
 *
 */
package nh.core.constants;

import static java.util.Map.entry;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import nh.core.util.CollectionUtils;

/**
 * @author Ngoc Huy
 *
 */
public abstract class Symbols {

  public static final char DOT = '.';
  // @formatter:off
  private static final Map<Character, List<String>> SYMBOL_NAMES = Map.ofEntries(
      entry('L', List.of("alphabetical characters")),
      entry('N', List.of("numeric characters")),
      entry('|', List.of("vertical bar")),
      entry(';', List.of("semicolon")),
      entry(':', List.of("colon")),
      entry(DOT, List.of("period")),
      entry('(', List.of("opening parenthesis")),
      entry(')', List.of("closing parenthesis")),
      entry('\s', List.of("space")),
      entry(',', List.of("comma")),
      entry('-', List.of("hyphen")),
      entry('_', List.of("underscore")),
      entry('"', List.of("quote")),
      entry('\'', List.of("apostrophe")),
      entry('/', List.of("slash")),
      entry('\\', List.of("back slash")),
      entry('!', List.of("exclamation")),
      entry('@', List.of("at sign")),
      entry('#', List.of("numero sign")),
      entry('$', List.of("dollar sign")),
      entry('%', List.of("percent sign")),
      entry('&', List.of("ampersand")),
      entry('*', List.of("asterisk")),
      entry('?', List.of("question mark")),
      entry('[', List.of("opening hard brackets")),
      entry(']', List.of("closing hard brackets")),
      entry('{', List.of("opening brackets")),
      entry('}', List.of("closing brackets")),
      entry('+', List.of("plus sign")),
      entry('=', List.of("equal sign")),
      entry('^', List.of("caret")));
  private static final Map<String, Character> SYMBOL_MAP = SYMBOL_NAMES.entrySet().stream()
      .map(entry -> entry.getValue()
          .stream()
          .map(symbolName -> Map.entry(symbolName, entry.getKey())))
      .flatMap(Function.identity())
      .collect(CollectionUtils.toMap());

  private Symbols() {
    throw new UnsupportedOperationException();
  }

  // @formatter:on
  private static void assertSymbol(Character symbol) {

    if (!SYMBOL_NAMES.containsKey(symbol)) {
      throw new IllegalArgumentException(String.format("Unknown symbol %s", symbol));
    }
  }

  public static Character getSymbol(String symbolName) {

    if (!SYMBOL_MAP.containsKey(symbolName)) {
      throw new IllegalArgumentException(String.format("Unknown symbol name %s", symbolName));
    }

    return SYMBOL_MAP.get(symbolName);
  }

  public static String getName(Character symbol) {
    assertSymbol(symbol);

    return SYMBOL_NAMES.get(symbol).get(0);
  }

  public static String renderName(Character symbol) {
    assertSymbol(symbol);

    return SYMBOL_NAMES.get(symbol).stream().collect(Collectors.joining("or"));
  }

}
