package nh.core.constants;

import org.springframework.data.jpa.domain.Specification;

/**
 * @author Ngoc Huy
 */
public abstract class Hibernates {

  public static final String ROOT_SESSION_FACTORY = "entityManagerFactory";
  @SuppressWarnings("rawtypes")
  public static final Specification ANY = (root, query, builder) -> builder.conjunction();

  private Hibernates() {
    throw new UnsupportedOperationException();
  }

}
