/**
 *
 */
package nh.core.declaration;

import java.util.function.Consumer;
import java.util.function.Function;
import nh.core.function.HandledConsumer;
import nh.core.function.HandledFunction;

/**
 * @author Ngoc Huy
 *
 */
public class DeclarationImpl<FIRST> implements Declaration<FIRST> {

  protected final FIRST first;

  DeclarationImpl(FIRST first) {
    this.first = first;
  }

  @Override
  public FIRST get() {
    return first;
  }

  @Override
  public <RETURN> Declaration<RETURN> map(Function<FIRST, RETURN> mapper) {
    return new DeclarationImpl<>(mapper.apply(first));
  }

  @Override
  public <NEXT_FIRST, NEXT_SECOND> BiDeclaration<NEXT_FIRST, NEXT_SECOND> map(
      Function<FIRST, NEXT_FIRST> firstMapper, Function<FIRST, NEXT_SECOND> secondMapper) {
    return new BiDeclarationImpl<>(firstMapper.apply(first), secondMapper.apply(first));
  }

  @Override
  public Declaration<FIRST> consume(Consumer<FIRST> consumer) {
    consumer.accept(first);
    return this;
  }

  @Override
  public <RETURN, E extends Exception> Declaration<RETURN> tryMap(
      HandledFunction<FIRST, RETURN, E> mapper) throws E {
    return new DeclarationImpl<>(mapper.apply(first));
  }

  @Override
  public <NEXT_FIRST, NEXT_SECOND, E extends Exception, T extends Exception> BiDeclaration<NEXT_FIRST, NEXT_SECOND> tryMap(
      HandledFunction<FIRST, NEXT_FIRST, E> firstMapper,
      HandledFunction<FIRST, NEXT_SECOND, T> secondMapper) throws E, T {
    return new BiDeclarationImpl<>(firstMapper.apply(first), secondMapper.apply(first));
  }

  @Override
  public <E extends Exception> Declaration<FIRST> tryConsume(HandledConsumer<FIRST, E> consumer)
      throws E {
    consumer.accept(first);
    return this;
  }

  @Override
  public <SECOND> BiDeclaration<FIRST, SECOND> second(SECOND second) {
    return new BiDeclarationImpl<>(first, second);
  }

  @Override
  public <SECOND> BiDeclaration<FIRST, SECOND> second(Function<FIRST, SECOND> secondProducer) {
    return new BiDeclarationImpl<>(first, secondProducer.apply(first));
  }

}
