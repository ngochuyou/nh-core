/**
 *
 */
package nh.core.declaration;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import nh.core.function.HandledBiConsumer;
import nh.core.function.HandledBiFunction;

/**
 * @author Ngoc Huy
 *
 */
public class BiDeclarationImpl<FIRST, SECOND> extends DeclarationImpl<FIRST>
    implements BiDeclaration<FIRST, SECOND> {

  protected final SECOND second;

  BiDeclarationImpl(FIRST first, SECOND second) {
    super(first);
    this.second = second;
  }

  @Override
  public <RETURN> Declaration<RETURN> map(BiFunction<FIRST, SECOND, RETURN> mapper) {
    return new DeclarationImpl<>(mapper.apply(first, second));
  }

  @Override
  public BiDeclaration<FIRST, SECOND> consume(BiConsumer<FIRST, SECOND> consumer) {
    consumer.accept(first, second);
    return this;
  }

  @Override
  public <RETURN, E extends Exception> Declaration<RETURN> tryMap(
      HandledBiFunction<FIRST, SECOND, RETURN, E> mapper) throws E {
    return new DeclarationImpl<>(mapper.apply(first, second));
  }

  @Override
  public <E extends Exception> BiDeclaration<FIRST, SECOND> tryConsume(
      HandledBiConsumer<FIRST, SECOND, E> consumer) throws E {
    consumer.accept(first, second);
    return this;
  }

  @Override
  public BiDeclaration<SECOND, FIRST> biInverse() {
    return new BiDeclarationImpl<>(second, first);
  }

  @Override
  public <THIRD> TriDeclaration<FIRST, THIRD, SECOND> insert(THIRD third) {
    return new TriDeclarationImpl<>(first, third, second);
  }

  @Override
  public <THIRD> TriDeclaration<FIRST, SECOND, THIRD> third(THIRD third) {
    return new TriDeclarationImpl<>(first, second, third);
  }

  @Override
  public <THIRD> TriDeclaration<FIRST, SECOND, THIRD> third(
      BiFunction<FIRST, SECOND, THIRD> fnc) {
    return new TriDeclarationImpl<>(first, second, fnc.apply(first, second));
  }

  @Override
  public Declaration<FIRST> useFirst() {
    return new DeclarationImpl<>(first);
  }

  @Override
  public Declaration<SECOND> useSecond() {
    return new DeclarationImpl<>(second);
  }

  @Override
  public SECOND getSecond() {
    return second;
  }

}
