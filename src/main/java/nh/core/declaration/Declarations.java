/**
 *
 */
package nh.core.declaration;

/**
 * @author Ngoc Huy
 *
 */
public interface Declarations {

  public static <FIRST> Declaration<FIRST> of(FIRST first) {
    return new DeclarationImpl<>(first);
  }

  public static <FIRST, SECOND> BiDeclaration<FIRST, SECOND> of(FIRST first, SECOND second) {
    return new BiDeclarationImpl<>(first, second);
  }

  public static <FIRST, SECOND, THIRD> TriDeclaration<FIRST, SECOND, THIRD> of(
      FIRST first,
      SECOND second,
      THIRD third) {
    return new TriDeclarationImpl<>(first, second, third);
  }

}
