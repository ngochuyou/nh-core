/**
 *
 */
package nh.core.declaration;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import nh.core.function.HandledBiConsumer;
import nh.core.function.HandledBiFunction;

/**
 * @author Ngoc Huy
 */
public interface BiDeclaration<FIRST, SECOND> extends Declaration<FIRST> {

  <RETURN> Declaration<RETURN> map(BiFunction<FIRST, SECOND, RETURN> mapper);

  BiDeclaration<FIRST, SECOND> consume(BiConsumer<FIRST, SECOND> consumer);

  <RETURN, E extends Exception> Declaration<RETURN> tryMap(
      HandledBiFunction<FIRST, SECOND, RETURN, E> mapper) throws E;

  <E extends Exception> BiDeclaration<FIRST, SECOND> tryConsume(
      HandledBiConsumer<FIRST, SECOND, E> consumer) throws E;

  BiDeclaration<SECOND, FIRST> biInverse();

  <THIRD> TriDeclaration<FIRST, THIRD, SECOND> insert(THIRD third);

  <THIRD> TriDeclaration<FIRST, SECOND, THIRD> third(THIRD third);

  <THIRD> TriDeclaration<FIRST, SECOND, THIRD> third(BiFunction<FIRST, SECOND, THIRD> fnc);

  Declaration<FIRST> useFirst();

  Declaration<SECOND> useSecond();

  SECOND getSecond();

}
