/**
 *
 */
package nh.core.declaration;

import nh.core.function.HandledTriConsumer;
import nh.core.function.HandledTriFunction;
import nh.core.function.TriConsumer;
import nh.core.function.TriFunction;

/**
 * @author Ngoc Huy
 *
 */
public class TriDeclarationImpl<FIRST, SECOND, THIRD> extends BiDeclarationImpl<FIRST, SECOND>
    implements TriDeclaration<FIRST, SECOND, THIRD> {

  protected final THIRD third;

  TriDeclarationImpl(FIRST first, SECOND second, THIRD third) {
    super(first, second);
    this.third = third;
  }

  @Override
  public <RETURN> Declaration<RETURN> map(TriFunction<FIRST, SECOND, THIRD, RETURN> mapper) {
    return new DeclarationImpl<>(mapper.apply(first, second, third));
  }

  @Override
  public TriDeclaration<FIRST, SECOND, THIRD> consume(TriConsumer<FIRST, SECOND, THIRD> consumer) {
    consumer.accept(first, second, third);
    return this;
  }

  @Override
  public <RETURN, E extends Exception> Declaration<RETURN> tryMap(
      HandledTriFunction<FIRST, SECOND, THIRD, RETURN, E> mapper) throws E {
    return new DeclarationImpl<>(mapper.apply(first, second, third));
  }

  @Override
  public <E extends Exception> TriDeclaration<FIRST, SECOND, THIRD> tryConsume(
      HandledTriConsumer<FIRST, SECOND, THIRD, E> consumer) throws E {
    consumer.accept(first, second, third);
    return this;
  }

  @Override
  public TriDeclaration<THIRD, SECOND, FIRST> triInverse() {
    return new TriDeclarationImpl<>(third, second, first);
  }

  @Override
  public Declaration<THIRD> useThird() {
    return new DeclarationImpl<>(third);
  }

  @Override
  public THIRD getThird() {
    return third;
  }

}
