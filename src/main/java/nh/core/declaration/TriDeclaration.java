/**
 *
 */
package nh.core.declaration;

import nh.core.function.HandledTriConsumer;
import nh.core.function.HandledTriFunction;
import nh.core.function.TriConsumer;
import nh.core.function.TriFunction;

/**
 * @author Ngoc Huy
 *
 */
public interface TriDeclaration<FIRST, SECOND, THIRD> extends BiDeclaration<FIRST, SECOND> {

  <RETURN> Declaration<RETURN> map(TriFunction<FIRST, SECOND, THIRD, RETURN> mapper);

  TriDeclaration<FIRST, SECOND, THIRD> consume(TriConsumer<FIRST, SECOND, THIRD> consumer);

  <RETURN, E extends Exception> Declaration<RETURN> tryMap(
      HandledTriFunction<FIRST, SECOND, THIRD, RETURN, E> mapper) throws E;

  <E extends Exception> TriDeclaration<FIRST, SECOND, THIRD> tryConsume(
      HandledTriConsumer<FIRST, SECOND, THIRD, E> consumer) throws E;

  TriDeclaration<THIRD, SECOND, FIRST> triInverse();

  Declaration<THIRD> useThird();

  THIRD getThird();

}
