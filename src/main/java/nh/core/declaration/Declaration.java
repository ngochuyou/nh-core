/**
 *
 */
package nh.core.declaration;

import java.util.function.Consumer;
import java.util.function.Function;
import nh.core.function.HandledConsumer;
import nh.core.function.HandledFunction;

/**
 * @author Ngoc Huy
 */
public interface Declaration<FIRST> {

  FIRST get();

  <RETURN> Declaration<RETURN> map(Function<FIRST, RETURN> mapper);

  <NEXT_FIRST, NEXT_SECOND> BiDeclaration<NEXT_FIRST, NEXT_SECOND> map(
      Function<FIRST, NEXT_FIRST> firstMapper,
      Function<FIRST, NEXT_SECOND> secondMapper);

  Declaration<FIRST> consume(Consumer<FIRST> consumer);

  <RETURN, E extends Exception> Declaration<RETURN> tryMap(HandledFunction<FIRST, RETURN, E> mapper)
      throws E;

  <NEXT_FIRST, NEXT_SECOND,
      E extends Exception, T extends Exception> BiDeclaration<NEXT_FIRST, NEXT_SECOND> tryMap(
      HandledFunction<FIRST, NEXT_FIRST, E> firstMapper,
      HandledFunction<FIRST, NEXT_SECOND, T> secondMapper) throws E, T;

  <E extends Exception> Declaration<FIRST> tryConsume(HandledConsumer<FIRST, E> consumer) throws E;

  <SECOND> BiDeclaration<FIRST, SECOND> second(SECOND second);

  <SECOND> BiDeclaration<FIRST, SECOND> second(Function<FIRST, SECOND> secondProducer);

}
