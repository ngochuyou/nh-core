package nh.core.exception;

public class UnauthorizedDomainAccessException extends Exception {

  public UnauthorizedDomainAccessException(String message) {
    super(message);
  }
}
