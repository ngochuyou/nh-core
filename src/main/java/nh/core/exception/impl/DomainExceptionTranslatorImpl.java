package nh.core.exception.impl;

import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import nh.core.constants.message.DomainAccessMessages;
import nh.core.constants.message.PersistenceMessages;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.InternationalizedResource;
import nh.core.domain.metadata.InternationalizedAttributesMetadata;
import nh.core.exception.DomainExceptionTranslator;
import nh.core.exception.UnauthorizedDomainAccessException;
import nh.core.execution.ExecutionContext;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.MessageSource;

public class DomainExceptionTranslatorImpl implements DomainExceptionTranslator {

  private final MessageSource messageSource;
  private final BeanFactory beanFactory;

  public DomainExceptionTranslatorImpl(MessageSource messageSource, BeanFactory beanFactory) {
    this.messageSource = messageSource;
    this.beanFactory = beanFactory;
  }

  @Override
  public <D extends DomainResource> EntityExistsException existsFrom(Serializable id,
      Class<D> domainType) {
    return new EntityExistsException(getMessage(PersistenceMessages.PS001, id));
  }

  private String getMessage(String code, Object... args) {
    return messageSource.getMessage(code, args, getLocale());
  }

  private Locale getLocale() {
    return beanFactory.getBean(ExecutionContext.class).getLocale();
  }

  @Override
  public <D extends DomainResource> EntityNotFoundException notFoundFrom(Serializable id,
      Class<D> domainType) {
    return new EntityNotFoundException(getMessage(PersistenceMessages.PS002, id));
  }

  @Override
  public <D extends DomainResource> UnauthorizedDomainAccessException domainAccessFrom(
      Class<D> domainType, List<String> attributes) {
    return new UnauthorizedDomainAccessException(
        messageSource.getMessage(DomainAccessMessages.DA001,
            resolveAttributeNames(domainType, attributes).toArray(Object[]::new),
            getLocale()));
  }

  @SuppressWarnings("unchecked")
  private <D extends DomainResource> List<String> resolveAttributeNames(Class<D> domainType,
      List<String> attributes) {
    if (!InternationalizedResource.class.isAssignableFrom(domainType)) {
      return attributes;
    }

    final Map<String, String> nameMappings = beanFactory.getBean(DomainResourceContext.class)
        .getMetadata(domainType)
        .require(InternationalizedAttributesMetadata.class)
        .getNameMappings();

    return attributes.stream()
        .map(attribute -> Optional.ofNullable(nameMappings.get(attribute)).orElse(attribute))
        .toList();
  }

}
