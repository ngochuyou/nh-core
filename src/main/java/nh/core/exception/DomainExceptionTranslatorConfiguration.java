package nh.core.exception;

import nh.core.exception.impl.DomainExceptionTranslatorImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;

public class DomainExceptionTranslatorConfiguration {

  @Bean
  public DomainExceptionTranslator domainExceptionTranslator(MessageSource messageSource,
      BeanFactory beanFactory) {
    return new DomainExceptionTranslatorImpl(messageSource, beanFactory);
  }

}
