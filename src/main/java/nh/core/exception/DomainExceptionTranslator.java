package nh.core.exception;

import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.util.List;
import nh.core.domain.DomainResource;

public interface DomainExceptionTranslator {

  <D extends DomainResource> EntityExistsException existsFrom(Serializable id, Class<D> domainType);

  <D extends DomainResource> EntityNotFoundException notFoundFrom(Serializable id, Class<D> domainType);

  <D extends DomainResource> UnauthorizedDomainAccessException domainAccessFrom(Class<D> domainType, List<String> attributes);

}
