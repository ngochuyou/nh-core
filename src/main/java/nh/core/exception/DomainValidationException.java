package nh.core.exception;

import java.io.IOException;
import java.io.Serial;
import java.util.Map;
import nh.core.BundledMessage;

public class DomainValidationException extends Exception {

  private final Class<?> resourceType;
  private final Map<String, BundledMessage> violations;

  public DomainValidationException(Class<?> resourceType, Map<String, BundledMessage> violations) {
    this.resourceType = resourceType;
    this.violations = violations;
  }

  public Class<?> getResourceType() {
    return resourceType;
  }

  public Map<String, BundledMessage> getViolations() {
    return violations;
  }

  @Serial
  private void writeObject(java.io.ObjectOutputStream stream)
      throws IOException {
    stream.defaultWriteObject();
  }

  @Serial
  private void readObject(java.io.ObjectInputStream stream)
      throws IOException, ClassNotFoundException {
    stream.defaultReadObject();
  }

}
