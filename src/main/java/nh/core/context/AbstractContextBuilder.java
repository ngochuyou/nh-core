/**
 *
 */
package nh.core.context;

import jakarta.annotation.PostConstruct;

/**
 * @author Ngoc Huy
 *
 */
public abstract class AbstractContextBuilder implements ContextBuilder {

  @PostConstruct
  public void doPostConstruct() {
    this.summary();
  }

}
