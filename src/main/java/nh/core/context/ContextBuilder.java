/**
 *
 */
package nh.core.context;

import nh.core.Loggable;

/**
 * @author Ngoc Huy
 *
 */
public interface ContextBuilder extends Loggable {

  default void summary() {
  }

}
