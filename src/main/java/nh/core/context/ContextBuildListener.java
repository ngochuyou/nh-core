/**
 *
 */
package nh.core.context;

/**
 * @author Ngoc Huy
 *
 */
public interface ContextBuildListener {

  void doAfterContextBuild() throws IllegalAccessException;

}
