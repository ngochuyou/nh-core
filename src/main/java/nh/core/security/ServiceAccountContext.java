package nh.core.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public interface ServiceAccountContext {

  Authentication getAuthentication();

  String getUsername();

  String getPassword();

  GrantedAuthority getGrantedAuthority();

}
