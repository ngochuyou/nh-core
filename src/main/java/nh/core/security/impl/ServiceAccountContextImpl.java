package nh.core.security.impl;

import java.util.List;
import nh.core.constants.Configurations;
import nh.core.security.ServiceAccountContext;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * @author Ngoc Huy
 */
@ConfigurationProperties(Configurations.SERVICE_ACCOUNT_CONTEXT_PREFIX)
public class ServiceAccountContextImpl implements ServiceAccountContext {

  private final String username;
  private final String password;
  private final GrantedAuthority authority;

  private final Authentication authentication;

  public ServiceAccountContextImpl(String username, String password, String authority) {
    this.username = username;
    this.password = password;
    this.authority = new SimpleGrantedAuthority(authority);

    authentication =
        new UsernamePasswordAuthenticationToken(username, password, List.of(this.authority));
  }

  @Override
  public Authentication getAuthentication() {
    return authentication;
  }

  @Override
  public String getUsername() {
    return username;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public GrantedAuthority getGrantedAuthority() {
    return authority;
  }

}
