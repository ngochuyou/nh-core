package nh.core.domain.graph;

import nh.core.Loggable;

public interface GraphLogic<T> extends Loggable {

  /**
   * The capability to combine this one self with another of its type
   *
   * @param next the additional logic
   * @return the combined logics in the form of a single logic
   * @param <E> any
   */
  <E extends T> GraphLogic<E> and(GraphLogic<E> next);

}
