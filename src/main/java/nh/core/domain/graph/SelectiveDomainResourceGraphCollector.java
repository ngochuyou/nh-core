/**
 *
 */
package nh.core.domain.graph;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * @author Ngoc Huy
 */
public interface SelectiveDomainResourceGraphCollector<E, C extends Collection<E>> extends
    DomainResourceGraphCollector<E, C> {

  Predicate<E> getQualifier();

}
