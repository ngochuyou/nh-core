/**
 *
 */
package nh.core.domain.graph;

import java.util.Collection;
import java.util.Deque;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import nh.core.context.ContextBuildListener;
import nh.core.domain.DomainResource;
import nh.core.function.HandledConsumer;

/**
 * @author Ngoc Huy
 *
 */
@SuppressWarnings("squid:S1452")
public interface DomainResourceGraph<T extends DomainResource> extends ContextBuildListener {

  Set<DomainResourceGraph<?>> getParents();

  Class<T> getResourceType();

  Set<DomainResourceGraph<?>> getChildren();

  <E extends T> void add(DomainResourceGraph<E> graph);

  <E extends Exception> void forEach(HandledConsumer<DomainResourceGraph<?>, E> consumer) throws E;

  <E extends T> DomainResourceGraph<E> locate(Class<E> resourceType);

  <E, C extends Collection<E>> C collect(
      Supplier<C> factory,
      Function<DomainResourceGraph<?>, E> mapper);

  <E, C extends Collection<E>> C collect(DomainResourceGraphCollector<E, C> collector);

  <E, C extends Collection<E>> C collectChildren(DomainResourceGraphCollector<E, C> collector);

  <E, C extends Collection<E>> C collectChildren(SelectiveDomainResourceGraphCollector<E, C> collector);

  Deque<Class<? extends DomainResource>> getClassInheritance();

  Deque<DomainResourceGraph<?>> getGraphInheritance();

}
