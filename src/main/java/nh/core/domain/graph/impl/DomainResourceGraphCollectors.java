/**
 *
 */
package nh.core.domain.graph.impl;

import java.lang.reflect.Modifier;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.function.Function;
import nh.core.domain.DomainResource;
import nh.core.domain.graph.DomainResourceGraph;

/**
 * @author Ngoc Huy
 */
public interface DomainResourceGraphCollectors {

  static AbstractCollector<Class<? extends DomainResource>, LinkedHashSet<Class<? extends DomainResource>>> toTypesSet() {
    return new AbstractCollector<>(LinkedHashSet::new, DomainResourceGraph::getResourceType) {
    };
  }

  static AbstractSelectiveCollector<Class<? extends DomainResource>, LinkedHashSet<Class<? extends DomainResource>>> toConcreteTypesSet() {
    return new AbstractSelectiveCollector<>(LinkedHashSet::new, DomainResourceGraph::getResourceType,
        DomainResourceGraphCollectors::isConcreteClass) {
    };
  }

  static boolean isConcreteClass(Class<?> type) {
    final int modifiers = type.getModifiers();

    return !(Modifier.isInterface(modifiers) && Modifier.isAbstract(modifiers));
  }

  static AbstractCollector<Class<? extends DomainResource>, LinkedList<Class<? extends DomainResource>>> toTypesList() {
    return new AbstractCollector<>(LinkedList::new, DomainResourceGraph::getResourceType) {
    };
  }

  @SuppressWarnings("squid:S1452")
  static AbstractCollector<DomainResourceGraph<?>, LinkedHashSet<DomainResourceGraph<?>>> toGraphsSet() {
    return new AbstractCollector<>(LinkedHashSet::new, Function.identity()) {
    };
  }

  @SuppressWarnings("squid:S1452")
  static AbstractCollector<DomainResourceGraph<?>, LinkedList<DomainResourceGraph<?>>> toGraphsList() {
    return new AbstractCollector<>(LinkedList::new, Function.identity()) {
    };
  }

}
