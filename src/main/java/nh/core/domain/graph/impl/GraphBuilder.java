/**
 *
 */
package nh.core.domain.graph.impl;

import static nh.core.declaration.Declarations.of;

import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import nh.core.constants.Settings;
import nh.core.domain.DomainResource;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.util.TypeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.TypeFilter;

@SuppressWarnings({
    "rawtypes", "unchecked"
})
public class GraphBuilder {

  private static final Logger logger = LoggerFactory.getLogger(GraphBuilder.class);

  /**
   * To progressively store the {@link DomainResourceGraph} while building for
   * quickly locate them
   */
  private final Map<Class, DomainResourceGraph> cache = new HashMap<>();
  private final DomainResourceGraph<DomainResource> root =
      new DomainResourceGraphImpl<>(DomainResource.class);

  private final TypeFilter beanIsDomainResource = (metadataReader, metadataReaderFactory) -> {

    try {
      return DomainResource.class
          .isAssignableFrom(Class.forName(metadataReader.getClassMetadata().getClassName()));
    } catch (ClassNotFoundException any) {

      if (logger.isErrorEnabled()) {
        logger.error("Error while scanning for root interfaces: {}", any.getMessage());
      }

      return false;
    }
  };

  public GraphBuilder() {
    cache.put(DomainResource.class, root);
  }

  public DomainResourceGraph<DomainResource> build(String implementationPackage)
      throws ClassNotFoundException, IllegalAccessException {

    logger.trace("Building graphs");

    of(scanForRootInterfaces())
        .tryConsume(this::constructGraphUsingRootInterfaces);

    of(scanForImplementations(implementationPackage))
        .tryConsume(this::addImplementationsToGraph);

    sealGraph();

    return root;
  }

  private void addImplementationsToGraph(Set<BeanDefinition> implementationDefs)
      throws ClassNotFoundException {

    if (logger.isTraceEnabled()) {
      logger.trace("Adding implementations to graph");
    }

    pushToGraph(implementationDefs);
  }

  private Set<BeanDefinition> scanForImplementations(String implementationPackage) {

    if (logger.isDebugEnabled()) {
      logger.debug("Scanning for implementations in package {}", Settings.CORE_PACKAGE);
    }

    final ClassPathScanningCandidateComponentProvider scanner =
        new ClassPathScanningCandidateComponentProvider(false) {

          @Override
          protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
            return true;
          }

        };

    scanner.addIncludeFilter(beanIsDomainResource);

    return scanner.findCandidateComponents(implementationPackage);
  }

  private void putGraph(BeanDefinition beanDef) throws ClassNotFoundException {
    final Class type = from(beanDef);

    if (logger.isTraceEnabled()) {
      logger.trace("New put-graph request of type {}", type.getName());
    }

    final DomainResourceGraph graph = new DomainResourceGraphImpl<>(type);
    final AnnotationMetadata metadata =
        ScannedGenericBeanDefinition.class.cast(beanDef).getMetadata();

    pushClassStack(metadata);
    pushInterfaceStack(metadata);
    putGraph(type, graph);
  }

  private void pushInterfaceStack(AnnotationMetadata metadata) throws ClassNotFoundException {
    final String[] interfaceClassNames = metadata.getInterfaceNames();

    if (interfaceClassNames.length == 0) {
      return;
    }

    for (final String interfaceClassName : interfaceClassNames) {
      pushParentsToGraph(with(interfaceClassName));
    }
  }

  private void pushClassStack(AnnotationMetadata metadata) throws ClassNotFoundException {

    if (logger.isTraceEnabled()) {
      logger.trace("Pushing class stack of type {} to graph", metadata.getClassName());
    }

    if (metadata.getSuperClassName() == null) {

      if (logger.isTraceEnabled()) {
        logger.trace("Class stack is empty for type {}", metadata.getClassName());
      }

      return;
    }

    pushParentsToGraph(with(metadata.getSuperClassName()));
  }

  private void pushParentsToGraph(Class directParentType) {
    final Deque<Class<?>> classStack = TypeUtils.getClassStack(directParentType);

    while (!classStack.isEmpty()) {
      Class<?> currentType = classStack.pop();

      if (!DomainResource.class.isAssignableFrom(currentType) || cache.containsKey(currentType)) {
        continue;
      }

      final DomainResourceGraphImpl parentGraph =
          new DomainResourceGraphImpl<>((Class<? extends DomainResource>) currentType);

      putGraph(currentType, parentGraph);
    }
  }

  private void putGraph(Class type, DomainResourceGraph graph) {

    if (cache.containsKey(type)) {

      if (logger.isTraceEnabled()) {
        logger.trace("{} has already exsited in graph", type.getName());
      }

      return;
    }

    if (logger.isTraceEnabled()) {
      logger.trace("Adding a new graph of type {}", type.getName());
    }

    cache.put(type, graph);
    root.add(graph);
  }

  private void constructGraphUsingRootInterfaces(Set<BeanDefinition> interfacesDefs)
      throws ClassNotFoundException {

    if (logger.isTraceEnabled()) {
      logger.trace("Constructing Graph using root interfaces");
    }

    pushToGraph(interfacesDefs);
  }

  private void pushToGraph(Set<BeanDefinition> beanDefs) throws ClassNotFoundException {

    for (final BeanDefinition beanDef : beanDefs) {
      putGraph(beanDef);
    }
  }

  private Set<BeanDefinition> scanForRootInterfaces() {

    if (logger.isTraceEnabled()) {
      logger.trace("Scanning for root interfaces in package {}", Settings.CORE_PACKAGE);
    }

    final ClassPathScanningCandidateComponentProvider scanner =
        new ClassPathScanningCandidateComponentProvider(false) {

          @Override
          protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
            return beanDefinition.getMetadata().isInterface();
          }

        };

    scanner.addIncludeFilter(beanIsDomainResource);

    return scanner.findCandidateComponents(Settings.CORE_PACKAGE);
  }

  private <T extends DomainResource> Class<T> with(String className) throws ClassNotFoundException {
    return (Class<T>) Class.forName(className);
  }

  private <T extends DomainResource> Class<T> from(BeanDefinition beanDef)
      throws ClassNotFoundException {
    return with(beanDef.getBeanClassName());
  }

  private void sealGraph() throws IllegalAccessException {

    if (logger.isTraceEnabled()) {
      logger.trace("Sealing graph");
    }

    root.forEach(DomainResourceGraph::doAfterContextBuild);
  }

}