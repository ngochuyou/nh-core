/**
 *
 */
package nh.core.domain.graph.impl;

import static org.springframework.util.CollectionUtils.isEmpty;

import java.io.Serial;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import nh.core.domain.DomainResource;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.graph.DomainResourceGraphCollector;
import nh.core.domain.graph.SelectiveDomainResourceGraphCollector;
import nh.core.function.HandledConsumer;
import nh.core.util.TypeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Predicates;

/**
 * @author Ngoc Huy
 *
 */
public class DomainResourceGraphImpl<T extends DomainResource> implements DomainResourceGraph<T> {

  private static final Logger logger = LoggerFactory.getLogger(DomainResourceGraphImpl.class);

  private final Class<T> resourceType;

  private Set<DomainResourceGraph<?>> parents;

  private Set<DomainResourceGraph<?>> children;

  public DomainResourceGraphImpl(final Class<T> resourceType) {
    this.resourceType = resourceType;
    parents = new LinkedHashSet<>(0);
    children = new LinkedHashSet<>(0);
  }

  @SuppressWarnings({
      "unchecked", "rawtypes"
  })
  @Override
  public <E extends T> void add(final DomainResourceGraph<E> newChild) {
    final Class<E> childType = newChild.getResourceType();

    if (resourceType.equals(childType)) {

      if (logger.isWarnEnabled()) {
        logger.warn("Skipping an existing graph");
      }

      return;
    }

    if (resourceType.equals(childType.getSuperclass())
        || TypeUtils.isImplementedFrom(childType, resourceType)) {

      if (logger.isTraceEnabled()) {
        logger.trace(
            "Located parent type {} of entry type {} in graph", resourceType.getName(),
            childType.getName());
      }

      newChild.getParents().add(this);
      children.add(newChild);
      return;
    }

    if (children.isEmpty()) {
      return;
    }

    for (final DomainResourceGraph<?> existingChild : children) {
      existingChild.add((DomainResourceGraph) newChild);
    }
  }

  @SuppressWarnings({
      "unchecked", "rawtypes"
  })
  @Override
  public <E extends T> DomainResourceGraph<E> locate(final Class<E> resourceType) {

    if (this.resourceType.equals(resourceType)) {
      return (DomainResourceGraph<E>) this;
    }

    if (children.isEmpty()) {
      return null;
    }

    DomainResourceGraph target;

    for (final DomainResourceGraph child : children) {
      target = child.locate(resourceType);

      if (target != null) {
        return target;
      }
    }

    return null;
  }

  private <E> Deque<E> getInheritance(final Function<DomainResourceGraph<?>, E> mapper) {
    final Deque<Set<DomainResourceGraph<?>>> cache = getInheritance(new ArrayDeque<>());
    final DistinctInheritanceDeque<E> queue = new DistinctInheritanceDeque<>();

    while (!cache.isEmpty()) {
      for (final DomainResourceGraph<?> graph : cache.pop()) {
        queue.add(mapper.apply(graph));
      }
    }

    return queue;
  }

  @SuppressWarnings({
      "rawtypes", "unchecked"
  })
  private Deque<Set<DomainResourceGraph<?>>> getInheritance(
      final Deque<Set<DomainResourceGraph<?>>> cache) {
    cache.push(Set.of(this));

    if (parents == null) {
      return cache;
    }

    for (final DomainResourceGraph parent : parents) {
      ((DomainResourceGraphImpl) parent).getInheritance(cache);
    }

    return cache;
  }

  @Override
  public Deque<Class<? extends DomainResource>> getClassInheritance() {
    return getInheritance(DomainResourceGraph::getResourceType);
  }

  @Override
  public Deque<DomainResourceGraph<?>> getGraphInheritance() {
    return getInheritance(Function.identity());
  }

  @Override
  public Set<DomainResourceGraph<?>> getParents() {
    return parents;
  }

  @Override
  public Class<T> getResourceType() {
    return resourceType;
  }

  @Override
  public Set<DomainResourceGraph<?>> getChildren() {
    return children;
  }

  @Override
  public <E extends Exception> void forEach(
      final HandledConsumer<DomainResourceGraph<?>, E> consumer)
      throws E {
    consumer.accept(this);

    for (final DomainResourceGraph<?> child : children) {
      child.forEach(consumer);
    }
  }

  @Override
  public <E, C extends Collection<E>> C collect(
      final Supplier<C> factory,
      final Function<DomainResourceGraph<?>, E> mapper) {
    return collect(factory, mapper, false);
  }

  @SuppressWarnings({
      "rawtypes", "unchecked"
  })
  private <E, C extends Collection<E>> C collect(
      final Supplier<C> factory,
      final Function<DomainResourceGraph<?>, E> mapper,
      final boolean bottomUp) {
    final C collection = factory.get();

    if (parents != null) {
      for (final DomainResourceGraph parentGraph : parents) {
        collection.addAll(((DomainResourceGraphImpl) parentGraph).collect(factory, mapper, true));
      }
    }

    collection.add(mapper.apply(this));

    if (!bottomUp) {

      for (final DomainResourceGraph<?> child : children) {
        collection.addAll(child.collect(factory, mapper));
      }
    }

    return collection;
  }

  @Override
  public <E, C extends Collection<E>> C collect(
      final DomainResourceGraphCollector<E, C> collector) {
    return collect(collector.getFactory(), collector.getMapper());
  }

  @Override
  public <E, C extends Collection<E>> C collectChildren(
      DomainResourceGraphCollector<E, C> collector) {
    return collectChildren(collector.getFactory().get(),
        collector.getMapper(),
        Predicates.isTrue());
  }

  @Override
  public <E, C extends Collection<E>> C collectChildren(
      SelectiveDomainResourceGraphCollector<E, C> collector) {
    return collectChildren(collector.getFactory().get(),
        collector.getMapper(),
        collector.getQualifier());
  }

  private <E, C extends Collection<E>> C collectChildren(
      C collection,
      Function<DomainResourceGraph<?>, E> mapper,
      Predicate<E> qualifier) {
    final E candidate = mapper.apply(this);

    if (qualifier.test(candidate)) {
      collection.add(candidate);
    }

    if (isEmpty(children)) {
      return collection;
    }

    for (DomainResourceGraph<?> child : children) {
      ((DomainResourceGraphImpl<?>) child).collectChildren(collection,
          mapper,
          qualifier);
    }

    return collection;
  }

  @Override
  public void doAfterContextBuild() {
    parents = isEmpty(parents)
        ? null
        : Collections.unmodifiableSet(parents);
    children = Collections.unmodifiableSet(children);
  }

  @Override
  public int hashCode() {
    return resourceType.hashCode();
  }

  @SuppressWarnings("rawtypes")
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }

    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }

    final DomainResourceGraphImpl other = (DomainResourceGraphImpl) obj;

    return Objects.equals(resourceType, other.resourceType);
  }

  private static class DistinctInheritanceDeque<E> extends ArrayDeque<E> {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Set<E> set = new HashSet<>();

    @Override
    public boolean contains(final Object o) {
      return set.contains(o);
    }

    @Override
    public boolean containsAll(final Collection<?> c) {
      return set.containsAll(c);
    }

    @Override
    public boolean add(final E e) {

      if (contains(e)) {
        return false;
      }

      set.add(e);
      return super.add(e);
    }

    @Override
    public void push(final E e) {

      if (contains(e)) {
        return;
      }

      set.add(e);
      super.push(e);
    }

  }

}
