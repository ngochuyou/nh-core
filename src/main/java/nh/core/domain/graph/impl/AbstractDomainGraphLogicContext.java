package nh.core.domain.graph.impl;

import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import nh.core.constants.Strings;
import nh.core.context.AbstractContextBuilder;
import nh.core.declaration.Declarations;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.graph.DomainGraphLogicContext;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.graph.GraphLogic;
import nh.core.domain.mapping.DomainMapping;
import nh.core.util.CollectionUtils;
import nh.core.util.DomainMappingUtils;
import nh.core.util.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;

@SuppressWarnings("rawtypes")
public abstract class AbstractDomainGraphLogicContext
    extends AbstractContextBuilder
    implements DomainGraphLogicContext {

  private static final Logger logger = LoggerFactory.getLogger(
      AbstractDomainGraphLogicContext.class);

  private final Map<Class, GraphLogic> logicsMap;

  protected <W extends GraphLogic<?>> AbstractDomainGraphLogicContext(
      Class<W> logicClass,
      AutowireCapableBeanFactory beanFactory,
      DomainResourceContext domainContext,
      String basePackage) {

    logicsMap = Declarations.of(logicClass, basePackage)
        .map(SpringUtils::scan)
        .consume(beanDefinitions -> logScanResult(logicClass, beanDefinitions))
        .second(beanFactory)
        .map(AbstractDomainGraphLogicContext::getMappings)
        .consume(AbstractDomainGraphLogicContext::logMappingResult)
        .second(beanFactory)
        .map(this::addFixedMapping)
        .consume(AbstractDomainGraphLogicContext::logJoinedMappingResult)
        .second(domainContext)
        .map(this::resolveNoOpLogics)
        .consume(AbstractDomainGraphLogicContext::logNoOpMappingResult)
        .second(domainContext)
        .map(AbstractDomainGraphLogicContext::getLogicInheritance)
        .consume(AbstractDomainGraphLogicContext::logLogicInheritances)
        .map(this::unify)
        .consume(AbstractDomainGraphLogicContext::logLogics)
        .map(Collections::unmodifiableMap)
        .get();
  }

  private static void logLogics(Map<Class, GraphLogic> classGraphLogicMap) {
    logger.debug("Unified all logics");
  }

  private Map<Class, GraphLogic> unify(
      Map<Class, LinkedHashSet<DomainMapping>> inheritances) {
    return inheritances.entrySet()
        .stream()
        .collect(Collectors.toMap(
            Map.Entry::getKey,
            mapping -> unifyInheritance(mapping.getValue())));
  }

  @SuppressWarnings("unchecked")
  private GraphLogic unifyInheritance(LinkedHashSet<DomainMapping> inheritance) {
    return inheritance.stream()
        .map(DomainMapping::getInstance)
        .map(GraphLogic.class::cast)
        .filter(this::isNotNoOp)
        .reduce(GraphLogic::and)
        .orElseGet(this::getNoOpLogic);
  }

  private static void logLogicInheritances(
      Map<Class, LinkedHashSet<DomainMapping>> inheritances) {
    if (logger.isDebugEnabled()) {
      logger.debug("\n\t{}", inheritances.entrySet()
          .stream()
          .map(entry -> String.format("%s:\t%s", entry.getKey(),
              getLoggableLogicInheritance(entry.getValue())))
          .collect(Collectors.joining("\n\t")));
    }
  }

  private static String getLoggableLogicInheritance(
      LinkedHashSet<DomainMapping> inheritance) {
    return inheritance.stream()
        .map(DomainMapping::getDomainType)
        .map(Class::getSimpleName)
        .collect(Collectors.joining(Strings.COMMON_JOINER));
  }

  private static Map<Class, LinkedHashSet<DomainMapping>> getLogicInheritance(
      List<DomainMapping> mappings,
      DomainResourceContext domainContext) {
    return Declarations.of(mappings)
        .map(AbstractDomainGraphLogicContext::collectMappingsToMap)
        .second(domainContext)
        .map(AbstractDomainGraphLogicContext::collectInheritance)
        .get();
  }

  @SuppressWarnings("unchecked")
  private static Map<Class, DomainMapping> collectMappingsToMap(List<DomainMapping> mappings) {
    return mappings.stream()
        .map(mapping -> (DomainMapping<DomainResource, GraphLogic>) mapping)
        .collect(Collectors.toMap(DomainMapping::getDomainType, Function.identity()));
  }

  @SuppressWarnings("unchecked")
  private static Map<Class, LinkedHashSet<DomainMapping>> collectInheritance(
      Map<Class, DomainMapping> mappings,
      DomainResourceContext domainContext) {
    final Map<Class, LinkedHashSet<DomainMapping>> product = new HashMap<>();

    /*
     * Construct the logic inheritance once per resource type
     **/
    for (final DomainResourceGraph graph : domainContext.getResourceGraph()
        .collect(DomainResourceGraphCollectors.toGraphsSet())) {
      final Class domainType = graph.getResourceType();
      final Deque<Class> domainInheritance = graph.getClassInheritance();
      // reflect the logics collected to the domainType
      final LinkedHashSet<DomainMapping> mappingSet = new LinkedHashSet<>();
      // now from the top
      while (!domainInheritance.isEmpty()) {
        // make it drop
        final Class currentDomainType = domainInheritance.pop();
        // if the logics for currentDomainType have already been registered,
        // put those into the logic set of domainType
        if (product.containsKey(currentDomainType)) {
          mappingSet.addAll(product.get(currentDomainType));
          continue;
        }
        // else, scope the logics configured from mappings
        product.put(currentDomainType,
            new LinkedHashSet<>(List.of(mappings.get(currentDomainType))));
        // ultimately put the above into the logic set of resourceType as well
        mappingSet.addAll(product.get(currentDomainType));
      }

      product.put(domainType, mappingSet);
    }

    return Collections.unmodifiableMap(product);
  }

  private static void logNoOpMappingResult(List<DomainMapping> mappings) {
    logger.debug("Mapping size with no-ops: {}", mappings.size());
  }

  private static void logJoinedMappingResult(List<DomainMapping> mappings) {
    logger.debug("Mapping size after joining: {}", mappings.size());
  }

  private List<DomainMapping> resolveNoOpLogics(
      List<DomainMapping> mappings,
      DomainResourceContext domainContext) {
    return Declarations.of(
            mappings,
            domainContext.getResourceGraph()
                .collect(DomainResourceGraphCollectors.toTypesSet()))
        .map(this::resolveMissingMappings)
        .second(mappings)
        .map(noOpMappings -> CollectionUtils.join(
            List.of(mappings, noOpMappings),
            Collectors.toList()))
        .get();
  }

  private List<DomainMapping> resolveMissingMappings(
      List<DomainMapping> declaredMappings,
      Set<Class<? extends DomainResource>> domainTypes) {
    final Set<Class> declaredTypes = declaredMappings.stream().map(DomainMapping::getDomainType)
        .collect(Collectors.toSet());

    return domainTypes.stream()
        .filter(domainType -> !declaredTypes.contains(domainType))
        .map(domainType -> DomainMapping.of(domainType, getNoOpLogic()))
        .map(DomainMapping.class::cast)
        .toList();
  }

  private List<DomainMapping> addFixedMapping(
      List<DomainMapping> mappings,
      AutowireCapableBeanFactory beanFactory) {
    return CollectionUtils.join(
        List.of(mappings, getFixedLogics(beanFactory)),
        Collectors.toList());
  }

  private static void logMappingResult(List<DomainMapping> mappings) {
    logger.debug("Resolved {} mappings", mappings.size());
  }

  private static <W extends GraphLogic<?>> void logScanResult(Class<W> logicClass,
      Set<BeanDefinition> beanDefinitions) {
    logger.debug("Found {} definitions of type {}",
        beanDefinitions.size(), logicClass.getSimpleName());
  }

  private static List<DomainMapping> getMappings(Set<BeanDefinition> beanDefinitions,
      AutowireCapableBeanFactory beanFactory) {
    return beanDefinitions.stream()
        .map(beanDef -> DomainMappingUtils.getMapping(beanDef, beanFactory))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .map(DomainMapping.class::cast)
        .filter(mapping -> GraphLogic.class.isAssignableFrom(mapping.getInstance().getClass()))
        .toList();
  }

  protected abstract GraphLogic getNoOpLogic();

  protected abstract boolean isNotNoOp(GraphLogic candidate);

  protected abstract List<DomainMapping> getFixedLogics(AutowireCapableBeanFactory beanFactory);

  protected Optional<GraphLogic> getLogic(Class domainType) {
    return Optional.ofNullable(logicsMap.get(domainType));
  }

  @Override
  public void summary() {
    for (final Entry<Class, GraphLogic> entry : logicsMap.entrySet()) {
      logger.debug("Using {} for {}", entry.getValue().getLoggableName(),
          entry.getKey().getSimpleName());
    }
  }
}
