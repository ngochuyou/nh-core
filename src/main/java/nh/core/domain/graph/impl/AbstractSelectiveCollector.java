/**
 *
 */
package nh.core.domain.graph.impl;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.graph.SelectiveDomainResourceGraphCollector;

/**
 * @author Ngoc Huy
 *
 */
abstract class AbstractSelectiveCollector<E, C extends Collection<E>>
    extends AbstractCollector<E, C> implements SelectiveDomainResourceGraphCollector<E, C> {

  private final Predicate<E> qualifier;

  protected AbstractSelectiveCollector(Supplier<C> factory, Function<DomainResourceGraph<?>, E> mapper,
      Predicate<E> qualifier) {
    super(factory, mapper);
    this.qualifier = qualifier;
  }

  @Override
  public Predicate<E> getQualifier() {
    return qualifier;
  }
}
