/**
 *
 */
package nh.core.domain.graph.impl;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Supplier;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.graph.DomainResourceGraphCollector;

/**
 * @author Ngoc Huy
 *
 */
abstract class AbstractCollector<E, C extends Collection<E>>
    implements DomainResourceGraphCollector<E, C> {

  private final Supplier<C> factory;

  private final Function<DomainResourceGraph<?>, E> mapper;

  protected AbstractCollector(Supplier<C> factory, Function<DomainResourceGraph<?>, E> mapper) {
    this.factory = factory;
    this.mapper = mapper;
  }

  @Override
  public Supplier<C> getFactory() {
    return factory;
  }

  @Override
  public Function<DomainResourceGraph<?>, E> getMapper() {
    return mapper;
  }

}
