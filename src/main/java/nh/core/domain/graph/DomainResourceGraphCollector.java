/**
 *
 */
package nh.core.domain.graph;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Ngoc Huy
 *
 */
public interface DomainResourceGraphCollector<E, C extends Collection<E>> {

  Supplier<C> getFactory();

  Function<DomainResourceGraph<?>, E> getMapper();

}
