package nh.core.domain.security.model;

import java.time.LocalDateTime;

/**
 * @author Ngoc Huy
 */
public class SecuredAttribute {

  private String principal;
  private LocalDateTime timestamp;

  public String getPrincipal() {
    return principal;
  }

  public void setPrincipal(String principal) {
    this.principal = principal;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(LocalDateTime timestamp) {
    this.timestamp = timestamp;
  }

}
