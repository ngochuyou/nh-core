package nh.core.domain.security;

/**
 * @author Ngoc Huy
 */
public interface SecurityEntryManagerListener {

  void informManagerBuilt(SecurityEntryManager entryManager);

}
