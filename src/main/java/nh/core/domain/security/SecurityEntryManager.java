package nh.core.domain.security;

import java.util.List;
import nh.core.domain.DomainResource;
import nh.core.domain.security.persister.SecurityEntryPersister;
import nh.core.exception.UnauthorizedDomainAccessException;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public interface SecurityEntryManager {

  List<SecurityEntryPersister<DomainResource>> getPersisters();

  <D extends DomainResource> void assertAuthority(
      Class<D> domainType,
      List<String> attributes) throws UnauthorizedDomainAccessException;

  <D extends DomainResource> void assertAuthority(
      Class<D> domainType,
      List<String> attributes,
      GrantedAuthority authority) throws UnauthorizedDomainAccessException;

}
