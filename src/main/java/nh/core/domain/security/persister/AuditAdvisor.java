package nh.core.domain.security.persister;

import nh.core.domain.DomainResource;
import nh.core.domain.security.persister.event.EntryAudit;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public interface AuditAdvisor {

  <D extends DomainResource> EntryAudit advise(
      String schema,
      Class<D> resourceType,
      GrantedAuthority authority);

}
