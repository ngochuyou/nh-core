package nh.core.domain.security.persister.event.impl;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import nh.core.domain.DomainResource;
import nh.core.domain.security.persister.event.Event;
import nh.core.domain.security.persister.event.EventListenerGroup;
import nh.core.domain.security.persister.event.PersistEvent;
import nh.core.domain.security.persister.event.PersistEventListener;

/**
 * @author Ngoc Huy
 */
public class EventListenerGroupImpl implements EventListenerGroup {

  private final Map<Class<? extends Event>, List<Consumer<Event>>> eventListenerGroups;

  public EventListenerGroupImpl() {
    eventListenerGroups = Map.of(
        PersistEventImpl.class, getPersistEventListeners());
  }

  @SuppressWarnings({
      "unchecked", "rawtypes"
  })
  private static List<Consumer<Event>> getPersistEventListeners() {
    final PersistEventListener persistEventListener =
        new PersistEventListenerImpl();
    final PersistEventListener persistenceExpirationListener =
        new PersistenceExpirationListener();

    return List.of(
        event -> persistEventListener.inform((PersistEvent) event),
        event -> persistenceExpirationListener.inform((PersistEvent) event));
  }

  private List<Consumer<Event>> getListeners(Event event) {
    final Class<? extends Event> eventClass = event.getClass();

    if (!eventListenerGroups.containsKey(eventClass)) {
      throw new IllegalArgumentException(String.format("Unknown event %s", eventClass));
    }

    return eventListenerGroups.get(eventClass);
  }

  @Override
  public <D extends DomainResource> void informPersist(PersistEvent<D> event) {
    getListeners(event).forEach(listener -> listener.accept(event));
  }

}
