package nh.core.domain.security.persister.impl;

import java.time.temporal.ChronoUnit;
import nh.core.domain.DomainResource;
import nh.core.domain.security.persister.SecurityEntry;
import nh.core.domain.security.persister.event.EntryAudit;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public class SecurityEntryImpl<D extends DomainResource> implements SecurityEntry<D> {

  private final String key;
  private final Class<D> resourceType;
  private final String attribute;
  private final Long ttl;
  private final ChronoUnit timeUnit;
  private final GrantedAuthority authority;
  private final EntryAudit audit;

  public SecurityEntryImpl(
      String key,
      Class<D> resourceType,
      String attribute,
      Long ttl,
      ChronoUnit timeUnit,
      GrantedAuthority authority,
      EntryAudit audit) {
    this.key = key;
    this.resourceType = resourceType;
    this.attribute = attribute;
    this.ttl = ttl;
    this.timeUnit = timeUnit;
    this.authority = authority;
    this.audit = audit;
  }

  @Override
  public String getKey() {
    return key;
  }

  @Override
  public Class<D> getResourceType() {
    return resourceType;
  }

  @Override
  public String getAttribute() {
    return attribute;
  }

  @Override
  public Long getTtl() {
    return ttl;
  }

  @Override
  public ChronoUnit getTimeUnit() {
    return timeUnit;
  }

  @Override
  public GrantedAuthority getAuthority() {
    return authority;
  }

  @Override
  public EntryAudit getAudit() {
    return audit;
  }

}
