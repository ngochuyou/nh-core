package nh.core.domain.security.persister.event;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import nh.core.domain.security.model.SecuredAttribute;

/**
 * @author Ngoc Huy
 */
public interface EventSource {

  SecuredAttribute get(String key);

  List<SecuredAttribute> get(List<String> keys);

  void set(String key, SecuredAttribute value);

  void set(Map<String, SecuredAttribute> pairs);

  void expire(String key, Duration duration);

  void expire(Map<String, Duration> pairs);

}
