package nh.core.domain.security.persister;

import nh.core.domain.DomainResource;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public interface KeyRenderer {

  <D extends DomainResource> String render(
      String schema,
      Class<D> resourceType,
      String attribute,
      GrantedAuthority authority);

}
