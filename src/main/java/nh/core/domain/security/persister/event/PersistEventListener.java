package nh.core.domain.security.persister.event;

import nh.core.domain.DomainResource;

/**
 * @author Ngoc Huy
 */
public interface PersistEventListener extends EventListener {

  <D extends DomainResource> void inform(PersistEvent<D> event);

}
