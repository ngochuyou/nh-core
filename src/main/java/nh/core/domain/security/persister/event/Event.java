package nh.core.domain.security.persister.event;

/**
 * @author Ngoc Huy
 */
public interface Event {

  EventSource getEventSource();

}
