package nh.core.domain.security.persister;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import nh.core.declaration.BiDeclaration;
import nh.core.domain.DomainResource;
import nh.core.domain.security.model.SecuredAttribute;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public interface SecurityEntryPersister<D extends DomainResource> {

  Class<D> getResourceType();

  void persist(GrantedAuthority authority);

  void persist(Long ttl, ChronoUnit unit, GrantedAuthority authority);

  void persist(String attribute, GrantedAuthority authority);

  void persist(
      String attribute,
      Long ttl,
      ChronoUnit unit,
      GrantedAuthority authority);

  void persist(List<String> attributes, GrantedAuthority authority);

  void persist(
      List<String> attribute,
      Long ttl,
      ChronoUnit unit,
      GrantedAuthority authority);

  BiDeclaration<String, SecuredAttribute> find(String attribute, GrantedAuthority authority);

  <R> R find(
      String attribute,
      GrantedAuthority authority,
      BiFunction<String, SecuredAttribute, R> keyValueTransformer);

  List<BiDeclaration<String, SecuredAttribute>> find(GrantedAuthority authority);

  <R> List<R> find(
      GrantedAuthority authority,
      Function<BiDeclaration<String, SecuredAttribute>, R> keyValuePairsTransformer);

  List<BiDeclaration<String, SecuredAttribute>> find(
      List<String> attributes,
      GrantedAuthority authority);

  <R> List<R> find(
      List<String> attributes,
      GrantedAuthority authority,
      Function<BiDeclaration<String, SecuredAttribute>, R> keyValuePairsTransformer);

}
