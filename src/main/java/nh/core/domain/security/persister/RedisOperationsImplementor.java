package nh.core.domain.security.persister;

import nh.core.domain.security.model.SecuredAttribute;
import org.springframework.data.redis.core.RedisOperations;

/**
 * @author Ngoc Huy
 */
public interface RedisOperationsImplementor
    extends RedisOperations<String, SecuredAttribute> {

}
