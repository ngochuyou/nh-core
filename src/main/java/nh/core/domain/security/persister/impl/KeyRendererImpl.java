package nh.core.domain.security.persister.impl;

import nh.core.constants.Strings;
import nh.core.domain.DomainResource;
import nh.core.domain.security.persister.KeyRenderer;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public class KeyRendererImpl implements KeyRenderer {

  @Override
  public <D extends DomainResource> String render(
      String schema,
      Class<D> resourceType,
      String attribute,
      GrantedAuthority authority) {
    return String.join(Strings.COLON, schema, resourceType.getName(), attribute,
        authority.getAuthority());
  }

}
