package nh.core.domain.security.persister.impl;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;
import nh.core.declaration.BiDeclaration;
import nh.core.declaration.Declarations;
import nh.core.domain.DomainResource;
import nh.core.domain.metadata.DomainResourceAttributesMetadata;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.domain.security.impl.RedisSecurityContextConfigurationProperties;
import nh.core.domain.security.model.SecuredAttribute;
import nh.core.domain.security.persister.AuditAdvisor;
import nh.core.domain.security.persister.KeyRenderer;
import nh.core.domain.security.persister.RedisOperationsImplementor;
import nh.core.domain.security.persister.SecurityEntry;
import nh.core.domain.security.persister.SecurityEntryPersister;
import nh.core.domain.security.persister.event.EntryAudit;
import nh.core.domain.security.persister.event.EventListenerGroup;
import nh.core.domain.security.persister.event.EventSource;
import nh.core.domain.security.persister.event.impl.PersistEventImpl;
import nh.core.util.RedisUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public class SecurityEntryPersisterImpl<D extends DomainResource>
    implements SecurityEntryPersister<D>, EventSource {

  private static final Logger logger = LoggerFactory.getLogger(SecurityEntryPersisterImpl.class);

  private final RedisOperationsImplementor redisOperations;

  private final Class<D> resourceType;
  private final String schema;
  private final DomainResourceAttributesMetadata<D> attributesMetadata;
  private final KeyRenderer keyRenderer;

  private final AuditAdvisor auditAdvisor;
  private final EventListenerGroup eventListenerGroup;

  @SuppressWarnings("unchecked")
  public SecurityEntryPersisterImpl(
      RedisSecurityContextConfigurationProperties configurationProperties,
      DomainResourceMetadata<D> metadata,
      RedisOperationsImplementor redisTemplate,
      KeyRenderer keyRenderer,
      EventListenerGroup eventListenerGroup,
      AuditAdvisor auditAdvisor) {
    this.resourceType = metadata.getResourceType();

    logger.debug(
        "Creating an instance of {}. Resource type {}", this.getClass().getSimpleName(),
        resourceType);

    this.redisOperations = redisTemplate;
    attributesMetadata = metadata.require(DomainResourceAttributesMetadata.class);
    schema = configurationProperties.getSchema();
    this.keyRenderer = keyRenderer;
    this.auditAdvisor = auditAdvisor;
    this.eventListenerGroup = eventListenerGroup;
  }

  private static List<BiDeclaration<String, SecuredAttribute>> pairUp(
      List<String> attributes,
      List<SecuredAttribute> values) {
    return IntStream.range(0, values.size())
        .mapToObj(i -> Declarations.of(attributes.get(i), values.get(i)))
        .toList();
  }

  @Override
  public Class<D> getResourceType() {
    return resourceType;
  }

  @Override
  public void persist(GrantedAuthority authority) {
    persist(null, null, authority);
  }

  @Override
  public void persist(
      Long ttl,
      ChronoUnit unit,
      GrantedAuthority authority) {
    persist(
        attributesMetadata.getAttributeNames(),
        ttl,
        unit,
        authority);
  }

  @Override
  public void persist(String attribute, GrantedAuthority authority) {
    persist(attribute, null, null, authority);
  }

  @Override
  public void persist(
      String attribute,
      Long ttl,
      ChronoUnit unit,
      GrantedAuthority authority) {
    persist(List.of(attribute), ttl, unit, authority);
  }

  @Override
  public void persist(List<String> attributes, GrantedAuthority authority) {
    persist(attributes, null, null, authority);
  }

  @Override
  public void persist(
      List<String> attributes,
      Long ttl,
      ChronoUnit unit,
      GrantedAuthority authority) {
    eventListenerGroup.informPersist(createEvent(attributes, ttl, unit, authority));
  }

  private PersistEventImpl<D> createEvent(
      List<String> attributes,
      Long ttl,
      ChronoUnit unit,
      GrantedAuthority authority) {
    final EntryAudit audit = auditAdvisor.advise(schema, resourceType, authority);

    return new PersistEventImpl<>(
        this,
        attributes.stream()
            .map(
                attribute -> new SecurityEntryImpl<>(
                    keyRenderer.render(schema, resourceType, attribute, authority),
                    resourceType,
                    attribute,
                    ttl,
                    unit,
                    authority,
                    audit))
            .map(entry -> (SecurityEntry<D>) entry)
            .toList());
  }

  @Override
  public BiDeclaration<String, SecuredAttribute> find(
      String attribute,
      GrantedAuthority authority) {
    return find(attribute, authority, null);
  }

  @Override
  public <R> R find(
      String attribute,
      GrantedAuthority authority,
      BiFunction<String, SecuredAttribute, R> keyValueTransformer) {
    final List<R> results = find(
        List.of(attribute),
        authority,
        biDeclaration -> keyValueTransformer.apply(
            biDeclaration.get(),
            biDeclaration.getSecond()));

    return results.get(0);
  }

  @Override
  public List<BiDeclaration<String, SecuredAttribute>> find(GrantedAuthority authority) {
    return find(authority, Function.identity());
  }

  @Override
  public <R> List<R> find(
      GrantedAuthority authority,
      Function<BiDeclaration<String, SecuredAttribute>, R> keyValueTransformer) {
    return find(attributesMetadata.getAttributeNames(), authority, keyValueTransformer);
  }

  @Override
  public List<BiDeclaration<String, SecuredAttribute>> find(
      List<String> attributes,
      GrantedAuthority authority) {
    return find(attributes, authority, Function.identity());
  }

  @Override
  public <R> List<R> find(
      List<String> attributes,
      GrantedAuthority authority,
      Function<BiDeclaration<String, SecuredAttribute>, R> keyValueTransformer) {
    return Declarations.of(
            redisOperations,
            attributes
                .stream()
                .map(
                    attribute -> keyRenderer
                        .render(schema, resourceType, attribute, authority))
                .toList())
        .map(RedisUtils::multiHGet)
        .map(values -> pairUp(attributes, values))
        .get()
        .stream()
        .map(keyValueTransformer)
        .toList();
  }

  @Override
  public void expire(String key, Duration duration) {
    redisOperations.expire(key, duration);
  }

  @Override
  public void expire(Map<String, Duration> pairs) {
    RedisUtils.bulkExpire(redisOperations, pairs);
  }

  @Override
  public void set(String key, SecuredAttribute value) {
    redisOperations.opsForValue().set(key, value);
  }

  @Override
  public void set(Map<String, SecuredAttribute> pairs) {
    RedisUtils.multiHSet(redisOperations, pairs);
  }

  @Override
  public SecuredAttribute get(String key) {
    return redisOperations.opsForValue().get(key);
  }

  @Override
  public List<SecuredAttribute> get(List<String> keys) {
    return RedisUtils.multiHGet(redisOperations, keys);
  }

}
