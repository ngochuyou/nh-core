package nh.core.domain.security.persister;

import java.time.temporal.ChronoUnit;
import nh.core.domain.DomainResource;
import nh.core.domain.security.persister.event.EntryAudit;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public interface SecurityEntry<D extends DomainResource> {

  String getKey();

  Class<D> getResourceType();

  String getAttribute();

  Long getTtl();

  ChronoUnit getTimeUnit();

  GrantedAuthority getAuthority();

  EntryAudit getAudit();

}
