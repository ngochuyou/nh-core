package nh.core.domain.security.persister.event.impl;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import nh.core.domain.DomainResource;
import nh.core.domain.security.persister.SecurityEntry;
import nh.core.domain.security.persister.event.EventSource;
import nh.core.domain.security.persister.event.PersistEvent;
import nh.core.domain.security.persister.event.PersistEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

/**
 * @author Ngoc Huy
 */
public class PersistenceExpirationListener implements PersistEventListener {

  private static final Logger logger = LoggerFactory.getLogger(PersistenceExpirationListener.class);

  @Override
  public <D extends DomainResource> void inform(PersistEvent<D> event) {
    final Map<String, Duration> pairs = makePairs(event.getEntries());

    if (CollectionUtils.isEmpty(pairs)) {
      return;
    }

    log(pairs);

    final EventSource eventSource = event.getEventSource();

    if (pairs.size() > 1) {
      eventSource.expire(pairs);
      return;
    }

    final Entry<String, Duration> entry = pairs.entrySet().stream().findFirst().orElseThrow();

    eventSource.expire(entry.getKey(), entry.getValue());
  }

  private void log(Map<String, Duration> pairs) {

    if (logger.isDebugEnabled()) {
      pairs.forEach((key, value) -> logger.debug(
          "Setting an expiration time of {} to entry {}", value,
          key));
    }
  }

  private <D extends DomainResource> Map<String, Duration> makePairs(
      List<SecurityEntry<D>> entries) {
    return entries
        .stream()
        .filter(entry -> Objects.nonNull(entry.getTtl()))
        .map(entry -> Map.entry(entry.getKey(), Duration.of(entry.getTtl(), entry.getTimeUnit())))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

}
