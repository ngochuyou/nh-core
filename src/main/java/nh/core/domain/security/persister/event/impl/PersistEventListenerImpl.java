package nh.core.domain.security.persister.event.impl;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import nh.core.declaration.BiDeclaration;
import nh.core.declaration.Declarations;
import nh.core.domain.DomainResource;
import nh.core.domain.security.model.SecuredAttribute;
import nh.core.domain.security.persister.SecurityEntry;
import nh.core.domain.security.persister.event.EntryAudit;
import nh.core.domain.security.persister.event.PersistEvent;
import nh.core.domain.security.persister.event.PersistEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

/**
 * @author Ngoc Huy
 */
public class PersistEventListenerImpl implements PersistEventListener {

  private static final Logger logger = LoggerFactory.getLogger(PersistEventListenerImpl.class);

  @Override
  public <D extends DomainResource> void inform(PersistEvent<D> event) {
    final Map<String, SecuredAttribute> pairs = makePairs(event.getEntries());

    if (CollectionUtils.isEmpty(pairs)) {
      return;
    }

    log(pairs);

    if (pairs.size() > 1) {
      event.getEventSource().set(pairs);
      return;
    }

    final Entry<String, SecuredAttribute> entry =
        pairs.entrySet().stream().findFirst().orElseThrow();

    event.getEventSource().set(entry.getKey(), entry.getValue());
  }

  private <D extends DomainResource> Map<String, SecuredAttribute> makePairs(
      List<SecurityEntry<D>> entries) {
    return entries.stream()
        .map(entry -> Declarations.of(entry.getKey(), createModel(entry.getAudit())))
        .collect(Collectors.toMap(BiDeclaration::get, BiDeclaration::getSecond));
  }

  private SecuredAttribute createModel(EntryAudit audit) {
    final SecuredAttribute securedAttribute = new SecuredAttribute();

    securedAttribute.setPrincipal(audit.getId());
    securedAttribute.setTimestamp(audit.getTimestamp());

    return securedAttribute;
  }

  private void log(Map<String, SecuredAttribute> pairs) {

    if (logger.isDebugEnabled()) {
      pairs.forEach((key, value) -> logger.debug(
          "Persisting {} by {} at {}", key, value.getPrincipal(),
          value.getTimestamp()));
    }
  }

}
