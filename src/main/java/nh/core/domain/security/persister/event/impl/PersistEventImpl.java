package nh.core.domain.security.persister.event.impl;

import java.util.List;
import nh.core.domain.DomainResource;
import nh.core.domain.security.persister.SecurityEntry;
import nh.core.domain.security.persister.event.EventSource;
import nh.core.domain.security.persister.event.PersistEvent;

/**
 * @author Ngoc Huy
 */
public class PersistEventImpl<D extends DomainResource>
    extends AbstractEvent
    implements PersistEvent<D> {

  private final List<SecurityEntry<D>> entries;

  public PersistEventImpl(EventSource eventSource, List<SecurityEntry<D>> entries) {
    super(eventSource);
    this.entries = entries;
  }

  @Override
  public List<SecurityEntry<D>> getEntries() {
    return entries;
  }

}
