package nh.core.domain.security.persister.event;

import java.util.List;
import nh.core.domain.DomainResource;
import nh.core.domain.security.persister.SecurityEntry;

/**
 * @author Ngoc Huy
 */
public interface PersistEvent<D extends DomainResource> extends Event {

  List<SecurityEntry<D>> getEntries();

}
