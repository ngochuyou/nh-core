package nh.core.domain.security.persister.event;

import nh.core.domain.DomainResource;

/**
 * @author Ngoc Huy
 */
public interface EventListenerGroup {

  <D extends DomainResource> void informPersist(PersistEvent<D> event);

}
