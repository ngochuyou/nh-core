package nh.core.domain.security.persister.event.impl;

import nh.core.domain.security.persister.event.Event;
import nh.core.domain.security.persister.event.EventSource;

/**
 * @author Ngoc Huy
 */
public abstract class AbstractEvent implements Event {

  private final EventSource eventSource;

  protected AbstractEvent(EventSource eventSource) {
    this.eventSource = eventSource;
  }

  @Override
  public EventSource getEventSource() {
    return eventSource;
  }

}
