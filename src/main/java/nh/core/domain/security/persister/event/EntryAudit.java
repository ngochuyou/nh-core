package nh.core.domain.security.persister.event;

import java.time.LocalDateTime;

/**
 * @author Ngoc Huy
 */
public interface EntryAudit {

  String getId();

  LocalDateTime getTimestamp();

}
