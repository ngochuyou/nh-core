package nh.core.domain.security.impl;

import nh.core.domain.DomainResource;
import nh.core.domain.security.SecurityEntryManager;
import nh.core.domain.security.SecurityEntryManagerListener;
import nh.core.domain.security.persister.SecurityEntryPersister;
import nh.core.security.ServiceAccountContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author Ngoc Huy
 */
public class DefaultSecurityEntryManagerListener implements SecurityEntryManagerListener {

  private static final Logger logger =
      LoggerFactory.getLogger(DefaultSecurityEntryManagerListener.class);

  private final ServiceAccountContext serviceAccountContext;

  public DefaultSecurityEntryManagerListener(
      ServiceAccountContext serviceAccountContext) {
    this.serviceAccountContext = serviceAccountContext;
  }

  @Override
  public void informManagerBuilt(SecurityEntryManager manager) {
    logger.debug("Persisting default attributes");

    SecurityContextHolder.getContext()
        .setAuthentication(serviceAccountContext.getAuthentication());

    for (final SecurityEntryPersister<DomainResource> persister : manager.getPersisters()) {
      persister.persist(serviceAccountContext.getGrantedAuthority());
    }

    SecurityContextHolder.clearContext();
  }

}
