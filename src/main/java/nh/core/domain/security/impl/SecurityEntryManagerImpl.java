package nh.core.domain.security.impl;

import static nh.core.util.SpringUtils.locate;

import java.lang.reflect.Modifier;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import nh.core.constants.Authentications;
import nh.core.declaration.BiDeclaration;
import nh.core.declaration.Declarations;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.graph.impl.DomainResourceGraphCollectors;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.domain.security.SecurityEntryManager;
import nh.core.domain.security.persister.AuditAdvisor;
import nh.core.domain.security.persister.KeyRenderer;
import nh.core.domain.security.persister.RedisOperationsImplementor;
import nh.core.domain.security.persister.SecurityEntryPersister;
import nh.core.domain.security.persister.event.EntryAudit;
import nh.core.domain.security.persister.event.EventListenerGroup;
import nh.core.domain.security.persister.event.impl.EventListenerGroupImpl;
import nh.core.domain.security.persister.impl.SecurityEntryPersisterImpl;
import nh.core.exception.DomainExceptionTranslator;
import nh.core.exception.UnauthorizedDomainAccessException;
import nh.core.execution.ExecutionContext;
import nh.core.util.SpringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;

/**
 * @author Ngoc Huy
 */
public class SecurityEntryManagerImpl implements SecurityEntryManager {

  private final Map<Class<? extends DomainResource>, SecurityEntryPersister<DomainResource>> persistersMap;

  private final ExecutionContext executionContext;
  private final DomainExceptionTranslator exceptionTranslator;

  public SecurityEntryManagerImpl(
      ApplicationContext applicationContext,
      DomainResourceContext domainResourceContext,
      ExecutionContext executionContext,
      DomainExceptionTranslator exceptionTranslator)
      throws ClassNotFoundException {
    this.exceptionTranslator = exceptionTranslator;
    persistersMap = Declarations.of(domainResourceContext)
        .second(SecurityEntryManagerImpl::collect)
        .third(applicationContext)
        .tryMap(SecurityEntryManagerImpl::construct)
        .map(SecurityEntryManagerImpl::map)
        .get();
    this.executionContext = executionContext;
  }

  private static List<Class<? extends DomainResource>> collect(
      DomainResourceContext domainResourceContext) {
    return new ArrayList<>(
        domainResourceContext
            .getResourceGraph().collect(DomainResourceGraphCollectors.toTypesSet()))
        .stream()
        .filter(type -> !Modifier.isAbstract(type.getModifiers()))
        .toList();
  }

  @SuppressWarnings({
      "squid:S6204", "unchecked"
  })
  private static List<SecurityEntryPersister<DomainResource>> construct(
      DomainResourceContext domainResourceContext,
      List<Class<? extends DomainResource>> resourceTypes,
      ApplicationContext applicationContext)
      throws BeansException, IllegalStateException, ClassNotFoundException {
    final RedisSecurityContextConfigurationProperties configurationProperties =
        locate(applicationContext, RedisSecurityContextConfigurationProperties.class);
    final RedisOperationsImplementor redisTemplate =
        locate(applicationContext, RedisOperationsImplementor.class);
    final KeyRenderer keyRenderer = locate(applicationContext, KeyRenderer.class);
    final EventListenerGroup eventListenerGroup = SpringUtils
        .instantiate(SpringUtils.createBeanDef(EventListenerGroupImpl.class),
            applicationContext.getAutowireCapableBeanFactory());
    final AuditAdvisor auditAdvisor = new AuditAdvisorImpl();

    return resourceTypes.stream().map(
            resourceType -> new SecurityEntryPersisterImpl<>(
                configurationProperties,
                (DomainResourceMetadata<DomainResource>) domainResourceContext
                    .getMetadata(resourceType),
                redisTemplate,
                keyRenderer,
                eventListenerGroup,
                auditAdvisor))
        .collect(Collectors.toUnmodifiableList());
  }

  private static Map<Class<? extends DomainResource>, SecurityEntryPersister<DomainResource>> map(
      List<SecurityEntryPersister<DomainResource>> persisters) {
    return Collections.unmodifiableMap(
        persisters.stream()
            .collect(
                Collectors.toMap(SecurityEntryPersister::getResourceType, Function.identity())));
  }

  @Override
  public List<SecurityEntryPersister<DomainResource>> getPersisters() {
    return new ArrayList<>(persistersMap.values());
  }

  @Override
  public <D extends DomainResource> void assertAuthority(Class<D> domainType,
      List<String> attributes) throws UnauthorizedDomainAccessException {
    assertAuthority(domainType, attributes, executionContext.getAuthorities().get(0));
  }

  @Override
  public <D extends DomainResource> void assertAuthority(Class<D> domainType,
      List<String> attributes, GrantedAuthority authority)
      throws UnauthorizedDomainAccessException {
    final SecurityEntryPersister<D> persister = requirePersister(domainType);
    final List<String> unauthorizedAttributes = filterUnauthorizedAttributes(attributes, authority,
        persister);

    if (!CollectionUtils.isEmpty(unauthorizedAttributes)) {
      throw exceptionTranslator.domainAccessFrom(domainType, attributes);
    }
  }

  private static <D extends DomainResource> List<String> filterUnauthorizedAttributes(
      List<String> requestedAttributes, GrantedAuthority authority,
      SecurityEntryPersister<D> persister) {
    final Set<String> authorizedAttributes = new HashSet<>(
        persister.find(requestedAttributes, authority,
            BiDeclaration::get));

    return requestedAttributes.stream()
        .filter(attribute -> !authorizedAttributes.contains(attribute))
        .toList();
  }

  @SuppressWarnings("unchecked")
  private <D extends DomainResource> SecurityEntryPersister<D> requirePersister(
      Class<D> domainType) {
    return (SecurityEntryPersister<D>) Optional.ofNullable(persistersMap.get(domainType))
        .orElseThrow(() -> new IllegalStateException(
            String.format("Unable to locate %s for type %s",
                SecurityEntryPersister.class.getSimpleName(), domainType)));
  }

  private static class AuditAdvisorImpl implements AuditAdvisor {

    @Override
    public <D extends DomainResource> EntryAudit advise(
        String schema,
        Class<D> resourceType,
        GrantedAuthority authority) {
      return new EntryAuditImpl();
    }

    private static class EntryAuditImpl implements EntryAudit {

      private final String id;
      private final LocalDateTime timestamp;

      public EntryAuditImpl() {
        timestamp = LocalDateTime.now();

        final Authentication authentication =
            SecurityContextHolder.getContext().getAuthentication();

        if (Objects.isNull(authentication)
            || authentication instanceof AnonymousAuthenticationToken) {
          id = Authentications.ANONYMOUS.getName();
          return;
        }

        id = authentication.getName();
      }

      @Override
      public String getId() {
        return id;
      }

      @Override
      public LocalDateTime getTimestamp() {
        return timestamp;
      }

    }

  }

}
