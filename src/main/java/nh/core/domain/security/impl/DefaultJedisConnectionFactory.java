package nh.core.domain.security.impl;

import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

/**
 * @author Ngoc Huy
 */
public class DefaultJedisConnectionFactory extends JedisConnectionFactory {

  public DefaultJedisConnectionFactory(RedisSecurityContextConfigurationProperties redisConfig) {
    super(new RedisStandaloneConfiguration(redisConfig.getHost(), redisConfig.getPort()));
  }

}
