package nh.core.domain.security.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import nh.core.domain.security.model.SecuredAttribute;
import nh.core.domain.security.persister.RedisOperationsImplementor;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author Ngoc Huy
 */
public class DefaultRedisTemplate
    extends RedisTemplate<String, SecuredAttribute>
    implements RedisOperationsImplementor {

  public DefaultRedisTemplate(
      RedisConnectionFactory connectionFactory,
      ObjectMapper mapper) {
    super();

    super.setConnectionFactory(connectionFactory);

    final RedisSerializer<String> stringSerializer = new StringRedisSerializer();

    super.setKeySerializer(stringSerializer);
    super.setValueSerializer(stringSerializer);

    final RedisSerializer<SecuredAttribute> jsonSerializer =
        new Jackson2JsonRedisSerializer<>(mapper, SecuredAttribute.class);

    super.setHashKeySerializer(stringSerializer);
    super.setHashValueSerializer(jsonSerializer);

    super.setEnableTransactionSupport(true);
    super.afterPropertiesSet();
  }

}
