package nh.core.domain.security.impl;

import nh.core.constants.Configurations;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Ngoc Huy
 */
@ConfigurationProperties(Configurations.REDIS_SECURITY_CONTEXT_PREFIX)
public class RedisSecurityContextConfigurationProperties {

  private final String host;
  private final int port;
  private final String schema;
  private final String packageToScan;
  private final int batchSize;

  public RedisSecurityContextConfigurationProperties(
      String host,
      int port,
      String schema,
      String packageToScan,
      int batchSize) {
    this.host = host;
    this.port = port;
    this.schema = schema;
    this.packageToScan = packageToScan;
    this.batchSize = batchSize;
  }

  public String getHost() {
    return host;
  }

  public int getPort() {
    return port;
  }

  public String getSchema() {
    return schema;
  }

  public String getPackageToScan() {
    return packageToScan;
  }

  public int getBatchSize() {
    return batchSize;
  }

}
