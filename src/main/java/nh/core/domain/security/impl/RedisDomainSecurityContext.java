package nh.core.domain.security.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import nh.core.constants.Settings;
import nh.core.declaration.Declarations;
import nh.core.domain.security.SecurityEntryManager;
import nh.core.domain.security.SecurityEntryManagerListener;
import nh.core.domain.security.DomainSecurityContext;
import nh.core.domain.security.persister.RedisOperationsImplementor;
import nh.core.domain.security.persister.event.impl.EventListenerGroupImpl;
import nh.core.domain.security.persister.impl.KeyRendererImpl;
import nh.core.util.RedisUtils;
import nh.core.util.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;

/**
 * @author Ngoc Huy
 */
public class RedisDomainSecurityContext implements DomainSecurityContext, DisposableBean {

  private static final Logger logger =
      LoggerFactory.getLogger(RedisDomainSecurityContext.class);

  private final ApplicationContext applicationContext;

  public RedisDomainSecurityContext(
      ApplicationContext applicationContext,
      RedisSecurityContextConfigurationProperties redisSecurityContextConfigurationProperties)
      throws ClassNotFoundException {
    this.applicationContext = applicationContext;

    logger.debug("Configuring {}", this.getClass().getSimpleName());

    register(applicationContext);
    inform(applicationContext, redisSecurityContextConfigurationProperties);
  }

  private void register(ApplicationContext applicationContext) {
    Declarations.of((BeanDefinitionRegistry) applicationContext.getAutowireCapableBeanFactory())
        .consume(this::registerConnectionFactory)
        .consume(this::registerTemplate)
        .consume(this::registerServices)
        .consume(this::registerManager);
  }

  private void inform(
      ApplicationContext applicationContext,
      RedisSecurityContextConfigurationProperties configurationProperties)
      throws ClassNotFoundException {
    Declarations.of(applicationContext, configurationProperties)
        .map(ManagerListenersLocator::new)
        .tryMap(ManagerListenersLocator::locate)
        .second(applicationContext.getBean(SecurityEntryManager.class))
        .consume(
            (listeners, manager) -> listeners
                .forEach(listener -> listener.informManagerBuilt(manager)));
  }

  private void registerManager(BeanDefinitionRegistry registry) {
    logRegistry(SecurityEntryManagerImpl.class);

    SpringUtils.register(
        registry,
        SecurityEntryManagerImpl.class,
        SecurityEntryManagerImpl.class.getName());
  }

  private static void logRegistry(Class<?> type) {
    logger.trace("Registering {}", type.getName());
  }

  private void registerServices(BeanDefinitionRegistry registry) {
    Stream.of(KeyRendererImpl.class, EventListenerGroupImpl.class)
        .forEach(beanClass -> this.registerService(registry, beanClass));
  }

  private void registerService(BeanDefinitionRegistry registry, Class<?> beanClass) {
    logger.trace("Registering service {}", beanClass);

    SpringUtils.register(registry, beanClass, beanClass.getName());
  }

  private void registerTemplate(BeanDefinitionRegistry registry) {
    logRegistry(DefaultRedisTemplate.class);

    SpringUtils.register(
        registry, DefaultRedisTemplate.class,
        DefaultRedisTemplate.class.getName());
  }

  private void registerConnectionFactory(BeanDefinitionRegistry registry) {
    logger.trace("Registering {}", DefaultJedisConnectionFactory.class.getName());

    SpringUtils.register(
        registry, DefaultJedisConnectionFactory.class,
        DefaultJedisConnectionFactory.class.getName());
  }

  @Override
  public void destroy() {
    logger.debug("Clearing {} context", DomainSecurityContext.class.getSimpleName());

    final RedisOperationsImplementor redisOperations =
        applicationContext.getBean(RedisOperationsImplementor.class);

    redisOperations.execute(RedisUtils::flushDb);
  }

  private static class ManagerListenersLocator {

    private final ApplicationContext applicationContext;
    private final RedisSecurityContextConfigurationProperties configurationProperties;

    public ManagerListenersLocator(
        ApplicationContext applicationContext,
        RedisSecurityContextConfigurationProperties redisSecurityContextConfigurationProperties) {
      this.applicationContext = applicationContext;
      this.configurationProperties =
          redisSecurityContextConfigurationProperties;
    }

    public List<SecurityEntryManagerListener> locate() throws ClassNotFoundException {
      logger.trace("Scanning for {} instances", SecurityEntryManagerListener.class);

      return Declarations.of(scan()).tryMap(this::construct).get();
    }

    private List<BeanDefinition> scan() {
      final ClassPathScanningCandidateComponentProvider scanner =
          new ClassPathScanningCandidateComponentProvider(false);

      scanner.addIncludeFilter(new AssignableTypeFilter(SecurityEntryManagerListener.class));

      return Stream.of(
              Settings.CORE_PACKAGE,
              configurationProperties.getPackageToScan())
          .map(scanner::findCandidateComponents)
          .flatMap(Collection::stream)
          .toList();
    }

    private List<SecurityEntryManagerListener> construct(List<BeanDefinition> beanDefs)
        throws BeansException, IllegalStateException, ClassNotFoundException {
      final List<SecurityEntryManagerListener> candidates = new ArrayList<>();

      for (final BeanDefinition beanDef : beanDefs) {
        candidates.add(
            SpringUtils.instantiate(beanDef, applicationContext.getAutowireCapableBeanFactory()));
      }

      return Collections.unmodifiableList(candidates);
    }

  }

}
