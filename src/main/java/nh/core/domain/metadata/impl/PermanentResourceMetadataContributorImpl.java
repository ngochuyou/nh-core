package nh.core.domain.metadata.impl;

import java.lang.annotation.Annotation;
import nh.core.declaration.Declarations;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.PermanentResource;
import nh.core.domain.annotation.Existence;
import nh.core.domain.metadata.PermanentResourceMetadata;
import nh.core.util.ReflectionUtils;

/**
 * @author Ngoc Huy
 */
public class PermanentResourceMetadataContributorImpl<P extends PermanentResource>
    implements PermanentResourceMetadataContributor<P, IllegalArgumentException> {

  @Override
  public PermanentResourceMetadata<P> contribute(
      Class<P> resourceType,
      DomainResourceContext domainResourceContext) {
    return new PermanentResourceMetadataImpl<>(
        resourceType,
        Declarations
            .<Class<P>, Class<? extends Annotation>>of(resourceType, Existence.class)
            .map(ReflectionUtils::requireField)
            .get(),
        domainResourceContext);
  }

}
