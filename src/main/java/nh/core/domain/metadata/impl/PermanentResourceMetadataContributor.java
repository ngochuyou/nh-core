package nh.core.domain.metadata.impl;

import nh.core.domain.PermanentResource;
import nh.core.domain.metadata.PermanentResourceMetadata;

/**
 * @author Ngoc Huy
 */
public interface PermanentResourceMetadataContributor<P extends PermanentResource, E extends Exception>
    extends DomainResourceMetadataContributor<P, PermanentResourceMetadata<P>, E> {

}
