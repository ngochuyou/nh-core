/**
 *
 */
package nh.core.domain.metadata.impl;

import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import nh.core.constants.Strings;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.metadata.AssociationForm;
import nh.core.domain.metadata.ComponentPath;
import nh.core.domain.metadata.DomainAssociation;
import nh.core.domain.metadata.DomainAssociation.OptionalAssociation;
import nh.core.domain.metadata.DomainResourceAttributesMetadata;

/**
 * Fully describes a {@link DomainResource}
 *
 * The root role of this implementation is the
 * {@link DomainResourceAttributesMetadata}, any other metadata will be
 * registered as a dependency
 *
 * @author Ngoc Huy
 *
 */
class DomainResourceAttributesMetadataImpl<T extends DomainResource>
    extends AbstractDomainResourceMetadata<T> implements DomainResourceAttributesMetadata<T> {

  private final List<String> declaredAttributeNames;
  private final List<String> unwrappedAttributeNames;
  private final List<String> wrappedAttributeNames;
  private final List<String> nonLazyAttributeNames;
  private final Map<String, Class<?>> attributeTypes;

  private final Map<String, ComponentPath> componentPaths;
  private final Map<String, DomainAssociation> associations;

  @SuppressWarnings("squid:S107")
  DomainResourceAttributesMetadataImpl(
      Class<T> resourceType,
      List<String> declaredAttributeNames,
      List<String> wrappedAttributeNames,
      List<String> unwrappedAttributeNames,
      Map<String, Class<?>> attributeTypes,
      List<String> nonLazyAttributeNames,
      Map<String, ComponentPath> componentPaths,
      Map<String, DomainAssociation> associations,
      DomainResourceContext resourceContext) {
    super(resourceType, resourceContext);
    this.declaredAttributeNames = unmodifiableList(declaredAttributeNames);
    this.unwrappedAttributeNames = unmodifiableList(unwrappedAttributeNames);
    this.wrappedAttributeNames = unmodifiableList(wrappedAttributeNames);
    this.nonLazyAttributeNames = unmodifiableList(nonLazyAttributeNames);
    this.attributeTypes = unmodifiableMap(attributeTypes);
    this.componentPaths = unmodifiableMap(componentPaths);
    this.associations = unmodifiableMap(associations);
  }

  @Override
  public List<String> getDeclaredAttributeNames() {
    return declaredAttributeNames;
  }

  @Override
  public List<String> getAttributeNames() {
    return unwrappedAttributeNames;
  }

  @Override
  public List<String> getWrappedAttributeNames() {
    return wrappedAttributeNames;
  }

  @Override
  public Class<?> getAttributeType(String attributeName) {
    assertAttributePresence(attributeName);
    return attributeTypes.get(attributeName);
  }

  @Override
  public List<String> getNonLazyAttributeNames() {
    return nonLazyAttributeNames;
  }

  @Override
  public boolean isAssociation(String attributeName) {
    return associations.containsKey(attributeName);
  }

  @Override
  public boolean isAssociationOptional(String associationName) {
    assertAttributePresence(associationName);
    return OptionalAssociation.class.isAssignableFrom(associations.get(associationName).getClass());
  }

  private void assertAttributePresence(String associationName) {

    if (!attributeTypes.containsKey(associationName)) {
      throw new IllegalArgumentException(String.format("Unknown attribute %s", associationName));
    }
  }

  @Override
  public Map<String, ComponentPath> getComponentPaths() {
    return componentPaths;
  }

  @Override
  public AssociationForm getAssociationForm(String attributeName) {
    assertAttributePresence(attributeName);
    return associations.get(attributeName).getType();
  }

  @Override
  public boolean isComponent(String attributeName) {
    assertAttributePresence(attributeName);
    return componentPaths.containsKey(attributeName);
  }

  public Map<String, Class<?>> getAttributeTypes() {
    return attributeTypes;
  }

  public Map<String, DomainAssociation> getAssociations() {
    return associations;
  }

  private <E> String collectList(List<E> list, Function<E, String> toString) {
    return list.isEmpty()
        ? "<<empty>>"
        : list.stream().map(toString).collect(Collectors.joining(Strings.COMMON_JOINER));
  }

  private <K, V> String collectMap(
      Map<K, V> map,
      Function<Map.Entry<K, V>, String> toString,
      CharSequence joiner) {
    return map.isEmpty()
        ? "<<empty>>"
        : map.entrySet().stream().map(toString).collect(Collectors.joining(joiner));
  }

  @Override
  public String toString() {

    if (wrappedAttributeNames.isEmpty()) {
      return String.format(
          "%s<%s>(<<empty>>)", this.getClass().getSimpleName(), getResourceType().getName());
    }

    final String joiner = "\n\t\t";

    return String.format(
        """
            %s<%s>(
            \tattributeNames=[
            \t\t%s
            \t],
            \tenclosedAttributeNames=[
            \t\t%s
            \t],
            \tnonLazyAttributes=[
            \t\t%s
            \t],
            \tattributeTypes=[
            \t\t%s
            \t],
            \tassociationAttributes=[
            \t\t%s
            \t],
            \tcomponentPaths=[
            \t\t%s
            \t]
            )""", this.getClass().getSimpleName(), getResourceType().getName(),
        collectList(unwrappedAttributeNames, Function.identity()),
        collectList(wrappedAttributeNames, Function.identity()),
        collectList(nonLazyAttributeNames, Function.identity()),
        collectMap(
            attributeTypes,
            entry -> String.format("%s: %s", entry.getKey(), entry.getValue().getName()), joiner),
        collectMap(
            associations,
            entry -> String.format("%s|%s", entry.getKey(), entry.getValue().getType()), joiner),
        collectMap(
            componentPaths, entry -> String.format("%s: %s", entry.getKey(), entry.getValue()),
            joiner));
  }

}
