package nh.core.domain.metadata.impl;

import java.util.regex.Pattern;
import nh.core.domain.metadata.MessageSourceArgumentProvider;

public interface Naming {

  String getAttributeName();

  Pattern getPattern();

  MessageSourceArgumentProvider getArgumentProvider();

  boolean isUnique();

}
