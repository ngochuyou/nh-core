package nh.core.domain.metadata.impl;

import java.util.regex.Pattern;
import nh.core.domain.metadata.MessageSourceArgumentProvider;

class NamingImpl implements Naming {

  private final String attributeName;
  private final Pattern pattern;
  private final boolean unique;
  private final MessageSourceArgumentProvider argumentProvider;

  NamingImpl(String attributeName, Pattern pattern, MessageSourceArgumentProvider argumentProvider,
      boolean unique) {
    this.attributeName = attributeName;
    this.pattern = pattern;
    this.argumentProvider = argumentProvider;
    this.unique = unique;
  }

  @Override
  public String getAttributeName() {
    return attributeName;
  }

  @Override
  public Pattern getPattern() {
    return pattern;
  }

  @Override
  public MessageSourceArgumentProvider getArgumentProvider() {
    return argumentProvider;
  }

  @Override
  public boolean isUnique() {
    return unique;
  }
}
