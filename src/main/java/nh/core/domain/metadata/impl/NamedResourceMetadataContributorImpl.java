package nh.core.domain.metadata.impl;

import static org.apache.commons.lang3.StringUtils.isBlank;

import jakarta.persistence.Column;
import java.lang.reflect.Field;
import java.util.Optional;
import java.util.regex.Pattern;
import nh.core.BundledMessage;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.NamedResource;
import nh.core.domain.annotation.Name;
import nh.core.domain.annotation.Name.Message;
import nh.core.domain.metadata.MessageSourceArgumentProvider;
import nh.core.domain.metadata.NamedResourceMetadata;
import nh.core.util.ReflectionUtils;
import nh.core.util.SpringUtils;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

/**
 * @author Ngoc Huy
 */
class NamedResourceMetadataContributorImpl<N extends NamedResource> implements
    NamedResourceMetadataContributor<N, IllegalArgumentException> {

  private final AutowireCapableBeanFactory beanFactory;

  NamedResourceMetadataContributorImpl(AutowireCapableBeanFactory beanFactory) {
    this.beanFactory = beanFactory;
  }

  @Override
  public NamedResourceMetadata<N> contribute(
      Class<N> resourceType,
      DomainResourceContext domainResourceContext)
      throws IllegalArgumentException {
    return new NamedResourceMetadataImpl<>(resourceType,
        resolveNaming(resourceType),
        domainResourceContext);
  }

  private Naming resolveNaming(Class<N> resourceType) {
    final Field field = ReflectionUtils.locateFields(resourceType,
            Name.class,
            Message::getMissingMessage)
        .get(0);

    return new NamingImpl(field.getName(),
        resolvePattern(field.getDeclaredAnnotation(Name.class).pattern()),
        resolveArgumentProvider(field),
        resolveUniqueness(field));
  }

  private MessageSourceArgumentProvider resolveArgumentProvider(Field field) {
    final Optional<nh.core.domain.annotation.Message> optionalMessageAnnotation =
        ReflectionUtils.locateAnnotation(field, nh.core.domain.annotation.Message.class);

    if (optionalMessageAnnotation.isEmpty()) {
      return getDefaultProvider();
    }

    final nh.core.domain.annotation.Message annotation = optionalMessageAnnotation.get();
    final String configuredMessage = annotation.value();

    if (!isBlank(configuredMessage)) {
      return fromMessage(configuredMessage);
    }

    final String code = annotation.code();

    if (!isBlank(code)) {
      return fromCode(annotation);
    }

    final Class<? extends MessageSourceArgumentProvider> argumentProviderClass = annotation.argumentProvider();

    if (MessageSourceArgumentProvider.class.equals(argumentProviderClass)) {
      return getDefaultProvider();
    }

    return SpringUtils.instantiate(argumentProviderClass, beanFactory);
  }

  /**
   * Let the {@link nh.core.domain.validation.impl.named.NamedDomainValidatorFactory} decides
   */
  private static MessageSourceArgumentProvider getDefaultProvider() {
    return null;
  }

  private static MessageSourceArgumentProvider fromMessage(String configuredMessage) {
    return (fieldName, pattern) -> new BundledMessage() {
      @Override
      public String getDefault() {
        return configuredMessage;
      }
    };
  }

  private static MessageSourceArgumentProvider fromCode(
      nh.core.domain.annotation.Message annotation) {
    return (fieldName, pattern) -> new BundledMessage() {
      @Override
      public String getCode() {
        return annotation.code();
      }

      @Override
      public Object[] getArguments() {
        return annotation.args();
      }
    };
  }

  private Pattern resolvePattern(String pattern) {
    return isBlank(pattern)
        ? null
        : Pattern.compile(pattern);
  }

  private boolean resolveUniqueness(Field field) {
    return Optional.ofNullable(field.getDeclaredAnnotation(Column.class))
        .map(Column::unique)
        .orElse(false);
  }

}
