package nh.core.domain.metadata.impl;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.annotation.Inter;
import nh.core.domain.metadata.InternationalizedAttributesMetadata;
import nh.core.util.ReflectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Ngoc Huy
 */
public class InternationalizedAttributesMetadataContributorImpl<D extends DomainResource>
    implements InternationalizedAttributesMetadataContributor<D, Exception> {

  @SuppressWarnings("rawtypes")
  public static final InternationalizedAttributesMetadataContributorImpl INSTANCE = new InternationalizedAttributesMetadataContributorImpl();

  private InternationalizedAttributesMetadataContributorImpl() {}

  @Override
  public InternationalizedAttributesMetadata<D> contribute(Class<D> resourceType,
      DomainResourceContext domainResourceContext) {
    return new InternationalizedAttributesMetadataImpl<>(resourceType,
        domainResourceContext,
        resolveMappings(resourceType));
  }

  private static <D extends DomainResource> Map<String, String> resolveMappings(Class<D> resourceType) {
    return Collections.unmodifiableMap(ReflectionUtils.getFields(resourceType)
        .stream()
        .filter(field -> Objects.nonNull(ReflectionUtils.locateAnnotation(field, Inter.class)))
        .map(InternationalizedAttributesMetadataContributorImpl::resolveInternationalization)
        .filter(Objects::nonNull)
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
  }

  private static Map.Entry<String, String> resolveInternationalization(Field field) {
    final String value = field.getDeclaredAnnotation(Inter.class).value();

    if (StringUtils.isBlank(value)) {
      return null;
    }

    return Map.entry(field.getName(), value);
  }

  @SuppressWarnings("unchecked")
  public static <E extends DomainResource> InternationalizedAttributesMetadataContributor<E, Exception> getInstance() {
    return INSTANCE;
  }

}
