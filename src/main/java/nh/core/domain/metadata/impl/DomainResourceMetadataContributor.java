package nh.core.domain.metadata.impl;

import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.metadata.DomainResourceMetadata;

/**
 * @author Ngoc Huy
 */
public interface DomainResourceMetadataContributor<D extends DomainResource, M extends DomainResourceMetadata<D>, E extends Exception> {

  M contribute(Class<D> resourceType, DomainResourceContext domainResourceContext) throws E;

}
