package nh.core.domain.metadata.impl;

import java.io.Serializable;
import nh.core.domain.EncryptedIdentifierResource;
import nh.core.domain.metadata.EncryptedIdentifierResourceMetadata;

/**
 * @author Ngoc Huy
 */
public interface EncryptedIdentifierResourceMetadataContributor<S extends Serializable, E extends EncryptedIdentifierResource<S>, R extends Exception>
    extends DomainResourceMetadataContributor<E, EncryptedIdentifierResourceMetadata<S, E>, R> {

}
