package nh.core.domain.metadata.impl;

import java.lang.reflect.Field;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.PermanentResource;
import nh.core.domain.metadata.PermanentResourceMetadata;

/**
 * @author Ngoc Huy
 */
class PermanentResourceMetadataImpl<P extends PermanentResource>
    extends AbstractDomainResourceMetadata<P> implements PermanentResourceMetadata<P> {

  private final Field scopedField;

  public PermanentResourceMetadataImpl(
      Class<P> resourceType,
      Field scopedField,
      DomainResourceContext resourceContext) {
    super(resourceType, resourceContext);
    this.scopedField = scopedField;
  }

  @Override
  public Field getScopedField() {
    return scopedField;
  }

}
