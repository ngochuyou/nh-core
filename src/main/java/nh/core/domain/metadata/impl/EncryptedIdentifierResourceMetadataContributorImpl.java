package nh.core.domain.metadata.impl;

import java.io.Serializable;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.EncryptedIdentifierResource;
import nh.core.domain.annotation.Encrypted;
import nh.core.domain.metadata.EncryptedIdentifierResourceMetadata;
import nh.core.util.ReflectionUtils;

/**
 * @author Ngoc Huy
 */
class EncryptedIdentifierResourceMetadataContributorImpl<S extends Serializable, E extends EncryptedIdentifierResource<S>>
    implements
    EncryptedIdentifierResourceMetadataContributor<S, E, NoSuchFieldException> {

  EncryptedIdentifierResourceMetadataContributorImpl() {
  }

  @Override
  public EncryptedIdentifierResourceMetadata<S, E> contribute(
      Class<E> resourceType,
      DomainResourceContext domainResourceContext) {
    return new EncryptedIdentifierResourceMetadataImpl<>(
        resourceType,
        ReflectionUtils.requireField(resourceType, Encrypted.class),
        domainResourceContext);
  }

}
