package nh.core.domain.metadata.impl;

import java.util.Map;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.metadata.InternationalizedAttributesMetadata;

class InternationalizedAttributesMetadataImpl<D extends DomainResource>
    extends AbstractDomainResourceMetadata<D> implements
    InternationalizedAttributesMetadata<D> {

  private final Map<String, String> nameMappings;

  InternationalizedAttributesMetadataImpl(Class<D> resourceType,
      DomainResourceContext resourceContext, Map<String, String> nameMappings) {
    super(resourceType, resourceContext);
    this.nameMappings = nameMappings;
  }

  @Override
  public Map<String, String> getNameMappings() {
    return nameMappings;
  }
}
