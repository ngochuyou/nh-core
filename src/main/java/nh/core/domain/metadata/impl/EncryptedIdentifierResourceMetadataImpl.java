package nh.core.domain.metadata.impl;

import java.io.Serializable;
import java.lang.reflect.Field;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.EncryptedIdentifierResource;
import nh.core.domain.metadata.EncryptedIdentifierResourceMetadata;

/**
 * @author Ngoc Huy
 */
class EncryptedIdentifierResourceMetadataImpl<S extends Serializable, E extends EncryptedIdentifierResource<S>>
    extends AbstractDomainResourceMetadata<E>
    implements EncryptedIdentifierResourceMetadata<S, E> {

  private final Field field;

  EncryptedIdentifierResourceMetadataImpl(
      Class<E> resourceType,
      Field field,
      DomainResourceContext resourceContext) {
    super(resourceType, resourceContext);
    this.field = field;
  }

  @Override
  public Field getScopedField() {
    return field;
  }

}
