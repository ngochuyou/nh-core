/**
 *
 */
package nh.core.domain.metadata.impl;

import static nh.core.declaration.Declarations.of;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import nh.core.domain.DomainComponent;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.metadata.AssociationForm;
import nh.core.domain.metadata.ComponentPath;
import nh.core.domain.metadata.DomainAssociation;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;

/**
 * @author Ngoc Huy
 *
 */
class DomainResourceMetadataContributorImpl<D extends DomainResource>
    implements
    BasicDomainResourceMetadataContributor<D, DomainResourceMetadata<D>, NoSuchFieldException> {

  private static final Logger logger =
      LoggerFactory.getLogger(DomainResourceMetadataContributorImpl.class);

  private final Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas;

  public DomainResourceMetadataContributorImpl(
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas) {
    this.onGoingMetadatas = onGoingMetadatas;
  }

  @Override
  public DomainResourceMetadata<D> contribute(
      Class<D> resourceType,
      DomainResourceContext domainResourceContext)
      throws NoSuchFieldException, SecurityException {

    if (logger.isTraceEnabled()) {
      logger.trace(
          "Building {} for resource type {}", DomainResourceMetadata.class.getName(),
          resourceType.getName());
    }

    final List<String> declaredAttributeNames = getDeclaredAttributeNames(resourceType);
    final List<String> wrappedAttributes =
        locateWrappedAttributeNames(resourceType, declaredAttributeNames, onGoingMetadatas);
    final Map<String, Class<?>> attributeTypes =
        resolveAttributeTypes(resourceType, wrappedAttributes, onGoingMetadatas);
    final List<String> attributesToBeUnwrapped = new ArrayList<>(wrappedAttributes);

    unwrapAttributes(attributesToBeUnwrapped, attributeTypes);

    final Map<String, ComponentPath> componentPaths =
        resolveComponentPaths(resourceType, wrappedAttributes, attributeTypes, onGoingMetadatas);
    final Map<String, DomainAssociation> associations = locateAssociations(
        resourceType, wrappedAttributes, attributeTypes, componentPaths, onGoingMetadatas);

    return new DomainResourceAttributesMetadataImpl<>(
        resourceType,
        declaredAttributeNames,
        wrappedAttributes,
        attributesToBeUnwrapped,
        attributeTypes,
        wrappedAttributes,
        componentPaths,
        associations,
        domainResourceContext);
  }

  private Map<String, DomainAssociation> locateAssociations(
      Class<D> resourceType,
      List<String> unwrappedAttributeNames,
      Map<String, Class<?>> attributeTypes,
      Map<String, ComponentPath> componentPaths,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas)
      throws NoSuchFieldException, SecurityException {

    if (logger.isTraceEnabled()) {
      logger.trace("Locating associations");
    }

    return of(
        locateDeclaredAssociations(
            resourceType, unwrappedAttributeNames, attributeTypes, componentPaths))
        .second(resourceType).third(onGoingMetadatas)
        .map(this::appendAssociationsFromParent).get();

  }

  private Map<String, DomainAssociation> appendAssociationsFromParent(

      Map<String, DomainAssociation> declaredAssociations,
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas) {
    final DomainResourceAttributesMetadataImpl<?> parentMetadata =
        locateParentMetadata(resourceType, onGoingMetadatas);

    if (parentMetadata == null) {
      return Collections.emptyMap();
    }

    of(parentMetadata).map(DomainResourceAttributesMetadataImpl.class::cast)
        .map(DomainResourceAttributesMetadataImpl::getAssociations)
        .consume(declaredAssociations::putAll);

    return declaredAssociations;
  }

  private Map<String, DomainAssociation> locateDeclaredAssociations(
      Class<D> resourceType,
      List<String> unwrappedAttributeNames,
      Map<String, Class<?>> attributeTypes,
      Map<String, ComponentPath> componentPaths) throws NoSuchFieldException, SecurityException {
    final Map<String, DomainAssociation> associations = new HashMap<>(0);

    for (final String attributeName : unwrappedAttributeNames) {

      if (!DomainResource.class.isAssignableFrom(attributeTypes.get(attributeName))) {
        continue;
      }

      final AssociationForm associationForm =
          DomainResource.class.isAssignableFrom(attributeTypes.get(attributeName))
              ? AssociationForm.SINGULAR
              : AssociationForm.PLURAL;

      associations.put(
          attributeName,
          isComponentAssociationOptional(resourceType, componentPaths.get(attributeName).getPath())
              ? new DomainAssociation.OptionalAssociation(attributeName, associationForm)
              : new DomainAssociation.MandatoryAssociation(attributeName, associationForm));
    }

    return associations;
  }

  private boolean isComponentAssociationOptional(
      Class<D> resourceType,
      Queue<String> path) throws NoSuchFieldException, SecurityException {
    final Queue<String> copiedPath = new ArrayDeque<>(path);
    Class<?> currentType = resourceType;
    Field field = currentType.getDeclaredField(copiedPath.poll());

    while (!copiedPath.isEmpty()) {
      currentType = field.getType();
      field = currentType.getDeclaredField(copiedPath.poll());
    }

    return field.isAnnotationPresent(Nullable.class);
  }

  private Map<String, ComponentPath> resolveComponentPaths(
      Class<D> resourceType,
      List<String> attributeNames,
      Map<String, Class<?>> attributeTypes,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas) {

    if (logger.isTraceEnabled()) {
      logger.trace("Resolving component paths");
    }

    of(locateDeclaredComponentPaths(attributeNames, attributeTypes, null)).second(resourceType)
        .third(onGoingMetadatas);

    return of(locateDeclaredComponentPaths(attributeNames, attributeTypes, null))
        .second(resourceType).third(onGoingMetadatas).map(this::appendComponentPathsFromParent)
        .get();
  }

  private Map<String, ComponentPath> appendComponentPathsFromParent(

      Map<String, ComponentPath> declaredComponentPaths,
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas) {

    final DomainResourceAttributesMetadataImpl<?> parentMetadata =
        locateParentMetadata(resourceType, onGoingMetadatas);

    if (parentMetadata == null) {
      return Collections.emptyMap();
    }

    of(parentMetadata).map(DomainResourceAttributesMetadataImpl.class::cast)
        .map(DomainResourceAttributesMetadataImpl::getComponentPaths)
        .consume(declaredComponentPaths::putAll);

    return declaredComponentPaths;
  }

  private Map<String, ComponentPath> locateDeclaredComponentPaths(
      List<String> attributeNames,
      Map<String, Class<?>> attributeTypes,
      ComponentPathImpl parentPath) {
    final Map<String, ComponentPath> componentPaths = new HashMap<>();

    for (final String attributeName : attributeNames) {
      final Class<?> possibleComponentType = attributeTypes.get(attributeName);
      final ComponentPathImpl componentPath = parentPath == null
          ? new ComponentPathImpl(attributeName)
          : of(parentPath).map(ComponentPathImpl::new).consume(self -> self.add(attributeName))
              .get();

      if (!DomainComponent.class.isAssignableFrom(possibleComponentType)) {

        if (parentPath != null) {
          componentPaths.put(attributeName, componentPath);
        }

        continue;
      }

      of(possibleComponentType).map(this::getDeclaredAttributeNames).second(attributeTypes)
          .third(componentPath).map(this::locateDeclaredComponentPaths)
          .consume(componentPaths::putAll);

    }

    return componentPaths;
  }

  /**
   * Thoroughly unwrap component attributes, collect attribute names and types
   *
   *
   * @param attributeNames reflect collected attribute names
   * @param attributeTypes reflect collected attribute types @
   */
  private void unwrapAttributes(List<String> attributeNames, Map<String, Class<?>> attributeTypes) {

    if (logger.isTraceEnabled()) {
      logger.trace("Unwrapping attributes");
    }

    final List<String> finalComponentAttributeNames = new ArrayList<>(0);
    final Map<String, Class<?>> finalComponentAttributeTypes = new HashMap<>(0);

    for (final String attributeName : attributeNames) {
      final Class<?> attributeType = attributeTypes.get(attributeName);

      if (!DomainComponent.class.isAssignableFrom(attributeType)) {
        continue;
      }

      final List<String> componentAttributeNames = new ArrayList<>(0);
      final Map<String, Class<?>> componentAttributeTypes = new HashMap<>(0);

      for (final Field componentField : Stream.of(attributeType.getDeclaredFields())
          .filter(this::isFieldManaged).toList()) {
        final String componentAttributeName = componentField.getName();

        componentAttributeNames.add(componentAttributeName);
        componentAttributeTypes.put(componentAttributeName, componentField.getType());
        unwrapAttributes(componentAttributeNames, componentAttributeTypes);
      }

      finalComponentAttributeNames.addAll(componentAttributeNames);
      finalComponentAttributeTypes.putAll(componentAttributeTypes);
    }

    attributeNames.addAll(finalComponentAttributeNames);
    attributeTypes.putAll(finalComponentAttributeTypes);
  }

  private Map<String, Class<?>> resolveAttributeTypes(
      Class<D> resourceType,
      List<String> wrappedAttributes,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas)
      throws NoSuchFieldException {

    if (logger.isTraceEnabled()) {
      logger.trace("Resolving attribute types");
    }

    return of(locateParentMetadata(resourceType, onGoingMetadatas)).second(wrappedAttributes)
        .third(resourceType).triInverse().tryMap(this::getAttributeTypes).second(resourceType)
        .third(onGoingMetadatas).map(this::appendAttributeTypesFromParent).get();
  }

  @SuppressWarnings("unchecked")
  private Map<String, Class<?>> appendAttributeTypesFromParent(
      Map<String, Class<?>> attributeTypes,
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas) {
    return of(locateParentMetadata(resourceType, onGoingMetadatas))
        .map(DomainResourceAttributesMetadataImpl.class::cast)
        .map(DomainResourceAttributesMetadataImpl::getAttributeTypes)
        .map(
            parentAttributeTypes -> of(attributeTypes)
                .consume(self -> self.putAll(parentAttributeTypes)).get())
        .get();

  }

  private Map<String, Class<?>> getAttributeTypes(
      Class<D> resourceType,
      List<String> attributeNames,
      DomainResourceAttributesMetadataImpl<?> parentMetadata)
      throws NoSuchFieldException, SecurityException {
    final Map<String, Class<?>> typesMap = new HashMap<>(attributeNames.size(), 1.5f);

    for (final String attribute : attributeNames) {

      if (parentMetadata != null && parentMetadata.getAttributeTypes().containsKey(attribute)) {
        typesMap.put(attribute, parentMetadata.getAttributeTypes().get(attribute));
        continue;
      }

      final Class<?> attributeType = resourceType.getDeclaredField(attribute).getType();

      typesMap.put(attribute, attributeType);
      typesMap.putAll(
          !DomainComponent.class.isAssignableFrom(attributeType)
              ? Collections.emptyMap()
              : getComponentAttributeTypes(attributeType));
    }

    return typesMap;
  }

  private Map<String, Class<?>> getComponentAttributeTypes(Class<?> componentType) {
    final Map<String, Class<?>> types = new HashMap<>(0);

    for (Field componentField : Stream.of(componentType.getDeclaredFields())
        .filter(this::isFieldManaged).toList()) {
      final Class<?> componentFieldType = componentField.getType();

      types.put(componentField.getName(), componentFieldType);

      if (!DomainComponent.class.isAssignableFrom(componentFieldType)) {
        continue;
      }

      types.putAll(getComponentAttributeTypes(componentFieldType));
    }

    return types;
  }

  private List<String> locateWrappedAttributeNames(
      Class<D> resourceType,
      List<String> declaredAttributeNames,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas) {

    if (logger.isTraceEnabled()) {
      logger.trace("Locating wrapped attributes");
    }

    return of(declaredAttributeNames).second(resourceType).third(onGoingMetadatas)
        .map(this::appendWrappedAttributeNamesFromParent).get();
  }

  private List<String> getDeclaredAttributeNames(Class<?> type) {
    return Stream.of(type.getDeclaredFields()).filter(this::isFieldManaged).map(Field::getName)
        .toList();
  }

  private boolean isFieldManaged(Field field) {
    final int modifiers = field.getModifiers();

    return !Modifier.isStatic(modifiers) && !Modifier.isTransient(modifiers);
  }

  private List<String> appendWrappedAttributeNamesFromParent(
      List<String> declaredAttributes,
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas) {
    return of(locateParentMetadata(resourceType, onGoingMetadatas))
        .map(
            parentMetadata -> parentMetadata != null
                ? parentMetadata.getWrappedAttributeNames()
                : List.<String>of())
        .map(
            parentAttributes -> CollectionUtils
                .join(List.of(declaredAttributes, parentAttributes), Collectors.toList()))
        .get();

  }

  private DomainResourceAttributesMetadataImpl<?> locateParentMetadata(
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadatas) {
    return (DomainResourceAttributesMetadataImpl<?>) onGoingMetadatas
        .get(resourceType.getSuperclass());
  }

}
