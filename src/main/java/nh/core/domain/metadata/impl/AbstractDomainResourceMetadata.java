/**
 *
 */
package nh.core.domain.metadata.impl;

import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.metadata.DomainResourceMetadata;

/**
 * @author Ngoc Huy
 *
 */
abstract class AbstractDomainResourceMetadata<D extends DomainResource>
    implements DomainResourceMetadata<D> {

  private final Class<D> resourceType;
  private final DomainResourceContext resourceContext;

  protected AbstractDomainResourceMetadata(
      Class<D> resourceType,
      DomainResourceContext resourceContext) {
    this.resourceType = resourceType;
    this.resourceContext = resourceContext;
  }

  @Override
  public Class<D> getResourceType() {
    return resourceType;
  }

  @Override
  public <M extends DomainResourceMetadata<D>> M require(Class<M> type) {
    return resourceContext.getMetadata(resourceType).require(type);
  }

}
