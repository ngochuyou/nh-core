package nh.core.domain.metadata.impl;

import nh.core.domain.NamedResource;
import nh.core.domain.metadata.NamedResourceMetadata;

/**
 * @author Ngoc Huy
 */
public interface NamedResourceMetadataContributor<N extends NamedResource, E extends Exception>
    extends DomainResourceMetadataContributor<N, NamedResourceMetadata<N>, E> {

}
