package nh.core.domain.metadata.impl;

import nh.core.domain.DomainResource;
import nh.core.domain.metadata.InternationalizedAttributesMetadata;

/**
 * @author Ngoc Huy
 */
public interface InternationalizedAttributesMetadataContributor<D extends DomainResource, E extends Exception>
    extends DomainResourceMetadataContributor<D, InternationalizedAttributesMetadata<D>, E> {

}
