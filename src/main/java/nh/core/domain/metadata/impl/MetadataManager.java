package nh.core.domain.metadata.impl;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import nh.core.declaration.Declarations;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.EncryptedIdentifierResource;
import nh.core.domain.IdentifiableResource;
import nh.core.domain.InternationalizedResource;
import nh.core.domain.NamedResource;
import nh.core.domain.PermanentResource;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.graph.impl.DomainResourceGraphCollectors;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.domain.metadata.InternationalizedAttributesMetadata;
import nh.core.domain.sf.SessionFactoryManager;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.util.ClassUtils;
import org.springframework.util.CollectionUtils;

public class MetadataManager {

  private static final Logger logger = LoggerFactory.getLogger(MetadataManager.class);

  private final AutowireCapableBeanFactory beanFactory;

  private final DomainResourceGraph<DomainResource> graph;
  private final SessionFactoryManager factoryManager;

  private final List<BiFunction<Class<? extends DomainResource>, Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>>, DomainResourceMetadataContributor<? extends DomainResource, ? extends DomainResourceMetadata<? extends DomainResource>, ? extends Exception>>> contributorQualifiers =
      List.of(
          this::qualifyBasicDomainResource,
          this::qualifyIdentifiableResource,
          this::qualifyNamedResource,
          this::qualifyPermanentResource,
          this::qualifyEncryptedIdentifierResource,
          this::qualifyInternationalizedAttributesMetadata);

  public MetadataManager(AutowireCapableBeanFactory beanFactory,
      DomainResourceGraph<DomainResource> graph,
      SessionFactoryManager factoryManager) {
    this.beanFactory = beanFactory;
    this.graph = graph;
    this.factoryManager = factoryManager;
  }

  private static boolean canBePersisted(Class<? extends DomainResource> resourceType) {
    final int modifiers = resourceType.getModifiers();

    return !Modifier.isInterface(resourceType.getModifiers()) && !Modifier.isAbstract(modifiers);
  }

  @SuppressWarnings({
      "squid:S1452", "unchecked"
  })
  public Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> build(
      DomainResourceContext domainResourceContext) throws Exception {
    logger.trace("Building metadatas");

    final Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> metadatas =
        new HashMap<>();

    for (final Class<? extends DomainResource> resourceType : graph
        .collect(DomainResourceGraphCollectors.toTypesSet())) {

      if (!canBePersisted(resourceType)) {
        continue;
      }

      logger.trace("Building metadata for type {}", resourceType.getName());

      final Class<DomainResource> wrappedType = (Class<DomainResource>) resourceType;

      metadatas.put(
          resourceType,
          getQualifiedContributor(wrappedType, metadatas)
              .contribute(wrappedType, domainResourceContext));
    }

    return Collections.unmodifiableMap(metadatas);
  }

  @SuppressWarnings("unchecked")
  private DomainResourceMetadataContributor<DomainResource, DomainResourceMetadata<DomainResource>, Exception> getQualifiedContributor(
      Class<DomainResource> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> metadatas) {
    return new CompositeDomainResourceMetadataContributor<>(
        contributorQualifiers.stream()
            .map(qualifier -> qualifier.apply(resourceType, metadatas))
            .map(
                contributor -> (DomainResourceMetadataContributor<DomainResource, DomainResourceMetadata<DomainResource>, Exception>) contributor)
            .filter(Objects::nonNull)
            .toList());
  }

  private <D extends DomainResource> DomainResourceMetadataContributor<D, DomainResourceMetadata<D>, NoSuchFieldException> qualifyBasicDomainResource(
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadata) {
    final SessionFactoryImplementor sfi = factoryManager.locateSessionFactory(resourceType);

    if (Objects.nonNull(sfi)) {
      return new HibernateDomainResourceMetadataContributor<>(sfi);
    }

    return new DomainResourceMetadataContributorImpl<>(onGoingMetadata);
  }

  @SuppressWarnings({
      "rawtypes", "unchecked"
  })
  private <D extends DomainResource> DomainResourceMetadataContributor<D, DomainResourceMetadata<D>, NoSuchFieldException> qualifyIdentifiableResource(
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadata) {

    if (!IdentifiableResource.class.isAssignableFrom(resourceType)) {
      return null;
    }

    return new IdentifiableResourceMetadataContributorImpl(factoryManager);
  }

  @SuppressWarnings({
      "rawtypes", "unchecked"
  })
  private <D extends DomainResource> DomainResourceMetadataContributor<D, DomainResourceMetadata<D>, NoSuchFieldException> qualifyNamedResource(
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadata) {

    if (!NamedResource.class.isAssignableFrom(resourceType)) {
      return null;
    }

    return new NamedResourceMetadataContributorImpl(beanFactory);
  }

  @SuppressWarnings({
      "rawtypes", "unchecked"
  })
  private <D extends DomainResource> DomainResourceMetadataContributor<D, DomainResourceMetadata<D>, NoSuchFieldException> qualifyPermanentResource(
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadata) {

    if (!PermanentResource.class.isAssignableFrom(resourceType)) {
      return null;
    }

    return new PermanentResourceMetadataContributorImpl();
  }

  @SuppressWarnings({
      "rawtypes", "unchecked"
  })
  private <D extends DomainResource> DomainResourceMetadataContributor<D, DomainResourceMetadata<D>, NoSuchFieldException> qualifyEncryptedIdentifierResource(
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadata) {

    if (!EncryptedIdentifierResource.class.isAssignableFrom(resourceType)) {
      return null;
    }

    return new EncryptedIdentifierResourceMetadataContributorImpl();
  }

  @SuppressWarnings("squid:S1172")
  private <D extends DomainResource> DomainResourceMetadataContributor<D, InternationalizedAttributesMetadata<D>, Exception> qualifyInternationalizedAttributesMetadata(
      Class<D> resourceType,
      Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> onGoingMetadata) {

    if (!InternationalizedResource.class.isAssignableFrom(resourceType)) {
      return null;
    }

    return InternationalizedAttributesMetadataContributorImpl.getInstance();
  }

  private record CompositeDomainResourceMetadataContributor<D extends DomainResource>(
      List<DomainResourceMetadataContributor<D, DomainResourceMetadata<D>, Exception>> contributors)
        implements
        DomainResourceMetadataContributor<D, DomainResourceMetadata<D>, Exception> {

    @Override
      public DomainResourceMetadata<D> contribute(
          Class<D> resourceType,
          DomainResourceContext domainResourceContext) throws Exception {

        if (CollectionUtils.isEmpty(contributors)) {
          throw new IllegalArgumentException(
              String.format(
                  "Did not find any %s for resource type [%s]",
                  DomainResourceMetadata.class.getSimpleName(), resourceType));
        }

        final int size = contributors.size();

        if (size == 1) {
          return contributors.get(0).contribute(resourceType, domainResourceContext);
        }

        final List<DomainResourceMetadata<D>> metadatas =
            new ArrayList<>(contributors.size());

        for (final DomainResourceMetadataContributor<D, DomainResourceMetadata<D>, Exception> contributor : contributors) {
          Declarations.of(resourceType, domainResourceContext)
              .tryMap(contributor::contribute)
              .map(DomainResourceMetadata.class::cast)
              .consume(metadatas::add);
        }

        return new CompositeDomainResourceMetadata(
            resourceType,
            metadatas,
            domainResourceContext);
      }

      private final class CompositeDomainResourceMetadata
          extends AbstractDomainResourceMetadata<D> {

        private final Map<Class<DomainResourceMetadata<D>>, DomainResourceMetadata<D>> metadatasMap;

        public CompositeDomainResourceMetadata(
            Class<D> resourceType,
            List<DomainResourceMetadata<D>> metadatas,
            DomainResourceContext resourceContext) {
          super(resourceType, resourceContext);

          metadatasMap = metadatas.stream()
              .map(this::resolveMappings)
              .flatMap(List::stream)
              .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
        }

        @SuppressWarnings("unchecked")
        private List<Entry<Class<DomainResourceMetadata<D>>, DomainResourceMetadata<D>>> resolveMappings(
            DomainResourceMetadata<D> metadata) {
          return Stream.of(
                  Stream.of(Map.entry((Class<DomainResourceMetadata<D>>) metadata.getClass(), metadata)),
                  ClassUtils
                      .getAllInterfacesAsSet(metadata)
                      .stream()
                      .filter(
                          metadataClass -> DomainResourceMetadata.class.isAssignableFrom(metadataClass)
                              && !DomainResourceMetadata.class.equals(metadataClass))
                      .map(metadataClass -> (Class<DomainResourceMetadata<D>>) metadataClass)
                      .map(metadataClass -> Map.entry(metadataClass, metadata)))
              .flatMap(Function.identity())
              .toList();
        }

        @SuppressWarnings("unchecked")
        @Override
        public <M extends DomainResourceMetadata<D>> M require(Class<M> type) {
          return (M) metadatasMap.get(type);
        }

        @Override
        public String toString() {
          return new HashSet<>(metadatasMap.values()).stream()
              .map(Object::toString)
              .collect(Collectors.joining("\n"));
        }

      }

    }

}
