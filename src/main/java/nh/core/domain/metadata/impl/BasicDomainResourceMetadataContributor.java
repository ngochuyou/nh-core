package nh.core.domain.metadata.impl;

import nh.core.domain.DomainResource;
import nh.core.domain.metadata.DomainResourceMetadata;

/**
 * @author Ngoc Huy
 */
public interface BasicDomainResourceMetadataContributor<D extends DomainResource, M extends DomainResourceMetadata<D>, E extends Exception>
    extends DomainResourceMetadataContributor<D, M, E> {

}
