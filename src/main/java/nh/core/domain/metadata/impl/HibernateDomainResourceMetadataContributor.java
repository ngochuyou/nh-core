package nh.core.domain.metadata.impl;

import static nh.core.declaration.Declarations.of;

import jakarta.persistence.metamodel.Attribute;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.Entity;
import nh.core.domain.metadata.AssociationForm;
import nh.core.domain.metadata.ComponentPath;
import nh.core.domain.metadata.DomainAssociation;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.util.TypeUtils;
import org.apache.catalina.webresources.FileResource;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.tuple.entity.EntityMetamodel;
import org.hibernate.type.CollectionType;
import org.hibernate.type.ComponentType;
import org.hibernate.type.EntityType;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class HibernateDomainResourceMetadataContributor<D extends DomainResource>
    implements
    BasicDomainResourceMetadataContributor<D, DomainResourceMetadata<D>, NoSuchFieldException> {

  private static final Logger logger =
      LoggerFactory.getLogger(HibernateDomainResourceMetadataContributor.class);

  private final SessionFactoryImplementor sfi;

  HibernateDomainResourceMetadataContributor(SessionFactoryImplementor sfi) {
    this.sfi = sfi;
  }

  @Override
  public DomainResourceMetadata<D> contribute(
      Class<D> resourceType,
      DomainResourceContext domainResourceContext) throws NoSuchFieldException, SecurityException {
    final EntityPersister persister = sfi.getMappingMetamodel().getEntityDescriptor(resourceType);
    final EntityMetamodel metamodel = persister.getEntityMetamodel();
    final List<String> declaredAttributeNames = locateDeclaredAttributeNames(resourceType);
    final List<String> wrappedAttributes = locateWrappedAttributeNames(metamodel);
    final Map<String, Class<?>> attributeTypes = locateAttributeTypes(metamodel, wrappedAttributes);
    final List<String> attributesToBeUnwrapped = new ArrayList<>(wrappedAttributes);

    unwrapAttributes(metamodel, attributesToBeUnwrapped, attributeTypes);

    final List<String> nonLazyAttributes =
        locateNonLazyAttributes(resourceType, metamodel, wrappedAttributes);
    final Map<String, DomainAssociation> associations =
        locateAssociations(metamodel, wrappedAttributes);
    final Map<String, ComponentPath> componentPaths =
        resolveComponentPaths(metamodel, wrappedAttributes);

    return new DomainResourceAttributesMetadataImpl<>(
        resourceType,
        declaredAttributeNames,
        wrappedAttributes,
        attributesToBeUnwrapped,
        attributeTypes,
        nonLazyAttributes,
        componentPaths,
        associations,
        domainResourceContext);
  }

  public Map<String, ComponentPath> resolveComponentPaths(
      EntityMetamodel metamodel,
      List<String> wrappedAttributes) {

    if (logger.isTraceEnabled()) {
      logger.trace("Resolving component paths");
    }

    final int span = wrappedAttributes.size();
    final Map<String, ComponentPath> paths = new HashMap<>();

    for (int i = 0; i < span; i++) {
      final String attributeName = wrappedAttributes.get(i);
      final Type type = metamodel.getIdentifierProperty().getName().equals(attributeName)
          ? metamodel.getIdentifierProperty().getType()
          : metamodel.getPropertyTypes()[metamodel.getPropertyIndex(attributeName)];

      if (!type.getClass().isAssignableFrom(ComponentType.class)) {
        continue;
      }

      paths.putAll(individuallyResolveComponentPath(new ComponentPathImpl(), attributeName, type));
    }

    return paths;
  }

  private Map<String, ComponentPathImpl> individuallyResolveComponentPath(
      ComponentPathImpl currentPath,
      String attributeName,
      Type type) {
    final ComponentPathImpl root =
        of(new ComponentPathImpl(currentPath)).consume(self -> self.add(attributeName)).get();
    final Map<String, ComponentPathImpl> paths = new HashMap<>();

    paths.put(attributeName, root);

    if (!(type instanceof ComponentType)) {
      return paths;
    }

    final ComponentType componentType = (ComponentType) type;
    final int span = componentType.getPropertyNames().length;

    for (int i = 0; i < span; i++) {

      of(new ComponentPathImpl(root))
          .second(componentType.getPropertyNames()[i])
          .third(componentType.getSubtypes()[i])
          .tryMap(this::individuallyResolveComponentPath)
          .consume(paths::putAll);

    }

    return paths;
  }

  private AssociationForm resolveAssociationType(org.hibernate.type.AssociationType type) {

    if (type instanceof CollectionType) {
      return AssociationForm.PLURAL;
    }

    if (type instanceof EntityType) {
      return AssociationForm.SINGULAR;
    }

    throw new IllegalArgumentException(
        String.format("Unknown association type [%s]", type.getName()));
  }

  private Map<String, DomainAssociation> locateAssociations(
      EntityMetamodel metamodel,
      List<String> wrappedAttributeNames) {

    if (logger.isTraceEnabled()) {
      logger.trace("Locating association");
    }

    final Map<String, DomainAssociation> associations = new HashMap<>();

    for (final String attributeName : wrappedAttributeNames) {

      if (attributeName.equals(metamodel.getIdentifierProperty().getName())) {
        continue;
      }

      final int attributeIndex = metamodel.getPropertyIndex(attributeName);

      associations.putAll(
          individuallyLocateAssociations(
              attributeName,
              attributeIndex,
              metamodel.getPropertyTypes()[attributeIndex],
              metamodel.getPropertyNullability()));

    }

    return associations.isEmpty()
        ? Collections.emptyMap()
        : associations;
  }

  private Map<String, DomainAssociation> individuallyLocateAssociations(
      String attributeName,
      int attributeIndex,
      Type type,
      boolean[] nullabilities) {
    final Map<String, DomainAssociation> associations = new HashMap<>();

    if (type instanceof org.hibernate.type.AssociationType hbmAssociationType) {
      final AssociationForm associationType = resolveAssociationType(hbmAssociationType);

      if (nullabilities[attributeIndex]) {
        associations.put(
            attributeName,
            new DomainAssociation.OptionalAssociation(
                attributeName,
                associationType));

        return associations;
      }

      associations.put(
          attributeName, new DomainAssociation.MandatoryAssociation(
              attributeName, associationType));

      return associations;

    }

    if (type instanceof ComponentType componentType) {
      final String[] subAttributes = componentType.getPropertyNames();
      final Type[] subtypes = componentType.getSubtypes();
      final int span = subAttributes.length;

      for (int i = 0; i < span; i++) {
        int propertyIndex = componentType.getPropertyIndex(subAttributes[i]);

        associations.putAll(
            individuallyLocateAssociations(
                subAttributes[i],
                propertyIndex,
                subtypes[propertyIndex],
                componentType.getPropertyNullability()));

      }

      return associations;
    }

    return Collections.emptyMap();
  }

  @SuppressWarnings("squid:S135")
  private List<String> locateNonLazyAttributes(
      Class<D> resourceType,
      EntityMetamodel metamodel,
      List<String> wrappedAttributeNames) throws NoSuchFieldException, SecurityException {

    if (logger.isTraceEnabled()) {
      logger.trace("Locating non-lazy attributes");
    }

    final List<String> nonLazyAttributes = new ArrayList<>();
    final int span = wrappedAttributeNames.size();
    final Type identifierType = metamodel.getIdentifierProperty().getType();
    final Type[] attributeHBMTypes = metamodel.getPropertyTypes();

    for (int i = 0; i < span; i++) {
      final String attributeName = wrappedAttributeNames.get(i);

      if (attributeName.equals(metamodel.getIdentifierProperty().getName())) {
        nonLazyAttributes.add(attributeName);

        if (isComponent(identifierType)) {
          nonLazyAttributes.addAll(
              locateNonLazyAttributesOfComponent(resourceType, (ComponentType) identifierType));
        }

        continue;
      }

      final Type attributeType = attributeHBMTypes[metamodel.getPropertyIndex(attributeName)];

      if (!isComponent(attributeType)) {

        if (isLazy(
            resourceType, attributeName, attributeType, i, metamodel.getPropertyLaziness())) {
          continue;
        }

        nonLazyAttributes.add(attributeName);
        continue;
      }

      nonLazyAttributes
          .addAll(locateNonLazyAttributesOfComponent(resourceType, (ComponentType) attributeType));
    }

    return nonLazyAttributes.isEmpty()
        ? Collections.emptyList()
        : nonLazyAttributes;
  }

  private List<String> locateNonLazyAttributesOfComponent(
      Class<?> owningType,
      ComponentType componentType) throws NoSuchFieldException, SecurityException {
    final List<String> nonLazyAttributes = new ArrayList<>();

    // assumes all properties in a component is eager

    final boolean[] componentAttributeLaziness =
        of(new boolean[componentType.getPropertyNames().length])
            .second(false)
            .consume(Arrays::fill)
            .get();

    for (final String componentAttributeName : componentType.getPropertyNames()) {
      final int componentAttributeIndex = componentType.getPropertyIndex(componentAttributeName);
      final Type componentAttributeType = componentType.getSubtypes()[componentAttributeIndex];

      if (isComponent(componentAttributeType)) {
        nonLazyAttributes.addAll(
            locateNonLazyAttributesOfComponent(
                componentType.getReturnedClass(), (ComponentType) componentAttributeType));
        continue;
      }

      if (!isLazy(
          owningType, componentAttributeName, componentAttributeType, componentAttributeIndex,
          componentAttributeLaziness)) {
        nonLazyAttributes.add(componentAttributeName);
      }
    }

    return nonLazyAttributes.isEmpty()
        ? Collections.emptyList()
        : nonLazyAttributes;
  }

  private boolean isLazy(
      Class<?> owningType,
      String name,
      Type type,
      int index,
      boolean[] propertyLaziness) throws NoSuchFieldException, SecurityException {

    if (!org.hibernate.type.AssociationType.class.isAssignableFrom(type.getClass())) {
      return propertyLaziness[index];
    }

    if (EntityType.class.isAssignableFrom(type.getClass())) {
      return !((EntityType) type).isEager(null);
    }

    if (CollectionType.class.isAssignableFrom(type.getClass())) {
      final Class<?> genericType =
          (Class<?>) TypeUtils.getGenericType(owningType.getDeclaredField(name));

      if (!Entity.class.isAssignableFrom(genericType)
          && !FileResource.class.isAssignableFrom(genericType)) {
        return propertyLaziness[index];
      }
      // this is how a CollectionPersister role is resolved by Hibernate
      // see org.hibernate.cfg.annotations.CollectionBinder.bind()
      String collectionRole =
          org.hibernate.internal.util.StringHelper.qualify(owningType.getName(), name);

      return sfi.getMappingMetamodel().getCollectionDescriptor(collectionRole).isLazy();
    }

    return propertyLaziness[index];
  }

  @SuppressWarnings("squid:S135")
  private void unwrapAttributes(
      EntityMetamodel entityMetamodel,
      List<String> attributesToBeUnwrapped,
      Map<String, Class<?>> attributeTypes) {

    if (logger.isTraceEnabled()) {
      logger.trace("Unwrapping attributes");
    }

    final int span = attributesToBeUnwrapped.size();
    final Type identifierType = entityMetamodel.getIdentifierProperty().getType();
    final Type[] attributeHBMTypes = entityMetamodel.getPropertyTypes();

    for (int i = 0; i < span; i++) {
      final String attributeName = attributesToBeUnwrapped.get(i);

      if (entityMetamodel.getIdentifierProperty().getName().equals(attributeName)) {

        if (isComponent(identifierType)) {
          unwrapComponentAttribute(
              (ComponentType) identifierType, attributesToBeUnwrapped, attributeTypes);
        }

        continue;
      }

      final Type attributeType = attributeHBMTypes[entityMetamodel.getPropertyIndex(attributeName)];

      if (!isComponent(attributeType)) {
        continue;
      }

      unwrapComponentAttribute(
          (ComponentType) attributeType, attributesToBeUnwrapped, attributeTypes);
    }
  }

  private void unwrapComponentAttribute(
      ComponentType componentType,
      List<String> attributesToBeUnwrapped,
      Map<String, Class<?>> attributeTypes) {
    final Type[] componentAttributeTypes = componentType.getSubtypes();
    final List<String> nestedComponentAttributes = new ArrayList<>(0);

    for (final String componentAttribute : componentType.getPropertyNames()) {
      final Type componentAttributeType =
          componentAttributeTypes[componentType.getPropertyIndex(componentAttribute)];

      nestedComponentAttributes.add(componentAttribute);
      attributeTypes.put(componentAttribute, componentAttributeType.getReturnedClass());

      if (!isComponent(componentAttributeType)) {
        continue;
      }

      unwrapComponentAttribute(
          (ComponentType) componentAttributeType, nestedComponentAttributes, attributeTypes);
    }

    attributesToBeUnwrapped.addAll(nestedComponentAttributes);
  }

  private boolean isComponent(Type type) {
    return ComponentType.class.isAssignableFrom(type.getClass());
  }

  private Map<String, Class<?>> locateAttributeTypes(
      EntityMetamodel metamodel,
      List<String> wrappedAttributeNames) {

    if (logger.isTraceEnabled()) {
      logger.trace("Locating attribute types");
    }

    final int span = wrappedAttributeNames.size();
    final Type[] propertyTypes = metamodel.getPropertyTypes();
    final Map<String, Class<?>> attributeTypes = new HashMap<>(0);

    for (int i = 0; i < span; i++) {
      final String attributeName = wrappedAttributeNames.get(i);

      if (metamodel.getIdentifierProperty().getName().equals(attributeName)) {
        attributeTypes
            .put(attributeName, metamodel.getIdentifierProperty().getType().getReturnedClass());
        continue;
      }

      attributeTypes.put(
          attributeName,
          propertyTypes[metamodel.getPropertyIndex(attributeName)].getReturnedClass());
    }

    return attributeTypes;
  }

  private List<String> locateDeclaredAttributeNames(
      Class<D> resourceType) {
    return new ArrayList<>(
        sfi.getJpaMetamodel().entity(resourceType)
            .getDeclaredAttributes().stream()
            .map(Attribute::getName).toList());
  }

  private List<String> locateWrappedAttributeNames(EntityMetamodel metamodel) {

    if (logger.isTraceEnabled()) {
      logger.trace("Locating wrapped attributes");
    }

    return of(getNonIdentifierAttributes(metamodel))
        .second(metamodel)
        .map(this::addIdentifierAttribute)
        .get();

  }

  private List<String> getNonIdentifierAttributes(EntityMetamodel metamodel) {
    return new ArrayList<>(List.of(metamodel.getPropertyNames()));
  }

  private List<String> addIdentifierAttribute(
      List<String> attributesToBeJoinnedWithIdentifier,
      EntityMetamodel metamodel) {
    attributesToBeJoinnedWithIdentifier.add(metamodel.getIdentifierProperty().getName());
    return attributesToBeJoinnedWithIdentifier;
  }

}
