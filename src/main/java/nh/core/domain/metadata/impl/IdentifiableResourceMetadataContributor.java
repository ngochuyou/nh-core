package nh.core.domain.metadata.impl;

import java.io.Serializable;
import nh.core.domain.IdentifiableResource;
import nh.core.domain.metadata.IdentifiableResourceMetadata;

/**
 * @author Ngoc Huy
 */
public interface IdentifiableResourceMetadataContributor<S extends Serializable, I extends IdentifiableResource<S>, E extends Exception>
    extends DomainResourceMetadataContributor<I, IdentifiableResourceMetadata<S, I>, E> {

}
