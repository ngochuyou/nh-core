package nh.core.domain.metadata.impl;

import nh.core.domain.DomainResourceContext;
import nh.core.domain.NamedResource;
import nh.core.domain.metadata.NamedResourceMetadata;

/**
 * @author Ngoc Huy
 */
class NamedResourceMetadataImpl<N extends NamedResource>
    extends AbstractDomainResourceMetadata<N> implements NamedResourceMetadata<N> {

  private final Naming scopedAttribute;

  NamedResourceMetadataImpl(
      Class<N> resourceType,
      Naming scopedAttribute,
      DomainResourceContext resourceContext) {
    super(resourceType, resourceContext);
    this.scopedAttribute = scopedAttribute;
  }

  @Override
  public Naming getScopedAttributeNames() {
    return scopedAttribute;
  }

}
