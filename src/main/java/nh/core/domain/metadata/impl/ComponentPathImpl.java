/**
 *
 */
package nh.core.domain.metadata.impl;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.stream.Collectors;
import nh.core.constants.Symbols;
import nh.core.domain.metadata.ComponentPath;

/**
 * @author Ngoc Huy
 *
 */
class ComponentPathImpl implements ComponentPath {

  private final Queue<String> path;

  ComponentPathImpl() {
    path = new ArrayDeque<>();
  }

  public ComponentPathImpl(String root) {
    path = new ArrayDeque<>();
    path.add(root);
  }

  public ComponentPathImpl(ComponentPathImpl parent) {
    this();
    path.addAll(parent.path);
  }

  @Override
  public void add(String component) {
    path.add(component);
  }

  @Override
  public Queue<String> getPath() {
    return path;
  }

  @Override
  public String toString() {
    return path.stream().collect(Collectors.joining(String.valueOf(Symbols.DOT)));
  }

}
