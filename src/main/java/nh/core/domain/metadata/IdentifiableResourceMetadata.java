package nh.core.domain.metadata;

import java.io.Serializable;
import nh.core.domain.IdentifiableResource;

public interface IdentifiableResourceMetadata<S extends Serializable, I extends IdentifiableResource<S>>
    extends DomainResourceMetadata<I> {

  boolean isIdentifierAutoGenerated();

}
