/**
 *
 */
package nh.core.domain.metadata;

import java.util.Collection;

/**
 * @author Ngoc Huy
 *
 */
public enum AssociationForm {

  /**
   * Singular, a single entity
   */
  SINGULAR,

  /**
   * Plural, could be a {@link Collection} or an array
   */
  PLURAL

}
