package nh.core.domain.metadata;

import java.util.Map;
import nh.core.domain.DomainResource;

public interface InternationalizedAttributesMetadata<D extends DomainResource> extends
    DomainResourceMetadata<D> {

  Map<String, String> getNameMappings();

}
