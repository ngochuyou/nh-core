package nh.core.domain.metadata;

import java.lang.reflect.Field;
import nh.core.domain.PermanentResource;

/**
 * @author Ngoc Huy
 */
public interface PermanentResourceMetadata<P extends PermanentResource>
    extends DomainResourceMetadata<P> {

  Field getScopedField();

}
