package nh.core.domain.metadata;

import java.util.regex.Pattern;
import nh.core.BundledMessage;

public interface MessageSourceArgumentProvider {

  BundledMessage getArgs(String fieldName, Pattern pattern);

}
