package nh.core.domain.metadata;

import nh.core.domain.NamedResource;
import nh.core.domain.metadata.impl.Naming;

/**
 * @author Ngoc Huy
 */
public interface NamedResourceMetadata<N extends NamedResource> extends DomainResourceMetadata<N> {

  /**
   * @return attributes that were scoped to be used in {@link NamedResource} logics
   */
  Naming getScopedAttributeNames();

}