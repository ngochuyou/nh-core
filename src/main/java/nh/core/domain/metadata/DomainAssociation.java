/**
 *
 */
package nh.core.domain.metadata;

import java.util.Objects;

/**
 * @author Ngoc Huy
 *
 */
public interface DomainAssociation {

  String getName();

  AssociationForm getType();

  abstract class AbstractAssociation implements DomainAssociation {

    private final String name;
    private final AssociationForm type;

    private AbstractAssociation(String name, AssociationForm type) {
      this.name = Objects.requireNonNull(name);
      this.type = Objects.requireNonNull(type);
    }

    @Override
    public AssociationForm getType() {
      return type;
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }

      AbstractAssociation that = (AbstractAssociation) o;

      if (!Objects.equals(name, that.name)) {
        return false;
      }
      return type == that.type;
    }

    @Override
    public int hashCode() {
      int result = name.hashCode();
      result = 31 * result + type.hashCode();
      return result;
    }
  }

  class OptionalAssociation extends AbstractAssociation {

    public OptionalAssociation(String name, AssociationForm type) {
      super(name, type);
    }

  }

  class MandatoryAssociation extends AbstractAssociation {

    public MandatoryAssociation(String name, AssociationForm type) {
      super(name, type);
    }

  }

}
