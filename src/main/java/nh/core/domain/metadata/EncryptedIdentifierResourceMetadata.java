package nh.core.domain.metadata;

import java.io.Serializable;
import java.lang.reflect.Field;
import nh.core.domain.EncryptedIdentifierResource;

/**
 * @author Ngoc Huy
 */
public interface EncryptedIdentifierResourceMetadata<S extends Serializable, E extends EncryptedIdentifierResource<S>>
    extends DomainResourceMetadata<E> {

  Field getScopedField();

}
