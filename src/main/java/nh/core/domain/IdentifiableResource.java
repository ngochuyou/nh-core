package nh.core.domain;

import java.io.Serializable;

public interface IdentifiableResource<S extends Serializable> extends DomainResource {

  S getId();

  void setId(S id);

}
