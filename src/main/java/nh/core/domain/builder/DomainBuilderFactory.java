/**
 *
 */
package nh.core.domain.builder;

import nh.core.domain.DomainResource;

/**
 * @author Ngoc Huy
 *
 */
public interface DomainBuilderFactory {

	<E extends DomainResource> DomainBuilder<E> getBuilder(Class<E> resourceClass);

}
