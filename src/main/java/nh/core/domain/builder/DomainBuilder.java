/**
 *
 */
package nh.core.domain.builder;

import jakarta.persistence.EntityManager;
import nh.core.domain.DomainResource;
import nh.core.domain.graph.GraphLogic;

/**
 * @author Ngoc Huy
 *
 */
public interface DomainBuilder<T extends DomainResource> extends GraphLogic<T> {

	/**
	 * Occurs only when the entity is being inserted
	 * </p>
	 * <em>Example:</em> While inserting an user we always hash the password.
	 * Whereas an update has to perform a check to determine if the user is updating
	 * their password or not, then make the decision to hash/update that password
	 */
	T buildInsertion(T persistence, EntityManager entityManager);

	T buildUpdate(T model, T persistence, EntityManager entityManger);

	<E extends T> DomainBuilder<E> and(DomainBuilder<E> next);

	@Override
	default <E extends T> GraphLogic<E> and(GraphLogic<E> next) {
		return and((DomainBuilder<E>) next);
	}

}
