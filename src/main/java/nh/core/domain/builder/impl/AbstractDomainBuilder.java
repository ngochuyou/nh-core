/**
 *
 */
package nh.core.domain.builder.impl;

import jakarta.persistence.EntityManager;
import nh.core.domain.DomainResource;
import nh.core.domain.builder.DomainBuilder;

/**
 * @author Ngoc Huy
 *
 */
public abstract class AbstractDomainBuilder<D extends DomainResource> implements DomainBuilder<D> {

	@Override
	public <E extends D> DomainBuilder<E> and(DomainBuilder<E> next) {
		return new CompositeDomainBuilder<>(this, next);
	}

	private static class CompositeDomainBuilder<D extends DomainResource, T extends D>
			extends AbstractDomainBuilder<T> {

		private final DomainBuilder<D> parentBuilder;
		private final DomainBuilder<T> childBuilder;

		public CompositeDomainBuilder(DomainBuilder<D> parentBuilder,
				DomainBuilder<T> childBuilder) {
			super();
			this.parentBuilder = parentBuilder;
			this.childBuilder = childBuilder;
		}

		@SuppressWarnings("unchecked")
		@Override
		public T buildInsertion(T model, EntityManager entityManager) {
			return childBuilder.buildInsertion((T) parentBuilder.buildInsertion(model, entityManager), entityManager);
		}

		@SuppressWarnings("unchecked")
		@Override
		public T buildUpdate(T model, T persistence, EntityManager entityManger) {
			return childBuilder.buildUpdate(model, (T) parentBuilder.buildUpdate(model, persistence, entityManger),
					entityManger);
		}

		@Override
		public String getLoggableName() {
			return String.format("[%s, %s]", parentBuilder.getLoggableName(), childBuilder.getLoggableName());
		}

	}

}
