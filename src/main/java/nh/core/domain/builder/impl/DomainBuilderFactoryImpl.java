package nh.core.domain.builder.impl;

import jakarta.persistence.EntityManager;
import java.util.List;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.IdentifiableResource;
import nh.core.domain.NamedResource;
import nh.core.domain.PermanentResource;
import nh.core.domain.builder.DomainBuilder;
import nh.core.domain.builder.DomainBuilderFactory;
import nh.core.domain.graph.GraphLogic;
import nh.core.domain.graph.impl.AbstractDomainGraphLogicContext;
import nh.core.domain.mapping.DomainMapping;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

public class DomainBuilderFactoryImpl
    extends AbstractDomainGraphLogicContext implements DomainBuilderFactory {

  public DomainBuilderFactoryImpl(AutowireCapableBeanFactory beanFactory,
      DomainResourceContext domainContext,
      String basePackage) {
    super(DomainBuilder.class, beanFactory, domainContext, basePackage);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <E extends DomainResource> DomainBuilder<E> getBuilder(Class<E> domainType) {
    return getLogic(domainType)
        .map(DomainBuilder.class::cast)
        .orElseThrow(() -> new IllegalStateException(
            String.format("Unable to locate %s for %s", DomainBuilder.class.getSimpleName(),
                domainType)));
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected GraphLogic getNoOpLogic() {
    return NO_OP_BUILDER;
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected boolean isNotNoOp(GraphLogic candidate) {
    return !NO_OP_BUILDER.equals(candidate);
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected List<DomainMapping> getFixedLogics(AutowireCapableBeanFactory beanFactory) {
    return List.of(
        DomainMapping.of(IdentifiableResource.class,
            beanFactory.createBean(IdentifiableDomainBuilder.class)),
        DomainMapping.of(NamedResource.class, new NamedDomainBuilder()),
        DomainMapping.of(PermanentResource.class, new PermanentDomainBuilder()));
  }

  @SuppressWarnings("rawtypes")
  private static final DomainBuilder NO_OP_BUILDER = new AbstractDomainBuilder() {
    @Override
    public DomainResource buildInsertion(DomainResource persistence, EntityManager entityManager) {
      return persistence;
    }

    @Override
    public DomainResource buildUpdate(DomainResource model, DomainResource persistence,
        EntityManager entityManger) {
      return persistence;
    }

    @Override
    public String getLoggableName() {
      return "<<NO_OP>>";
    }
  };

}
