package nh.core.domain.builder.impl;

import jakarta.persistence.EntityManager;
import nh.core.domain.NamedResource;
import nh.core.util.StringUtils;

class NamedDomainBuilder extends AbstractDomainBuilder<NamedResource> {

  NamedDomainBuilder() {}

  @Override
  public NamedResource buildInsertion(NamedResource persistence, EntityManager entityManager) {
    persistence.setName(StringUtils.normalizeString(persistence.getName()));
    return persistence;
  }

  @Override
  public NamedResource buildUpdate(NamedResource model, NamedResource persistence,
      EntityManager entityManger) {
    persistence.setName(StringUtils.normalizeString(model.getName()));
    return persistence;
  }
}
