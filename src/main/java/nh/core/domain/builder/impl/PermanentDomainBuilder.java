package nh.core.domain.builder.impl;

import jakarta.persistence.EntityManager;
import nh.core.domain.PermanentResource;

class PermanentDomainBuilder extends AbstractDomainBuilder<PermanentResource> {

  PermanentDomainBuilder() {
  }

  @Override
  public PermanentResource buildInsertion(PermanentResource persistence,
      EntityManager entityManager) {
    persistence.setDeleted(Boolean.FALSE);
    return persistence;
  }

  @Override
  public PermanentResource buildUpdate(PermanentResource model, PermanentResource persistence,
      EntityManager entityManger) {
    return persistence;
  }
}
