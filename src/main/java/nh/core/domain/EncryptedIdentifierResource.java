package nh.core.domain;

import java.io.Serializable;

/**
 * @author Ngoc Huy
 */
public interface EncryptedIdentifierResource<S extends Serializable>
    extends IdentifiableResource<S> {

  String getCode();

  void setCode(String encryptedIdentifier);

}
