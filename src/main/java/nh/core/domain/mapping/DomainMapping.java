package nh.core.domain.mapping;

import nh.core.domain.DomainResource;

/**
 * Describing a mapping between a {@link DomainResource} class and a {@code T}
 *
 * @param <C> generic type of the {@link DomainResource}
 * @param <T> generic type of any definitions
 */
public interface DomainMapping<C extends DomainResource, T> {

  /**
   * @return the type of the {@link DomainResource}
   */
  Class<C> getDomainType();

  /**
   * @return the mapped instance
   */
  T getInstance();

  static <A extends DomainResource, B> DomainMapping<A, B> of(Class<A> type, B instance) {
    return new DomainMappingImpl<>(type, instance);
  }

}
