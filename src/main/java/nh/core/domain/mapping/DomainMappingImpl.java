package nh.core.domain.mapping;

import nh.core.domain.DomainResource;

class DomainMappingImpl<C extends DomainResource, T> implements DomainMapping<C, T> {

  private final Class<C> domainType;
  private final T instance;

  DomainMappingImpl(Class<C> domainType, T instance) {
    this.domainType = domainType;
    this.instance = instance;
  }

  @Override
  public Class<C> getDomainType() {
    return domainType;
  }

  @Override
  public T getInstance() {
    return instance;
  }
}
