package nh.core.domain.tuplizer.impl;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.graph.impl.DomainResourceGraphCollectors;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.domain.sf.SessionFactoryManager;
import nh.core.domain.tuplizer.DomainResourceTuplizer;
import nh.core.function.TriConsumer;
import nh.core.reflect.Accessor;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.internal.util.collections.CollectionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ngoc Huy
 */
public class TuplizerBuilder {

  private static final Logger logger = LoggerFactory.getLogger(TuplizerBuilder.class);

  private final Map<Class<? extends DomainResource>, DomainResourceTuplizer<? extends DomainResource>> tuplizers =
      new HashMap<>();

  private final DomainResourceContext domainResourceContext;
  private final DomainResourceGraph<DomainResource> resourceGraph;
  private final SessionFactoryManager sessionFactoryManager;

  private final Map<AccessorKey, Accessor> accessorsCache = new HashMap<>();
  private final Map<Class<? extends DomainResource>, DomainResourceTuplizer<? extends DomainResource>> parentTuplizersCache =
      new HashMap<>();

  public TuplizerBuilder(
      DomainResourceContext domainResourceContext,
      DomainResourceGraph<DomainResource> resourceGraph,
      SessionFactoryManager sessionFactoryManager) {
    this.domainResourceContext = domainResourceContext;
    this.resourceGraph = resourceGraph;
    this.sessionFactoryManager = sessionFactoryManager;
  }

  @SuppressWarnings({
      "rawtypes", "unchecked", "squid:S1452"
  })
  public Map<Class<? extends DomainResource>, DomainResourceTuplizer<? extends DomainResource>> build()
      throws NoSuchMethodException {

    if (logger.isTraceEnabled()) {
      logger.trace("Building tuplizers");
    }

    final BiFunction<Class<?>, String, Accessor> cachedAccessorProvider = this::findCachedAccessor;
    final TriConsumer<Class<?>, String, Accessor> accessorEntryConsumer =
        this::cacheAccessor;
    final Function<Class<? extends DomainResource>, DomainResourceTuplizer<?>> parentTuplizerProvider =
        this::locateParentTuplizer;

    for (final Class<? extends DomainResource> resourceType : resourceGraph
        .collect(DomainResourceGraphCollectors.toTypesSet())) {
      final int resourceClassModifiers = resourceType.getModifiers();

      if (Modifier.isAbstract(resourceClassModifiers)
          || Modifier.isInterface(resourceClassModifiers)) {
        continue;
      }

      logger.trace(
          "Resolving {} for resource type {}", DomainResourceTuplizer.class.getName(),
          resourceType.getName());

      final SessionFactoryImplementor sfi =
          sessionFactoryManager.locateSessionFactory(resourceType);
      final DomainResourceMetadata<? extends DomainResource> metadata =
          domainResourceContext.getMetadata(resourceType);

      tuplizers.put(
          resourceType,
          Objects.isNull(sfi)
              ? new DomainResourceTuplizerImpl(
              metadata,
              cachedAccessorProvider,
              accessorEntryConsumer,
              parentTuplizerProvider)
              : new HibernateResourceTuplizer(
                  metadata,
                  sfi,
                  cachedAccessorProvider,
                  accessorEntryConsumer));
    }

    return tuplizers;
  }

  private DomainResourceTuplizer<?> locateParentTuplizer(
      Class<? extends DomainResource> resourceType) {

    if (parentTuplizersCache.containsKey(resourceType)) {
      return parentTuplizersCache.get(resourceType);
    }

    final DomainResourceGraph<?> graph = resourceGraph.locate(resourceType);

    if (CollectionHelper.isEmpty(graph.getParents())) {
      return null;
    }

    for (final DomainResourceGraph<?> possibleParentGraph : graph.getParents()) {
      final Class<?> parentType = possibleParentGraph.getResourceType();

      if (Modifier.isInterface(parentType.getModifiers())) {
        continue;
      }

      final DomainResourceTuplizer<?> parentTuplizer = tuplizers
          .get(parentType);

      parentTuplizersCache.put(resourceType, parentTuplizer);

      return parentTuplizer;
    }

    return null;
  }

  private Accessor findCachedAccessor(Class<?> ownerType, String attributePath) {
    final AccessorKey key = new AccessorKey(ownerType, attributePath);

    if (accessorsCache.containsKey(key)) {
      logger.trace("Using cached accessor for key [{}]", key);
    }

    return accessorsCache.get(key);
  }

  private void cacheAccessor(Class<?> ownerType, String attributePath, Accessor accessor) {
    final AccessorKey key = new AccessorKey(ownerType, attributePath);

    if (logger.isTraceEnabled()) {
      logger.trace("Caching accessor with key [{}]", key);
    }

    accessorsCache.put(new AccessorKey(ownerType, attributePath), accessor);
  }

  private class AccessorKey {

    private final Class<?> resourceType;
    private final String path;

    public AccessorKey(Class<?> resourceType, String path) {
      this.resourceType = resourceType;
      this.path = path;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;

      result = prime * result + getEnclosingInstance().hashCode();
      result = prime * result + ((resourceType == null)
          ? 0
          : resourceType.hashCode());
      result = prime * result + ((path == null)
          ? 0
          : path.hashCode());

      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if ((obj == null) || (getClass() != obj.getClass())) {
        return false;
      }

      final AccessorKey other = (AccessorKey) obj;

      if (!getEnclosingInstance().equals(other.getEnclosingInstance())) {
        return false;
      }

      if (path == null) {
        if (other.path != null) {
          return false;
        }
      } else if (!path.equals(other.path)) {
        return false;
      }

      if (resourceType == null) {
        if (other.resourceType != null) {
          return false;
        }
      } else if (!resourceType.equals(other.resourceType)) {
        return false;
      }

      return true;
    }

    private TuplizerBuilder getEnclosingInstance() {
      return TuplizerBuilder.this;
    }

    @Override
    public String toString() {
      return String.format("%s#%s", resourceType, path);
    }

  }

}
