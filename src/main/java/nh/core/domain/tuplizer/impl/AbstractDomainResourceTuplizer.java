package nh.core.domain.tuplizer.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.function.BiFunction;
import nh.core.domain.DomainResource;
import nh.core.domain.tuplizer.DomainResourceTuplizer;
import nh.core.domain.tuplizer.TuplizerException;
import nh.core.function.HandledSupplier;
import nh.core.function.TriConsumer;
import nh.core.reflect.Accessor;
import nh.core.reflect.Getter;
import nh.core.reflect.Setter;
import nh.core.util.TypeUtils;
import org.springframework.util.Assert;

/**
 * @author Ngoc Huy
 */
abstract class AbstractDomainResourceTuplizer<D extends DomainResource>
    implements DomainResourceTuplizer<D> {

  private final Class<D> resourceType;

  protected AbstractDomainResourceTuplizer(Class<D> resourceType) {
    this.resourceType = resourceType;
  }

  @Override
  public D instantiate(Object... args) throws TuplizerException {

    try {
      return TypeUtils.constructFromNoArgs(resourceType);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
             | InvocationTargetException
             | NoSuchMethodException | SecurityException e) {
      throw new TuplizerException(e);
    }
  }

  @Override
  public void setProperty(Object source, String propName, Object value) throws TuplizerException {

    try {
      final Setter setter = getSetter(propName);

      Assert.notNull(
          setter, String.format(
              "Unable to locate %s for property %s", Setter.class.getSimpleName(), propName));

      setter.set(source, value);
    } catch (Exception e) {
      throw new TuplizerException(e);
    }
  }

  @Override
  public Object getProperty(Object source, String propName) throws TuplizerException {

    try {
      final Getter getter = getGetter(propName);

      Assert.notNull(
          getter, String.format(
              "Unable to locate %s for property %s", Getter.class.getSimpleName(), propName));

      return getter.get(source);
    } catch (Exception e) {
      throw new TuplizerException(e);
    }
  }

  protected abstract Getter getGetter(String propName);

  protected abstract Setter getSetter(String propName);

  protected abstract Map<String, Accessor> getAccessors();

  @Override
  public Class<D> getResourceType() {
    return resourceType;
  }

  protected Accessor locateOnGoingAccessor(
      String attributeIdentifier,
      HandledSupplier<Accessor, NoSuchMethodException> accessorSupplier,
      BiFunction<Class<?>, String, Accessor> cachedAccessorProvider,
      TriConsumer<Class<?>, String, Accessor> accessorEntryConsumer) throws NoSuchMethodException {
    final Accessor accessor = cachedAccessorProvider.apply(resourceType, attributeIdentifier);

    if (accessor != null) {
      return accessor;
    }

    final Accessor newCachedAccessorEntry = accessorSupplier.get();

    accessorEntryConsumer.accept(resourceType, attributeIdentifier, newCachedAccessorEntry);

    return newCachedAccessorEntry;
  }

}
