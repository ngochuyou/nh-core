package nh.core.domain.tuplizer.impl;

import java.lang.reflect.Member;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import nh.core.domain.DomainResource;
import nh.core.domain.metadata.ComponentPath;
import nh.core.domain.metadata.DomainResourceAttributesMetadata;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.function.TriConsumer;
import nh.core.reflect.Accessor;
import nh.core.reflect.Getter;
import nh.core.reflect.Setter;
import nh.core.reflect.impl.AccessorFactory;
import nh.core.util.ReflectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

/**
 * @author Ngoc Huy
 */
class DomainResourceTuplizerImpl<D extends DomainResource>
    extends AbstractDomainResourceTuplizer<D> {

  private static final Logger logger = LoggerFactory.getLogger(DomainResourceTuplizerImpl.class);

  private final Map<String, Accessor> accessors;

  @SuppressWarnings("unchecked")
  DomainResourceTuplizerImpl(
      DomainResourceMetadata<D> metadata,
      BiFunction<Class<?>, String, Accessor> cachedAccessorProvider,
      TriConsumer<Class<?>, String, Accessor> accessorEntryConsumer,
      Function<Class<? extends DomainResource>, AbstractDomainResourceTuplizer<?>> parentTuplizerProvider)
      throws NoSuchMethodException {
    super(metadata.getResourceType());

    logger.trace(
        "Creating {} for type {}", this.getClass().getSimpleName(), metadata.getResourceType());

    accessors = Collections
        .unmodifiableMap(
            resolveAccessors(
                metadata.require(DomainResourceAttributesMetadata.class),
                cachedAccessorProvider, accessorEntryConsumer, parentTuplizerProvider));
  }

  @Override
  protected Map<String, Accessor> getAccessors() {
    return accessors;
  }

  private Map<String, Accessor> resolveAccessors(
      DomainResourceAttributesMetadata<D> metadata,
      BiFunction<Class<?>, String, Accessor> cachedAccessorProvider,
      TriConsumer<Class<?>, String, Accessor> accessorEntryConsumer,
      Function<Class<? extends DomainResource>, AbstractDomainResourceTuplizer<?>> parentTuplizerProvider)
      throws NoSuchMethodException {
    final Map<String, Accessor> accessorCandidates = new HashMap<>();
    final Class<D> resourceType = metadata.getResourceType();
    final AbstractDomainResourceTuplizer<?> parentTuplizer =
        parentTuplizerProvider.apply(resourceType);

    if (parentTuplizer != null) {
      accessorCandidates.putAll(parentTuplizer.getAccessors());
    }

    for (final String attributeName : metadata.getDeclaredAttributeNames()) {
      final Accessor accessor = locateOnGoingAccessor(
          attributeName,
          () -> AccessorFactory.basic(resourceType, attributeName),
          cachedAccessorProvider,
          accessorEntryConsumer);

      accessorCandidates.put(attributeName, accessor);
    }

    if (metadata.getComponentPaths().isEmpty()) {
      return CollectionUtils.isEmpty(accessorCandidates)
          ? Collections.emptyMap()
          : accessorCandidates;
    }

    for (final Entry<String, ComponentPath> componentEntry : metadata.getComponentPaths()
        .entrySet()) {
      final String componentAttributeName = componentEntry.getKey();

      if (accessorCandidates.containsKey(componentAttributeName)) {
        continue;
      }

      final ComponentPath componentPath = componentEntry.getValue();
      final Queue<String> pathQueue = new ArrayDeque<>(componentPath.getPath());

      String currentOwnerName = pathQueue.poll();

      while (!pathQueue.isEmpty()) {
        final Class<?> currentOwnerType = metadata.getAttributeType(currentOwnerName);
        final Accessor currentOwnerAccessor = accessorCandidates.get(currentOwnerName);
        final String currentMemberName = pathQueue.poll();
        final Accessor memberAccessor =
            AccessorFactory.basic(currentOwnerType, currentMemberName);
        final Accessor componentAccessor = locateOnGoingAccessor(
            componentAttributeName,
            () -> AccessorFactory.delegate(
                new ComponentGetter(currentOwnerAccessor.getGetter(), memberAccessor.getGetter()),
                new ComponentSetter(
                    currentOwnerAccessor,
                    () -> ReflectionUtils.locateNoArgsConstructor(currentOwnerType),
                    memberAccessor.getSetter())),
            cachedAccessorProvider, accessorEntryConsumer);

        accessorCandidates.put(currentMemberName, componentAccessor);

        currentOwnerName = currentMemberName;
      }
    }

    return accessorCandidates;
  }

  @Override
  protected Getter getGetter(String propName) {
    return accessors.get(propName).getGetter();
  }

  @Override
  protected Setter getSetter(String propName) {
    return accessors.get(propName).getSetter();
  }

  private static class ComponentSetter implements Setter {

    private static final Member MEMBER;

    static {

      try {
        MEMBER = ComponentSetter.class.getDeclaredMethod("set", Object.class, Object.class);
      } catch (NoSuchMethodException | SecurityException any) {
        throw new IllegalArgumentException(any);
      }
    }

    private final Accessor ownerAccessor;
    private final Supplier<Object> ownerSupplier;
    private final Setter memberSetter;

    public ComponentSetter(
        Accessor ownerAccessor,
        Supplier<Object> ownerSupplier,
        Setter memberSetter) {
      this.ownerAccessor = ownerAccessor;
      this.ownerSupplier = ownerSupplier;
      this.memberSetter = memberSetter;
    }

    @Override
    public Member getMember() {
      return MEMBER;
    }

    @Override
    public void set(Object source, Object val) {
      Object owner = ownerAccessor.get(source);

      if (owner == null) {
        owner = ownerSupplier.get();
        ownerAccessor.set(source, owner);
      }

      memberSetter.set(owner, val);
    }

  }

  private static class ComponentGetter implements Getter {

    private static final Member MEMBER;

    static {

      try {
        MEMBER = ComponentGetter.class.getDeclaredMethod("get", Object.class);
      } catch (NoSuchMethodException | SecurityException any) {
        throw new IllegalArgumentException(any);
      }
    }

    private final Getter ownerGetter;
    private final Getter memberGetter;

    public ComponentGetter(Getter ownerGetter, Getter memberGetter) {
      this.ownerGetter = ownerGetter;
      this.memberGetter = memberGetter;
    }

    @Override
    public Member getMember() {
      return MEMBER;
    }

    @Override
    public Object get(Object source) {
      final Object owner = ownerGetter.get(source);

      if (owner == null) {
        return null;
      }

      return memberGetter.get(owner);
    }

    @Override
    public Class<?> getReturnedType() {
      return memberGetter.getReturnedType();
    }

  }

}
