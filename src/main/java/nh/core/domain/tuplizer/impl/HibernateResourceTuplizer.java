package nh.core.domain.tuplizer.impl;

import java.io.Serializable;
import java.lang.reflect.Member;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import nh.core.domain.Entity;
import nh.core.domain.metadata.ComponentPath;
import nh.core.domain.metadata.DomainResourceAttributesMetadata;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.domain.tuplizer.TuplizerException;
import nh.core.function.TriConsumer;
import nh.core.reflect.Accessor;
import nh.core.reflect.Getter;
import nh.core.reflect.Setter;
import nh.core.reflect.impl.AccessorFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.metamodel.spi.EntityInstantiator;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.persister.entity.UniqueKeyLoadable;
import org.hibernate.type.ComponentType;
import org.hibernate.type.Type;
import org.springframework.util.CollectionUtils;

/**
 * @author Ngoc Huy
 */
class HibernateResourceTuplizer<I extends Serializable, E extends Entity<I>>
    extends AbstractDomainResourceTuplizer<E> {

  private final Map<String, Accessor> accessors;

  private final EntityInstantiator entityInstantiator;

  @SuppressWarnings("unchecked")
  HibernateResourceTuplizer(
      DomainResourceMetadata<E> metadata,
      SessionFactoryImplementor sfi,
      BiFunction<Class<?>, String, Accessor> cachedAccessorProvider,
      TriConsumer<Class<?>, String, Accessor> accessorEntryConsumer) throws NoSuchMethodException {
    super(metadata.getResourceType());

    final EntityPersister entityPersister =
        sfi.getMappingMetamodel().getEntityDescriptor(metadata.getResourceType());

    this.entityInstantiator = entityPersister.getRepresentationStrategy()
        .getInstantiator();
    this.accessors = Collections.unmodifiableMap(
        resolveRootAccessors(
            entityPersister,
            metadata.require(DomainResourceAttributesMetadata.class).getComponentPaths(),
            cachedAccessorProvider,
            accessorEntryConsumer));
  }

  private Map<String, Accessor> resolveRootAccessors(
      EntityPersister persister,
      Map<String, ComponentPath> componentPaths,
      BiFunction<Class<?>, String, Accessor> cachedAccessorProvider,
      TriConsumer<Class<?>, String, Accessor> accessorEntryConsumer) throws NoSuchMethodException {
    final Map<String, Accessor> rootAccessors = new HashMap<>();

    rootAccessors.putAll(
        resolveIdentifierAccessors(
            persister, componentPaths, cachedAccessorProvider, accessorEntryConsumer));

    for (final String attributeName : persister.getPropertyNames()) {
      final Accessor ownerAccessor = locateOnGoingAccessor(
          attributeName,
          () -> buildBasicTypeAccessor(attributeName, persister),
          cachedAccessorProvider,
          accessorEntryConsumer);

      rootAccessors.put(attributeName, ownerAccessor);

      final Type attributeType = persister.getPropertyType(attributeName);

      if (!attributeType.isComponentType()) {
        continue;
      }

      final ComponentType componentType = (ComponentType) attributeType;

      rootAccessors.putAll(
          resolveComponentAccessors(
              ownerAccessor,
              componentType,
              componentPaths,
              cachedAccessorProvider,
              accessorEntryConsumer));
    }

    return CollectionUtils.isEmpty(rootAccessors)
        ? Collections.emptyMap()
        : rootAccessors;
  }

  private Map<String, Accessor> resolveIdentifierAccessors(
      EntityPersister persister,
      Map<String, ComponentPath> componentPaths,
      BiFunction<Class<?>, String, Accessor> cachedAccessorProvider,
      TriConsumer<Class<?>, String, Accessor> accessorEntryConsumer) throws NoSuchMethodException {
    final HashMap<String, Accessor> identifierAccessors = new HashMap<>();
    final String identifierPropertyName = persister.getIdentifierPropertyName();
    final Accessor identifierAccessor = locateOnGoingAccessor(
        persister.getIdentifierPropertyName(),
        () -> buildBasicTypeAccessor(identifierPropertyName, persister),
        cachedAccessorProvider,
        accessorEntryConsumer);

    if (!persister.getIdentifierType().isComponentType()) {
      return identifierAccessors;
    }

    final ComponentType identifierType = (ComponentType) persister.getIdentifierType();

    identifierAccessors.putAll(
        resolveComponentAccessors(
            identifierAccessor,
            identifierType,
            componentPaths,
            cachedAccessorProvider,
            accessorEntryConsumer));

    return identifierAccessors;
  }

  private Map<String, Accessor> resolveComponentAccessors(
      Accessor ownerAccessor,
      ComponentType type,
      Map<String, ComponentPath> componentPaths,
      BiFunction<Class<?>, String, Accessor> cachedAccessorProvider,
      TriConsumer<Class<?>, String, Accessor> accessorEntryConsumer) throws NoSuchMethodException {
    final Map<String, Accessor> componentAccessors = new HashMap<>();

    for (final String componentAttributeName : type.getPropertyNames()) {
      final Type componentAttributeType =
          type.getSubtypes()[type.getPropertyIndex(componentAttributeName)];
      final Accessor nextOwnerAccessor = locateOnGoingAccessor(
          componentPaths.get(componentAttributeName).toString(),
          () -> AccessorFactory.delegate(
              new ComponentGetter(ownerAccessor.getGetter(), type, componentAttributeName),
              new ComponentSetter(ownerAccessor, type, componentAttributeName)),
          cachedAccessorProvider,
          accessorEntryConsumer);

      if (!componentAttributeType.isComponentType()) {
        continue;
      }

      componentAccessors.putAll(
          resolveComponentAccessors(
              nextOwnerAccessor,
              (ComponentType) componentAttributeType,
              componentPaths,
              cachedAccessorProvider,
              accessorEntryConsumer));
    }

    return componentAccessors;
  }

  private Accessor buildBasicTypeAccessor(String attributeName, EntityPersister persister) {
    final UniqueKeyLoadable loadable = (UniqueKeyLoadable) persister;

    return AccessorFactory.delegate(
        new HibernateGetter(persister, loadable, attributeName),
        new HibernateSetter(persister, loadable, attributeName));
  }

  @SuppressWarnings("unchecked")
  @Override
  public E instantiate(Object... args) throws TuplizerException {

    if (couldUseHibernateEngine(args)) {
      return (E) entityInstantiator
          .instantiate((SessionFactoryImplementor) args[0]);
    }

    return super.instantiate(args);
  }

  private boolean couldUseHibernateEngine(Object[] args) {

    if (Objects.isNull(args) || args.length != 1) {
      return false;
    }

    return args[0] instanceof SessionFactoryImplementor;
  }

  @Override
  protected Getter getGetter(String propName) {
    return accessors.get(propName).getGetter();
  }

  @Override
  protected Setter getSetter(String propName) {
    return accessors.get(propName).getSetter();
  }

  @Override
  protected Map<String, Accessor> getAccessors() {
    return accessors;
  }

  private class HibernateGetter implements Getter {

    private final Function<Object, Object> getter;

    private HibernateGetter(
        EntityPersister entityPersister,
        UniqueKeyLoadable loadable,
        String attributeName) {

      if (entityPersister.getIdentifierPropertyName().equals(attributeName)) {
        getter = source -> entityPersister.getIdentifierMapping()
            .getIdentifier(source);
        return;
      }

      final org.hibernate.property.access.spi.Getter nonIdentifierGetter = entityPersister
          .getAttributeMapping(loadable.getPropertyIndex(attributeName))
          .getPropertyAccess()
          .getGetter();

      getter = nonIdentifierGetter::get;
    }

    @Override
    public Member getMember() {

      try {
        return this.getClass().getDeclaredField("getter");
      } catch (NoSuchFieldException | SecurityException any) {
        any.printStackTrace();
        return null;
      }
    }

    @Override
    public Object get(Object source) {
      return getter.apply(source);
    }

    @Override
    public Class<?> getReturnedType() {
      return Object.class;
    }

  }

  private class HibernateSetter implements Setter {

    private final BiConsumer<Object, Object> setter;

    public HibernateSetter(
        EntityPersister persister,
        UniqueKeyLoadable loadable,
        String attributeName) {
      setter = loadable.getIdentifierPropertyName().equals(attributeName)
          ? (source, val) -> persister
          .setIdentifier(source, val, null)
          : (source, val) -> persister
              .setValue(source, loadable.getPropertyIndex(attributeName), val);
    }

    @Override
    public Member getMember() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void set(Object source, Object val) {
      setter.accept(source, val);
    }

  }

  private class ComponentGetter implements Getter {

    private final Getter ownerGetter;
    private final ComponentType ownerType;
    private final int index;

    private final Member member;

    private ComponentGetter(Getter ownerGetter, ComponentType ownerType, String attributeName)
        throws NoSuchMethodException, SecurityException {
      this.ownerGetter = ownerGetter;
      this.ownerType = ownerType;
      this.index = ownerType.getPropertyIndex(attributeName);
      member = this.getClass().getDeclaredMethod("get", Object.class);
    }

    @Override
    public Member getMember() {
      return member;
    }

    @Override
    public Object get(Object source) {
      return ownerType.getPropertyValue(ownerGetter.get(source), index);
    }

    @Override
    public Class<?> getReturnedType() {
      return ownerType.getSubtypes()[index].getReturnedClass();
    }

  }

  private class ComponentSetter implements Setter {

    private final Accessor ownerAccessor;
    private final ComponentType ownerType;
    private final Setter setter;

    private final Member member;

    public ComponentSetter(Accessor ownerAccessor, ComponentType type, String attributeName)
        throws SecurityException, NoSuchMethodException {
      this.ownerType = type;
      this.ownerAccessor = ownerAccessor;
      setter = AccessorFactory.basic(type.getReturnedClass(), attributeName).getSetter();
      member = this.getClass().getDeclaredMethod("set", Object.class, Object.class);
    }

    @Override
    public Member getMember() {
      return member;
    }

    @Override
    public void set(Object source, Object val) {
      Object owner = ownerAccessor.get(source);

      if (owner == null) {
        owner = ownerType.getMappingModelPart()
            .getEmbeddableTypeDescriptor()
            .getRepresentationStrategy()
            .getInstantiator()
            .instantiate(() -> null, null);
        ownerAccessor.set(source, owner);
      }

      setter.set(owner, val);
    }

  }

}
