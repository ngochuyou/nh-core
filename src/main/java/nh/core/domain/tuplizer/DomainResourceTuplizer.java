/**
 *
 */
package nh.core.domain.tuplizer;

import nh.core.domain.DomainResource;

/**
 * @author Ngoc Huy
 *
 */
public interface DomainResourceTuplizer<T extends DomainResource> {

  Class<T> getResourceType();

  T instantiate(Object... args) throws TuplizerException;

  void setProperty(Object source, String propName, Object value) throws TuplizerException;

  Object getProperty(Object source, String propName) throws TuplizerException;

}
