package nh.core.domain.validation;

import java.util.Map;
import nh.core.BundledMessage;

public interface DomainValidation {

  boolean isOk();

  Map<String, BundledMessage> getErrors();

}
