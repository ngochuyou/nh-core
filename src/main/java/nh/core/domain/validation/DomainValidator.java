package nh.core.domain.validation;

import jakarta.persistence.EntityManager;
import java.io.Serializable;
import nh.core.domain.DomainResource;
import nh.core.domain.graph.GraphLogic;

public interface DomainValidator<S extends Serializable, T extends DomainResource>
    extends GraphLogic<T> {

	DomainValidation isSatisfiedBy(EntityManager entityManager, T instance);

	DomainValidation isSatisfiedBy(EntityManager entityManager, S id, T instance);

  <E extends T> DomainValidator<S, E> and(DomainValidator<S, E> next);

  @Override
	default <E extends T> GraphLogic<E> and(GraphLogic<E> next) {
		return and((DomainValidator<S, E>) next);
	}

}
