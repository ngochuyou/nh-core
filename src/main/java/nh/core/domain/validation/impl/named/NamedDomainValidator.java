package nh.core.domain.validation.impl.named;

import jakarta.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import nh.core.domain.NamedResource;
import nh.core.domain.metadata.MessageSourceArgumentProvider;
import nh.core.domain.validation.DomainValidation;
import nh.core.domain.validation.EnumeratedDomainValidation;
import nh.core.domain.validation.impl.AbstractDomainValidator;
import nh.core.domain.validation.impl.DomainValidationImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NamedDomainValidator<S extends Serializable>
    extends AbstractDomainValidator<S, NamedResource> {

  private static final Logger logger = LoggerFactory.getLogger(NamedDomainValidator.class);

  private final String fieldName;
  private final Pattern pattern;
  private final MessageSourceArgumentProvider argumentProvider;

  public NamedDomainValidator(String fieldName, Pattern pattern,
      MessageSourceArgumentProvider argumentProvider) {
    this.fieldName = fieldName;
    this.pattern = pattern;
    this.argumentProvider = argumentProvider;

    if (logger.isDebugEnabled()) {
      logger.debug("Compliant pattern is {} for field [{}]", pattern, fieldName);
    }
  }

  @Override
  public DomainValidation isSatisfiedBy(EntityManager entityManager, NamedResource instance) {
    return isSatisfiedBy(entityManager, null, instance);
  }

  @Override
  public DomainValidation isSatisfiedBy(EntityManager entityManager, S id, NamedResource instance) {
    if (Objects.isNull(instance.getName()) || !doesNameMatch(instance.getName())) {
      return new DomainValidationImpl(false,
          Map.of(fieldName, argumentProvider.getArgs(fieldName, pattern)));
    }

    return EnumeratedDomainValidation.OK;
  }

  private boolean doesNameMatch(String name) {
    return pattern.matcher(name).matches();
  }
}
