package nh.core.domain.validation.impl;

import jakarta.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.graph.GraphLogic;
import nh.core.domain.graph.impl.AbstractDomainGraphLogicContext;
import nh.core.domain.mapping.DomainMapping;
import nh.core.domain.validation.DomainValidation;
import nh.core.domain.validation.DomainValidator;
import nh.core.domain.validation.DomainValidatorFactory;
import nh.core.domain.validation.EnumeratedDomainValidation;
import nh.core.domain.validation.impl.named.NamedDomainValidatorFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

public class DomainValidatorFactoryImpl extends AbstractDomainGraphLogicContext
    implements DomainValidatorFactory {

  public DomainValidatorFactoryImpl(
      AutowireCapableBeanFactory beanFactory,
      DomainResourceContext domainContext,
      String basePackage) {
    super(DomainValidator.class, beanFactory, domainContext, basePackage);
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected GraphLogic getNoOpLogic() {
    return NO_OP_VALIDATOR;
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected boolean isNotNoOp(GraphLogic candidate) {
    return !Optional.ofNullable(candidate)
        .map(NO_OP_VALIDATOR::equals)
        .orElse(false);
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected List<DomainMapping> getFixedLogics(AutowireCapableBeanFactory beanFactory) {
    return lookForNamingRules(beanFactory);
  }

  @SuppressWarnings("rawtypes")
  private static List<DomainMapping> lookForNamingRules(AutowireCapableBeanFactory beanFactory) {
    return beanFactory.getBean(NamedDomainValidatorFactory.class)
        .lookForRules();
  }

  @SuppressWarnings("rawtypes")
  private static final DomainValidator NO_OP_VALIDATOR = new AbstractDomainValidator() {

    @Override
    public DomainValidation isSatisfiedBy(EntityManager entityManager, DomainResource instance) {
      return EnumeratedDomainValidation.OK;
    }

    @Override
    public DomainValidation isSatisfiedBy(EntityManager entityManager, Serializable id,
        DomainResource instance) {
      return EnumeratedDomainValidation.OK;
    }

    @Override
    public String getLoggableName() {
      return "<<NO_OP>>";
    }
  };

  @SuppressWarnings("unchecked")
  @Override
  public <S extends Serializable, T extends DomainResource> DomainValidator<S, T> getValidator(
      Class<T> domainType) {
    return getLogic(domainType)
        .map(DomainValidator.class::cast)
        .orElseThrow(() -> new IllegalStateException(
            String.format("Unable to locate %s for %s", DomainValidator.class.getSimpleName(),
                domainType)));
  }
}
