package nh.core.domain.validation.impl;

import java.util.Map;
import nh.core.BundledMessage;
import nh.core.domain.validation.DomainValidation;

public class DomainValidationImpl implements DomainValidation {

  private final boolean ok;
  private final Map<String, BundledMessage> errors;

  public DomainValidationImpl(boolean ok, Map<String, BundledMessage> errors) {
    this.ok = ok;
    this.errors = errors;
  }

  @Override
  public boolean isOk() {
    return ok;
  }

  @Override
  public Map<String, BundledMessage> getErrors() {
    return errors;
  }
}
