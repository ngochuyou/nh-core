package nh.core.domain.validation.impl.named;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import nh.core.BundledMessage;
import nh.core.constants.message.DomainValidationMessages;
import nh.core.declaration.Declarations;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.NamedResource;
import nh.core.domain.graph.impl.DomainResourceGraphCollectors;
import nh.core.domain.mapping.DomainMapping;
import nh.core.domain.metadata.MessageSourceArgumentProvider;
import nh.core.domain.metadata.NamedResourceMetadata;
import nh.core.domain.metadata.impl.Naming;
import nh.core.util.RegexUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("rawtypes")
public class NamedDomainValidatorFactory {

  public static final MessageSourceArgumentProvider DEFAULT_ARGUMENTS_PROVIDER = new MessageSourceArgumentProvider() {

    private static final BundledMessage DEFAULT_ARGS = new BundledMessage() {
      @Override
      public String getCode() {
        return DomainValidationMessages.DV002;
      }

    };

    @Override
    public BundledMessage getArgs(String fieldName, Pattern pattern) {
      return DEFAULT_ARGS;
    }
  };

  private final DomainResourceContext domainContext;

  public NamedDomainValidatorFactory(DomainResourceContext domainContext) {
    this.domainContext = domainContext;
  }

  public List<DomainMapping> lookForRules() {
    return Declarations.of(collect())
        .map(this::resolveValidators)
        .get();
  }

  private Set<Class> collect() {
    return domainContext.getResourceGraph().locate(NamedResource.class)
        .collectChildren(DomainResourceGraphCollectors.toConcreteTypesSet())
        .stream()
        .map(Class.class::cast)
        .collect(Collectors.toUnmodifiableSet());
  }

  private List<DomainMapping> resolveValidators(Set<Class> domainTypes) {
    return domainTypes.stream()
        .map(this::resolveNaming)
        .map(this::createValidators)
        .toList();
  }

  @SuppressWarnings("unchecked")
  private DomainMapping<?, Naming> resolveNaming(Class domainType) {
    return DomainMapping.of(domainType,
        ((NamedResourceMetadata) domainContext.getMetadata(domainType)
            .require(NamedResourceMetadata.class))
            .getScopedAttributeNames());
  }

  private DomainMapping createValidators(
      DomainMapping<?, Naming> mapping) {
    if (Objects.isNull(mapping.getInstance().getPattern())) {
      return DomainMapping.of(mapping.getDomainType(), createFromDefaultPattern(mapping));
    }

    return DomainMapping.of(mapping.getDomainType(), createFromPattern(mapping));
  }

  private NamedDomainValidator createFromDefaultPattern(DomainMapping<?, Naming> mapping) {
    return new DefaultPatternNamedDomainValidator(mapping.getInstance());
  }

  private NamedDomainValidator createFromPattern(DomainMapping<?, Naming> mapping) {
    final Naming naming = mapping.getInstance();

    return new NamedDomainValidator(naming.getAttributeName(),
        naming.getPattern(),
        Optional.ofNullable(naming.getArgumentProvider()).orElse(DEFAULT_ARGUMENTS_PROVIDER));
  }

  private static class DefaultPatternNamedDomainValidator<S extends Serializable> extends
      NamedDomainValidator<S> {

    private static final Logger logger = LoggerFactory.getLogger(
        DefaultPatternNamedDomainValidator.class);

    private static final Pattern DEFAULT_PATTERN = Pattern.compile(
        "^["
            + RegexUtils.VIETNAMESE_CHARACTERS
            + "\\p{L}\\p{N}"
            + " "
            + "\\.,()\\[\\]_\\-+=/!@#$%\\^&*'\"?:"
            + "]{1,255}$");

    static {
      logger.debug("Default pattern is {}", DEFAULT_PATTERN);
    }

    public DefaultPatternNamedDomainValidator(Naming naming) {
      super(naming.getAttributeName(), DEFAULT_PATTERN, DEFAULT_ARGUMENTS_PROVIDER);
    }

  }

}
