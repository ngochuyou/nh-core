/**
 *
 */
package nh.core.domain.validation.impl;

import jakarta.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import nh.core.domain.DomainResource;
import nh.core.domain.validation.DomainValidation;
import nh.core.domain.validation.DomainValidator;

/**
 * @author Ngoc Huy
 */
public abstract class AbstractDomainValidator<S extends Serializable, T extends DomainResource> implements
    DomainValidator<S, T> {

  @Override
  public <E extends T> DomainValidator<S, E> and(DomainValidator<S, E> next) {
    return new CompositeValidator<>(this, next);
  }

  private class CompositeValidator<E extends T> extends AbstractDomainValidator<S, E> {

    private final DomainValidator<S, T> left;
    private final DomainValidator<S, E> right;

    private CompositeValidator(DomainValidator<S, T> left, DomainValidator<S, E> right) {
      this.left = left;
      this.right = right;
    }

    @Override
    public DomainValidation isSatisfiedBy(EntityManager entityManager, E instance) {
      return combine(left.isSatisfiedBy(entityManager, instance),
					right.isSatisfiedBy(entityManager, instance));
    }

    @Override
    public DomainValidation isSatisfiedBy(EntityManager entityManager, S id, E instance) {
      return combine(left.isSatisfiedBy(entityManager, id, instance),
					right.isSatisfiedBy(entityManager, id, instance));
    }

    private static DomainValidation combine(
        DomainValidation firstValidation,
        DomainValidation secondValidation) {
      return new DomainValidationImpl(
          firstValidation.isOk() && secondValidation.isOk(),
          Stream.concat(firstValidation.getErrors().entrySet().stream(),
                  secondValidation.getErrors().entrySet().stream())
              .collect(Collectors.toMap(
                  Map.Entry::getKey,
                  Map.Entry::getValue)));
    }

    @Override
    public String getLoggableName() {
      return String.format("[%s, %s]", left.getLoggableName(), right.getLoggableName());
    }

  }

}
