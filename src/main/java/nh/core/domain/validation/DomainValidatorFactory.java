package nh.core.domain.validation;

import java.io.Serializable;
import nh.core.domain.DomainResource;

public interface DomainValidatorFactory {

  <S extends Serializable, T extends DomainResource> DomainValidator<S, T> getValidator(
      Class<T> resourceType);

}
