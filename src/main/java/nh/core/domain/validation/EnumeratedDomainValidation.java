package nh.core.domain.validation;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import nh.core.BundledMessage;
import nh.core.constants.message.DomainValidationMessages;

public enum EnumeratedDomainValidation implements DomainValidation {

  OK(true, Collections.emptyMap()),
  FAILED(false, null);

  private final boolean ok;
  private final Map<String, BundledMessage> errors;

  EnumeratedDomainValidation(boolean ok, Map<String, BundledMessage> errors) {
    this.ok = ok;
    this.errors = errors;
  }

  @Override
  public boolean isOk() {
    return ok;
  }

  @Override
  public Map<String, BundledMessage> getErrors() {
    return Optional.ofNullable(errors).orElse(NOT_PROVIDED);
  }

  private static final Map<String, BundledMessage> NOT_PROVIDED = Map.of("*",
      new BundledMessage() {

        @Override
        public String getCode() {
          return DomainValidationMessages.DV001;
        }

      });

}
