package nh.core.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import nh.core.domain.metadata.MessageSourceArgumentProvider;
import org.apache.commons.lang3.StringUtils;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Message {

  String value() default StringUtils.EMPTY;

  String code() default StringUtils.EMPTY;

  String[] args() default {};

  Class<? extends MessageSourceArgumentProvider> argumentProvider()
      default MessageSourceArgumentProvider.class;

}
