package nh.core.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Ngoc Huy
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Encrypted {

  String pattern() default StringUtils.EMPTY;

  public static interface Message {

    public static String getMissingMessage(Class<?> resourceType) {
      return String
          .format(
              "@%s is missing on type %s", Encrypted.class.getSimpleName(), resourceType.getName());
    }

  }

}
