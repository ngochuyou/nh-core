package nh.core.domain.annotation;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import nh.core.domain.NamedResource;

/**
 * Indicates the targeted field fully inherits every logic of the {@link NamedResource}
 *
 * @author Ngoc Huy
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Name {

  String pattern() default EMPTY;

  interface Message {

    static String getMissingMessage(Class<?> resourceType) {
      return String
          .format("@%s is missing on type %s", Name.class.getSimpleName(), resourceType.getName());
    }

  }

}