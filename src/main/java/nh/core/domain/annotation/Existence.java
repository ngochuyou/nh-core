package nh.core.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Ngoc Huy
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Existence {

  public static interface Message {

    public static String getMissingMessage(Class<?> resourceType) {
      return String
          .format(
              "@%s is missing on type %s", Existence.class.getSimpleName(), resourceType.getName());
    }

  }

}
