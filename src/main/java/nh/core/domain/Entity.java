package nh.core.domain;

import java.io.Serializable;

public interface Entity<S extends Serializable> extends IdentifiableResource<S> {

}
