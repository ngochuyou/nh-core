/**
 *
 */
package nh.core.domain;

import nh.core.context.ContextBuilder;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.domain.sf.SessionFactoryManager;
import nh.core.domain.tuplizer.DomainResourceTuplizer;

/**
 * @author Ngoc Huy
 *
 */
public interface DomainResourceContext extends ContextBuilder {

  DomainResourceGraph<DomainResource> getResourceGraph();

  <T extends DomainResource> DomainResourceMetadata<T> getMetadata(Class<T> resourceType);

  <T extends DomainResource> DomainResourceTuplizer<T> getTuplizer(Class<T> resourceType);

  SessionFactoryManager getSessionFactoryManager();

}
