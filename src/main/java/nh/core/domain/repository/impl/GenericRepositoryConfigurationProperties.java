package nh.core.domain.repository.impl;

import jakarta.persistence.LockModeType;
import nh.core.constants.Configurations;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Ngoc Huy
 */
@ConfigurationProperties(Configurations.REPO_CONTEXT_PREFIX)
public class GenericRepositoryConfigurationProperties {

  private final PageableConfigurationProperties pageable;
  private final LockConfigurationProperties lockMode;

  public GenericRepositoryConfigurationProperties(
      PageableConfigurationProperties pageable,
      LockConfigurationProperties lockMode) {
    this.pageable = pageable;
    this.lockMode = lockMode;
  }

  public PageableConfigurationProperties getPageable() {
    return pageable;
  }

  public LockConfigurationProperties getLockMode() {
    return lockMode;
  }

  @ConfigurationProperties(Configurations.REPO_CONTEXT_PAGEABLE_PREFIX)
  public static class PageableConfigurationProperties {

    private final int size;

    public PageableConfigurationProperties(int size) {
      this.size = size;
    }

    public int getSize() {
      return size;
    }

  }

  @ConfigurationProperties(Configurations.REPO_CONTEXT_LOCK_PREFIX)
  public static class LockConfigurationProperties {

    private final LockModeType find;
    private final LockModeType update;

    public LockConfigurationProperties(LockModeType find, LockModeType update) {
      this.find = find;
      this.update = update;
    }

    public LockModeType getFind() {
      return find;
    }

    public LockModeType getUpdate() {
      return update;
    }

  }

}
