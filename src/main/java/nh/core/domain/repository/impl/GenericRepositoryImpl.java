package nh.core.domain.repository.impl;

import jakarta.persistence.LockModeType;
import jakarta.persistence.Tuple;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import nh.core.constants.Pageables;
import nh.core.declaration.Declarations;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.PermanentResource;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.graph.impl.DomainResourceGraphCollectors;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.domain.metadata.PermanentResourceMetadata;
import nh.core.domain.repository.GenericRepository;
import nh.core.domain.repository.Selector;
import nh.core.domain.repository.SetStatementBuilder;
import nh.core.domain.repository.WhereStatementBuilder;
import nh.core.domain.repository.impl.GenericRepositoryConfigurationProperties.LockConfigurationProperties;
import nh.core.domain.sf.SessionFactoryManager;
import nh.core.util.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SharedSessionContract;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ClassUtils;

/**
 * @author Ngoc Huy
 */
public class GenericRepositoryImpl implements GenericRepository {

  private static final Logger logger = LoggerFactory.getLogger(GenericRepositoryImpl.class);
  @SuppressWarnings({
      "rawtypes", "unchecked"
  })
  private static final Map<Class, Function<DomainResourceMetadata<DomainResource>, Specification>> FIXED_SPECIFICATIONS =
      Map.of(
          PermanentResource.class,
          metadata -> (root, query, builder) -> builder
              .equal(
                  root.get(
                      metadata.require(PermanentResourceMetadata.class).getScopedField().getName()),
                  Boolean.FALSE));
  private final Pageable defaultPageable;
  private final LockModeType defaultFindLockMode;
  private final LockModeType defaultUpdateLockMode;
  private final Map<Class<? extends DomainResource>, Specification<? extends DomainResource>> fixedSpecifications;
  private final SessionFactoryManager sessionFactoryManager;

  @SuppressWarnings("unchecked")
  public GenericRepositoryImpl(
      DomainResourceContext resourceContext,
      GenericRepositoryConfigurationProperties configurationProperties) {

    if (logger.isTraceEnabled()) {
      logger.debug("Instantiating {}", GenericRepositoryImpl.class.getName());
    }

    this.defaultPageable = Pageable.ofSize(configurationProperties.getPageable().getSize());

    final LockConfigurationProperties lockConfigurationProperties =
        configurationProperties.getLockMode();

    this.defaultFindLockMode = lockConfigurationProperties.getFind();
    this.defaultUpdateLockMode = lockConfigurationProperties.getUpdate();
    this.sessionFactoryManager = resourceContext.getSessionFactoryManager();

    final Map<Class<? extends DomainResource>, Specification<? extends DomainResource>> fixedSpecificationCandidates =
        new HashMap<>(0);

    for (final DomainResourceGraph<? extends DomainResource> node : resourceContext
        .getResourceGraph()
        .collect(DomainResourceGraphCollectors.toGraphsSet())) {
      final Class<? extends DomainResource> entityType = node.getResourceType();

      if (Modifier.isAbstract(entityType.getModifiers())) {
        continue;
      }

      Declarations.of(getAllInterfaces(entityType))
          .map(this::filterOutNonTargetedTypes)
          .map(ArrayList::new)
          .second((DomainResourceMetadata<DomainResource>) resourceContext.getMetadata(entityType))
          .map(this::chainFixedSpecifications)
          .second(entityType)
          .biInverse()
          .consume(fixedSpecificationCandidates::put);
    }

    this.fixedSpecifications = Collections.unmodifiableMap(fixedSpecificationCandidates);
  }

  private <D extends DomainResource> Set<Class<?>> getAllInterfaces(Class<D> entityType) {

    if (logger.isTraceEnabled()) {
      logger.trace(
          "Finding all interfaces of {} type [{}]", DomainResource.class.getSimpleName(),
          entityType.getName());
    }

    return ClassUtils.getAllInterfacesForClassAsSet(entityType);
  }

  private Set<Class<?>> filterOutNonTargetedTypes(Set<Class<?>> interfaces) {

    if (logger.isTraceEnabled()) {
      logger.trace("Filtering fixed interfaces");
    }

    return interfaces.stream().filter(FIXED_SPECIFICATIONS::containsKey)
        .collect(Collectors.toSet());
  }

  @SuppressWarnings({
      "rawtypes", "unchecked"
  })
  private Specification chainFixedSpecifications(
      List<Class<?>> interfaces,
      DomainResourceMetadata<DomainResource> domainResourceMetadata) {

    if (logger.isTraceEnabled()) {
      logger.trace("Chaining {}(s)", Specification.class.getSimpleName());
    }

    // shouldn't be null here
    if (interfaces.isEmpty()) {
      return HibernateUtils.any();
    }

    return interfaces.stream()
        .map(FIXED_SPECIFICATIONS::get)
        .map(fnc -> fnc.apply(domainResourceMetadata))
        .reduce(Specification::and)
        .orElse(FIXED_SPECIFICATIONS.get(interfaces.get(0)).apply(domainResourceMetadata));
  }

  @Override
  public <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      SharedSessionContract session) {
    return findAll(type, selector, defaultPageable, defaultFindLockMode, session);
  }

  @Override
  public <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      LockModeType lockModeType,
      SharedSessionContract session) {
    return findAll(type, selector, defaultPageable, lockModeType, session);
  }

  @Override
  public <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Pageable pageable,
      SharedSessionContract session) {
    return findAll(type, selector, pageable, defaultFindLockMode, session);
  }

  @Override
  public <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Pageable pageable,
      LockModeType lockMode,
      SharedSessionContract session) {
    final CriteriaBuilder criteriaBuilder = locateCriteriaBuilder(type);

    return Declarations.of(criteriaBuilder)
        .map(CriteriaBuilder::createTupleQuery)
        .second(cq -> cq.from(type))
        .consume((cq, root) -> doSelect(cq, root, selector, criteriaBuilder))
        .map((cq, root) -> doOrder(cq, root, pageable, criteriaBuilder))
        .second(session)
        .map(this::createHQL)
        .second(pageable)
        .map(this::doPaging)
        .second(lockMode)
        .map(this::doLocking)
        .map(Query::list)
        .get();
  }

  private <D extends DomainResource> CriteriaBuilder locateCriteriaBuilder(Class<D> type) {
    return Declarations.of(type)
        .map(sessionFactoryManager::locateSessionFactory)
        .map(SessionFactoryImplementor::getCriteriaBuilder)
        .get();
  }

  private <D extends DomainResource, E> void doSelect(
      CriteriaQuery<E> cq,
      Root<D> root,
      Selector<D, E> selector,
      CriteriaBuilder criteriaBuilder) {
    cq.multiselect(selector.select(root, cq, criteriaBuilder));
  }

  private <D extends DomainResource, E> CriteriaQuery<E> doOrder(
      CriteriaQuery<E> cq,
      Root<D> root,
      Pageable pageable,
      CriteriaBuilder criteriaBuilder) {
    return Declarations.of(pageable.getSort())
        .map(Sort::get)
        .map(stream -> stream.map(order -> toOrder(root, order, criteriaBuilder)))
        .map(Stream::toList)
        .map(cq::orderBy)
        .get();
  }

  private <D extends DomainResource> Order toOrder(
      Root<D> root,
      org.springframework.data.domain.Sort.Order hbmOrder,
      CriteriaBuilder criteriaBuilder) {

    if (hbmOrder.isAscending()) {
      return criteriaBuilder.asc(root.get(hbmOrder.getProperty()));
    }

    return criteriaBuilder.desc(root.get(hbmOrder.getProperty()));
  }

  private <E> Query<E> createHQL(CriteriaQuery<E> cq, SharedSessionContract session) {
    return session.createQuery(cq);
  }

  private <E> Query<E> doPaging(Query<E> hql, Pageable pageable) {
    return hql.setMaxResults(pageable.getPageSize())
        .setFirstResult(pageable.getPageSize() * pageable.getPageNumber());
  }

  private <E> Query<E> doLocking(Query<E> hql, LockModeType lockMode) {
    return hql.setLockMode(lockMode);
  }

  @Override
  public <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> spec,
      SharedSessionContract session) {
    return findAll(type, selector, spec, defaultPageable, defaultFindLockMode, session);
  }

  @Override
  public <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> spec,
      LockModeType lockModeType,
      SharedSessionContract session) {
    return findAll(type, selector, spec, defaultPageable, lockModeType, session);
  }

  @Override
  public <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> spec,
      Pageable pageable,
      SharedSessionContract session) {
    return findAll(type, selector, spec, pageable, defaultFindLockMode, session);
  }

  @Override
  public <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> specification,
      Pageable pageable,
      LockModeType lockMode,
      SharedSessionContract session) {
    final CriteriaBuilder criteriaBuilder = locateCriteriaBuilder(type);

    return Declarations.of(criteriaBuilder)
        .map(CriteriaBuilder::createTupleQuery)
        .second(cq -> cq.from(type))
        .consume((cq, root) -> doSelect(cq, root, selector, criteriaBuilder))
        .consume((cq, root) -> doFilter(cq, root, specification, criteriaBuilder))
        .map((cq, root) -> doOrder(cq, root, pageable, criteriaBuilder))
        .second(session)
        .map(this::createHQL)
        .second(pageable)
        .map(this::doPaging)
        .second(lockMode)
        .map(this::doLocking)
        .map(Query::list)
        .get();
  }

  @SuppressWarnings("unchecked")
  private <D extends DomainResource, E> CriteriaQuery<E> doFilter(
      CriteriaQuery<E> cq,
      Root<D> root,
      Specification<D> requestedSpecication,
      CriteriaBuilder criteriaBuilder) {
    final Specification<D> mandatorySpecification = (Specification<D>) fixedSpecifications
        .get(root.getModel().getBindableJavaType());

    return cq.where(
        mandatorySpecification.and(requestedSpecication).toPredicate(root, cq, criteriaBuilder));
  }

  @Override
  public <D extends DomainResource> List<D> findAll(Class<D> type, SharedSessionContract session) {
    return findAll(type, defaultPageable, defaultFindLockMode, session);
  }

  @Override
  public <D extends DomainResource> List<D> findAll(
      Class<D> type,
      LockModeType lockModeType,
      SharedSessionContract session) {
    return findAll(type, defaultPageable, lockModeType, session);
  }

  @Override
  public <D extends DomainResource> List<D> findAll(
      Class<D> type,
      Pageable pageable,
      SharedSessionContract session) {
    return findAll(type, pageable, defaultFindLockMode, session);
  }

  @Override
  public <D extends DomainResource> List<D> findAll(
      Class<D> type,
      Pageable pageable,
      LockModeType lockMode,
      SharedSessionContract session) {
    final CriteriaBuilder criteriaBuilder = locateCriteriaBuilder(type);

    return Declarations.of(criteriaBuilder.createQuery(type))
        .second(cq -> cq.from(type))
        .map((cq, root) -> doOrder(cq, root, pageable, criteriaBuilder))
        .second(session)
        .map(this::createHQL)
        .second(pageable)
        .map(this::doPaging)
        .second(lockMode)
        .map(this::doLocking)
        .map(Query::list)
        .get();

  }

  @Override
  public <D extends DomainResource> List<D> findAll(
      Class<D> type,
      SharedSessionContract session,
      Specification<D> spec) {
    return findAll(type, defaultPageable, defaultFindLockMode, session, spec);
  }

  @Override
  public <D extends DomainResource> List<D> findAll(
      Class<D> type,
      LockModeType lockModeType,
      SharedSessionContract session,
      Specification<D> spec) {
    return findAll(type, defaultPageable, lockModeType, session, spec);
  }

  @Override
  public <D extends DomainResource> List<D> findAll(
      Class<D> type,
      Pageable pageable,
      SharedSessionContract session,
      Specification<D> spec) {
    return findAll(type, pageable, defaultFindLockMode, session, spec);
  }

  @Override
  public <D extends DomainResource> List<D> findAll(
      Class<D> type,
      Pageable pageable,
      LockModeType lockMode,
      SharedSessionContract session,
      Specification<D> specification) {
    final CriteriaBuilder criteriaBuilder = locateCriteriaBuilder(type);

    return Declarations.of(criteriaBuilder.createQuery(type))
        .second(cq -> cq.from(type))
        .consume((cq, root) -> doFilter(cq, root, specification, criteriaBuilder))
        .map((cq, root) -> doOrder(cq, root, pageable, criteriaBuilder))
        .second(session)
        .map(this::createHQL)
        .second(pageable)
        .map(this::doPaging)
        .second(lockMode)
        .map(this::doLocking)
        .map(Query::list)
        .get();
  }

  @Override
  public <D extends DomainResource> Optional<D> findById(
      Class<D> type,
      Serializable id,
      SharedSessionContract session) {
    return findById(type, id, defaultFindLockMode, session);
  }

  @Override
  public <D extends DomainResource> Optional<D> findById(
      Class<D> type,
      Serializable id,
      LockModeType lockMode,
      SharedSessionContract session) {
    return findOne(type, HibernateUtils.hasId(type, id, session), lockMode, session);
  }

  @Override
  public <D extends DomainResource> Optional<Tuple> findById(
      Class<D> type,
      Serializable id,
      Selector<D, Tuple> selector,
      SharedSessionContract session) {
    return findById(type, id, selector, defaultFindLockMode, session);
  }

  @Override
  public <D extends DomainResource> Optional<Tuple> findById(
      Class<D> type,
      Serializable id,
      Selector<D, Tuple> selector,
      LockModeType lockMode,
      SharedSessionContract session) {
    return findOne(type, selector, HibernateUtils.hasId(type, id, session), lockMode, session);
  }

  @Override
  public <D extends DomainResource> Optional<D> findOne(
      Class<D> type,
      Specification<D> spec,
      SharedSessionContract session) {
    return findOne(type, spec, defaultFindLockMode, session);
  }

  @Override
  public <D extends DomainResource> Optional<D> findOne(
      Class<D> type,
      Specification<D> specification,
      LockModeType lockMode,
      SharedSessionContract session) {
    final CriteriaBuilder criteriaBuilder = locateCriteriaBuilder(type);

    return Declarations.of(criteriaBuilder.createQuery(type))
        .second(cq -> cq.from(type))
        .map((cq, root) -> doFilter(cq, root, specification, criteriaBuilder))
        .second(session)
        .map(this::createHQL)
        .second(Pageables.ONE)
        .map(this::doPaging)
        .second(lockMode)
        .map(this::doLocking)
        .map(Query::getResultStream)
        .map(Stream::findFirst)
        .get();
  }

  @Override
  public <D extends DomainResource> Optional<Tuple> findOne(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> spec,
      SharedSessionContract session) {
    return findOne(type, selector, spec, defaultFindLockMode, session);
  }

  @Override
  public <D extends DomainResource> Optional<Tuple> findOne(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> specification,
      LockModeType lockMode,
      SharedSessionContract session) {
    final CriteriaBuilder criteriaBuilder = locateCriteriaBuilder(type);

    return Declarations.of(type)
        .map(sessionFactoryManager::locateSessionFactory)
        .map(SessionFactoryImplementor::getCriteriaBuilder)
        .map(CriteriaBuilder::createTupleQuery)
        .second(cq -> cq.from(type))
        .consume((cq, root) -> doSelect(cq, root, selector, criteriaBuilder))
        .map((cq, root) -> doFilter(cq, root, specification, criteriaBuilder))
        .second(session)
        .map(this::createHQL)
        .second(Pageables.ONE)
        .map(this::doPaging)
        .second(lockMode)
        .map(this::doLocking)
        .map(Query::getResultStream)
        .map(Stream::findFirst)
        .get();

  }

  @Override
  public <D extends DomainResource> long count(Class<D> type, SharedSessionContract session) {
    return count(type, HibernateUtils.any(), session);
  }

  @Override
  public <D extends DomainResource> long count(
      Class<D> type,
      Specification<D> specification,
      SharedSessionContract session) {
    final CriteriaBuilder criteriaBuilder = locateCriteriaBuilder(type);

    return Declarations.of(criteriaBuilder.createQuery(Long.class))
        .second(cq -> cq.from(type))
        .<Selector<D, Long>>third((root, cq, builder) -> List.of(builder.count(root)))
        .consume((cq, root, selector) -> doSelect(cq, root, selector, criteriaBuilder))
        .map((cq, root) -> doFilter(cq, root, specification, criteriaBuilder))
        .second(session)
        .map(this::createHQL)
        .map(Query::getSingleResult)
        .get();
  }

  @Override
  public <D extends DomainResource> boolean doesExist(
      Class<D> type,
      SharedSessionContract session) {
    return doesExist(type, HibernateUtils.any(), session);
  }

  @Override
  public <D extends DomainResource> boolean doesExist(
      Class<D> type,
      Specification<D> specification,
      SharedSessionContract session) {
    return findOne(
        type,
        (root, cq, builder) -> List
            .of(root.get(HibernateUtils.locateIdPropertyName(type, session))),
        specification, session).isPresent();
  }

  @Override
  public <D extends DomainResource> int update(
      Class<D> type,
      SetStatementBuilder<D> setStatementBuilder,
      WhereStatementBuilder<D> specification,
      SharedSessionContract session) {
    return update(
        type, type, setStatementBuilder, specification, defaultUpdateLockMode, session);
  }

  @Override
  public <D extends DomainResource> int update(
      Class<D> type,
      Serializable id,
      SetStatementBuilder<D> setStatementBuilder,
      WhereStatementBuilder<D> specification,
      LockModeType lockMode,
      SharedSessionContract sessionContract) {

    if (!(sessionContract instanceof Session)) {
      throw new IllegalArgumentException(
          String.format(
              "Unable to perform locking with session type %s",
              sessionContract.getClass().getName()));
    }

    if (lockMode != LockModeType.NONE) {
      findOne(
          type, (root, cq, builder) -> List.of(builder.count(root)),
          HibernateUtils.hasId(type, id, sessionContract), lockMode, sessionContract);
    }

    final CriteriaBuilder criteriaBuilder =
        sessionFactoryManager.locateSessionFactory(type).getCriteriaBuilder();
    final CriteriaUpdate<D> cu = criteriaBuilder.createCriteriaUpdate(type);
    final Root<D> from = cu.from(type);

    setStatementBuilder.build(from, cu, criteriaBuilder)
        .where(specification.build(from, cu, criteriaBuilder));

    return sessionContract.createMutationQuery(cu).executeUpdate();
  }

}
