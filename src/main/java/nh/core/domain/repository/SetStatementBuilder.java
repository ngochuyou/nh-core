package nh.core.domain.repository;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.criteria.Root;

/**
 * @author Ngoc Huy
 */
@FunctionalInterface
public interface SetStatementBuilder<T> {

  @SuppressWarnings("squid:S1452")
  CriteriaUpdate<?> build(Root<T> root, CriteriaUpdate<?> query, CriteriaBuilder builder);

}
