package nh.core.domain.repository;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

/**
 * @author Ngoc Huy
 */
@FunctionalInterface
public interface WhereStatementBuilder<T> {

  Predicate build(Root<T> root, CriteriaUpdate<?> query, CriteriaBuilder builder);

}
