package nh.core.domain.repository;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Selection;
import java.util.List;
import nh.core.domain.DomainResource;

/**
 * @author Ngoc Huy
 */
@FunctionalInterface
public interface Selector<D extends DomainResource, E> {

  @SuppressWarnings("squid:S1452")
  List<Selection<?>> select(
      Root<D> root,
      CriteriaQuery<E> query,
      CriteriaBuilder builder);

}
