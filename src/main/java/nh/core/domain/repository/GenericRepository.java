package nh.core.domain.repository;

import jakarta.persistence.LockModeType;
import jakarta.persistence.Tuple;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import nh.core.context.ContextBuilder;
import nh.core.domain.DomainResource;
import org.hibernate.SharedSessionContract;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author Ngoc Huy
 */
public interface GenericRepository extends ContextBuilder {

  <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      SharedSessionContract session);

  <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      LockModeType lockModeType,
      SharedSessionContract session);

  <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Pageable pageable,
      SharedSessionContract session);

  <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Pageable pageable,
      LockModeType lockMode,
      SharedSessionContract session);

  <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> spec,
      SharedSessionContract session);

  <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> spec,
      LockModeType lockModeType,
      SharedSessionContract session);

  <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> spec,
      Pageable pageable,
      SharedSessionContract session);

  <D extends DomainResource> List<Tuple> findAll(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> spec,
      Pageable pageable,
      LockModeType lockMode,
      SharedSessionContract session);

  /* ==================== */
  <D extends DomainResource> List<D> findAll(Class<D> type, SharedSessionContract session);

  <D extends DomainResource> List<D> findAll(
      Class<D> type,
      LockModeType lockModeType,
      SharedSessionContract session);

  <D extends DomainResource> List<D> findAll(
      Class<D> type,
      Pageable pageable,
      SharedSessionContract session);

  <D extends DomainResource> List<D> findAll(
      Class<D> type,
      Pageable pageable,
      LockModeType lockMode,
      SharedSessionContract session);

  <D extends DomainResource> List<D> findAll(
      Class<D> type,
      SharedSessionContract session,
      Specification<D> spec);

  <D extends DomainResource> List<D> findAll(
      Class<D> type,
      LockModeType lockModeType,
      SharedSessionContract session,
      Specification<D> spec);

  <D extends DomainResource> List<D> findAll(
      Class<D> type,
      Pageable pageable,
      SharedSessionContract session,
      Specification<D> spec);

  <D extends DomainResource> List<D> findAll(
      Class<D> type,
      Pageable pageable,
      LockModeType lockMode,
      SharedSessionContract session,
      Specification<D> spec);

  /* ==================== */
  <D extends DomainResource> Optional<D> findById(
      Class<D> clazz,
      Serializable id,
      SharedSessionContract session);

  <D extends DomainResource> Optional<D> findById(
      Class<D> clazz,
      Serializable id,
      LockModeType lockMode,
      SharedSessionContract session);

  /* ==================== */
  <D extends DomainResource> Optional<Tuple> findById(
      Class<D> clazz,
      Serializable id,
      Selector<D, Tuple> selector,
      SharedSessionContract session);

  <D extends DomainResource> Optional<Tuple> findById(
      Class<D> clazz,
      Serializable id,
      Selector<D, Tuple> selector,
      LockModeType lockMode,
      SharedSessionContract session);

  /* ==================== */
  <D extends DomainResource> Optional<D> findOne(
      Class<D> type,
      Specification<D> spec,
      SharedSessionContract session);

  <D extends DomainResource> Optional<D> findOne(
      Class<D> type,
      Specification<D> spec,
      LockModeType lockMode,
      SharedSessionContract session);

  /* ==================== */
  <D extends DomainResource> Optional<Tuple> findOne(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> spec,
      SharedSessionContract session);

  <D extends DomainResource> Optional<Tuple> findOne(
      Class<D> type,
      Selector<D, Tuple> selector,
      Specification<D> spec,
      LockModeType lockMode,
      SharedSessionContract session);

  /* ==================== */
  <D extends DomainResource> long count(Class<D> type, SharedSessionContract session);

  <D extends DomainResource> long count(
      Class<D> type,
      Specification<D> specification,
      SharedSessionContract session);

  /* ==================== */
  <D extends DomainResource> boolean doesExist(Class<D> type, SharedSessionContract session);

  <D extends DomainResource> boolean doesExist(
      Class<D> type,
      Specification<D> specification,
      SharedSessionContract session);

  /* ==================== */
  <D extends DomainResource> int update(
      Class<D> type,
      SetStatementBuilder<D> setStatementBuilder,
      WhereStatementBuilder<D> specification,
      SharedSessionContract session);

  <D extends DomainResource> int update(
      Class<D> type,
      Serializable id,
      SetStatementBuilder<D> setStatementBuilder,
      WhereStatementBuilder<D> specification,
      LockModeType lockMode,
      SharedSessionContract session);

}
