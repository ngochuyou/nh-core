package nh.core.domain.impl;

import static org.apache.logging.log4j.util.Strings.EMPTY;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import nh.core.constants.Strings;
import nh.core.context.AbstractContextBuilder;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.graph.impl.GraphBuilder;
import nh.core.domain.metadata.DomainResourceMetadata;
import nh.core.domain.metadata.impl.MetadataManager;
import nh.core.domain.sf.SessionFactoryManager;
import nh.core.domain.sf.impl.SessionFactoryManagerImpl;
import nh.core.domain.tuplizer.DomainResourceTuplizer;
import nh.core.domain.tuplizer.impl.TuplizerBuilder;
import org.hibernate.internal.util.collections.CollectionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * @author Ngoc Huy
 */
public class DomainResourceContextImpl extends AbstractContextBuilder
    implements DomainResourceContext {

  private static final Logger logger = LoggerFactory.getLogger(DomainResourceContextImpl.class);

  private final DomainResourceGraph<DomainResource> resourceGraph;

  private final SessionFactoryManager sessionFactoryManager;
  private final Map<Class<? extends DomainResource>, DomainResourceMetadata<? extends DomainResource>> metadatasMap;
  private final Map<Class<? extends DomainResource>, DomainResourceTuplizer<? extends DomainResource>> tuplizersMap;

  public DomainResourceContextImpl(ApplicationContext applicationContext, String contextPackage)
      throws Exception {
    if (logger.isTraceEnabled()) {
      logger.trace("Constructing {}", DomainResourceContextImpl.class.getName());
    }

    resourceGraph = new GraphBuilder().build(contextPackage);
    sessionFactoryManager =
        new SessionFactoryManagerImpl(contextPackage, applicationContext, resourceGraph);
    metadatasMap = new MetadataManager(applicationContext.getAutowireCapableBeanFactory(),
        resourceGraph, sessionFactoryManager)
        .build(this);
    tuplizersMap = new TuplizerBuilder(this, resourceGraph, sessionFactoryManager).build();
  }

  @Override
  public DomainResourceGraph<DomainResource> getResourceGraph() {
    return resourceGraph;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T extends DomainResource> DomainResourceMetadata<T> getMetadata(Class<T> resourceType) {
    return (DomainResourceMetadata<T>) metadatasMap.get(resourceType);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T extends DomainResource> DomainResourceTuplizer<T> getTuplizer(Class<T> resourceType) {
    return (DomainResourceTuplizer<T>) tuplizersMap.get(resourceType);
  }

  @Override
  public SessionFactoryManager getSessionFactoryManager() {
    return sessionFactoryManager;
  }

  @Override
  public void summary() {
    if (logger.isDebugEnabled()) {
      logger.debug("\n{}", visualizeGraph(resourceGraph, 1));
      logger.debug(
          "\n{}",
          metadatasMap.values().stream().map(Object::toString).collect(Collectors.joining("\n")));
    }
  }

  private String visualizeGraph(
      DomainResourceGraph<? extends DomainResource> node,
      int indentation) {
    final StringBuilder builder = new StringBuilder();

    builder.append(
        String.format(
            "%s%s: %s%n",
            indentation != 0
                ? String.format(
                "%s%s",
                IntStream.range(0, indentation).mapToObj(index -> "\s\s\s")
                    .collect(Collectors.joining(EMPTY)),
                "|__")
                : EMPTY,
            node.getResourceType().getSimpleName(),
            node.getParents() != null
                ? node.getParents().stream().map(p -> p.getResourceType().getSimpleName())
                .collect(Collectors.joining(Strings.COMMON_JOINER))
                : "<ROOT>"));

    if (CollectionHelper.isEmpty(node.getChildren())) {
      return builder.toString();
    }

    for (final DomainResourceGraph<?> children : node.getChildren()) {
      builder.append(visualizeGraph(children, indentation + 1));
    }

    return builder.toString();
  }

}
