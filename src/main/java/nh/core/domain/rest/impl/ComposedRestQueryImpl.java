/**
 *
 */
package nh.core.domain.rest.impl;

import java.util.List;
import java.util.Map;
import nh.core.domain.DomainResource;
import nh.core.domain.rest.ComposedNonBatchingRestQuery;
import nh.core.domain.rest.ComposedRestQuery;
import nh.core.domain.rest.RestPageable;
import nh.core.domain.rest.RestQuery;
import nh.core.domain.rest.filter.Filter;

/**
 * @author Ngoc Huy
 */
public class ComposedRestQueryImpl<D extends DomainResource> implements ComposedRestQuery<D> {

  private final RestQuery<D> delegatedQuery;

  private final List<ComposedNonBatchingRestQuery<?>> nonBatchingAssociationQueries;
  private final List<ComposedRestQuery<?>> batchingAssociationQueries;
  private final Map<String, Filter> filters;

  public ComposedRestQueryImpl(
      RestQuery<D> delegatedQuery,
      List<ComposedNonBatchingRestQuery<?>> nonBatchingAssociationQueries,
      List<ComposedRestQuery<?>> batchingAssociationQueries,
      Map<String, Filter> filters) {
    this.delegatedQuery = delegatedQuery;
    this.nonBatchingAssociationQueries = nonBatchingAssociationQueries;
    this.batchingAssociationQueries = batchingAssociationQueries;
    this.filters = filters;
  }

  @Override
  public Class<D> getResourceType() {
    return delegatedQuery.getResourceType();
  }

  @Override
  public List<String> getAttributes() {
    return delegatedQuery.getAttributes();
  }

  @Override
  public void setAttributes(List<String> attributes) {
    throw new UnsupportedOperationException();
  }

  @Override
  public RestPageable getPage() {
    return delegatedQuery.getPage();
  }

  @Override
  public void setPage(RestPageable pageable) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getAssociationName() {
    return delegatedQuery.getAssociationName();
  }

  @Override
  public void setAssociationName(String name) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<ComposedRestQuery<?>> getBatchingAssociationQueries() {
    return batchingAssociationQueries;
  }

  @Override
  public List<ComposedNonBatchingRestQuery<?>> getNonBatchingAssociationQueries() {
    return nonBatchingAssociationQueries;
  }

  @Override
  public Map<String, Filter> getFilters() {
    return filters;
  }

}
