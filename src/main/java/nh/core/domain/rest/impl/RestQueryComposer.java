package nh.core.domain.rest.impl;

import static java.util.Collections.unmodifiableList;
import static nh.core.declaration.Declarations.of;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.annotation.Domain;
import nh.core.domain.graph.impl.DomainResourceGraphCollectors;
import nh.core.domain.metadata.AssociationForm;
import nh.core.domain.metadata.DomainResourceAttributesMetadata;
import nh.core.domain.rest.ComposedNonBatchingRestQuery;
import nh.core.domain.rest.ComposedRestQuery;
import nh.core.domain.rest.RestQuery;
import nh.core.domain.rest.filter.Filter;
import nh.core.domain.security.SecurityEntryManager;
import nh.core.exception.UnauthorizedDomainAccessException;
import nh.core.reflect.Accessor;
import nh.core.reflect.impl.AccessorFactory;
import nh.core.util.ReflectionUtils;
import nh.core.util.SpringUtils;
import nh.core.util.TypeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.security.core.GrantedAuthority;

public class RestQueryComposer {

  private static final Logger logger = LoggerFactory.getLogger(RestQueryComposer.class);

  private final DomainResourceContext domainContext;
  private final SecurityEntryManager securityManager;

  private final Map<Class<? extends DomainResource>, QueryMetadata> metadataMap;
  private final Map<Class<? extends DomainResource>, FilterAccessors> filterAccessorMap;

  public RestQueryComposer(DomainResourceContext domainContext,
      SecurityEntryManager securityManager,
      String contextPackage)
      throws ClassNotFoundException, NoSuchFieldException {
    this.domainContext = domainContext;
    this.securityManager = securityManager;

    final Map<Class<? extends DomainResource>, Class<? extends RestQuery<?>>> queryClasses =
        scanForQueryClassess(contextPackage);

    metadataMap = of(domainContext, queryClasses)
        .tryMap(RestQueryComposer::toMetadata)
        .map(Collections::unmodifiableMap)
        .get();
    filterAccessorMap = of(domainContext, queryClasses)
        .tryMap(RestQueryComposer::toFilters)
        .map(Collections::unmodifiableMap)
        .get();
  }

  public <D extends DomainResource> ComposedRestQuery<D> compose(RestQuery<D> restQuery,
      GrantedAuthority authority,
      boolean isBatching) throws UnauthorizedDomainAccessException, ReflectiveOperationException {
    logger.debug("Composing query");
    return compose(restQuery, authority, null, isBatching);
  }

  private <D extends DomainResource> ComposedRestQuery<D> compose(RestQuery<D> owningQuery,
      GrantedAuthority authority,
      Integer associatedPosition,
      boolean isBatching) throws UnauthorizedDomainAccessException, ReflectiveOperationException {
    final QueryAssociations associationCandidates = QueryAssociations.from(owningQuery,
        metadataMap.get(owningQuery.getResourceType()), isBatching);
    final AttributesProcessor<D> attributesProcessor = new AttributesProcessor<>(owningQuery,
        associationCandidates, authority);

    owningQuery.setAttributes(attributesProcessor.getProcessedBasicAttributes());

    final QueryAssociations associations = attributesProcessor.getProcessedAssociations();
    final AssociationComposingUnit associationComposingUnit = new AssociationComposingUnit(
        owningQuery,
        associations, authority, isBatching);
    final Map<String, Filter> filters = resolveFilters(owningQuery);

    if (isBatching) {
      return new ComposedRestQueryImpl<>(
          owningQuery,
          associationComposingUnit.getComposedNonBatchingQueries(),
          associationComposingUnit.getComposedBatchingQueries(),
          filters);
    }

    return new ComposedNonBatchingRestQueryImpl<>(
        owningQuery,
        associationComposingUnit.getComposedNonBatchingQueries(),
        associationComposingUnit.getComposedBatchingQueries(),
        filters,
        associatedPosition);
  }

  private <D extends DomainResource> Map<String, Filter> resolveFilters(RestQuery<D> restQuery) {
    final Map<String, Filter> filters = new HashMap<>();

    for (final Entry<String, Accessor> accessor : filterAccessorMap
        .get(restQuery.getResourceType())
        .entrySet()) {
      final Filter filter = Optional.ofNullable(accessor.getValue().get(restQuery))
          .map(Filter.class::cast).orElse(null);

      if (filter == null || filter.getExpressionProducers().isEmpty()) {
        continue;
      }

      filters.put(accessor.getKey(), filter);
    }

    return Collections.unmodifiableMap(filters);
  }

  private static Map<Class<? extends DomainResource>, FilterAccessors> toFilters(
      DomainResourceContext domainContext,
      Map<Class<? extends DomainResource>, Class<? extends RestQuery<?>>> queryClasses) {
    final Map<Class<? extends DomainResource>, FilterAccessors> filterCandidates = new HashMap<>();

    for (final Class<? extends DomainResource> domainType : domainContext.getResourceGraph()
        .collect(DomainResourceGraphCollectors.toTypesSet())) {
      if (!queryClasses.containsKey(domainType)) {
        continue;
      }

      final FilterAccessors filterAccessors = new FilterAccessors(domainType, filterCandidates);
      final Class<? extends RestQuery<?>> queryClass = queryClasses.get(domainType);

      for (final Field field : queryClass.getDeclaredFields()) {
        if (!Filter.class.isAssignableFrom(field.getType())) {
          continue;
        }

        final String filterAttributeName = field.getName();

        filterAccessors.put(filterAttributeName,
            AccessorFactory.basic(queryClass, filterAttributeName));
      }

      filterCandidates.put(domainType, filterAccessors);
    }

    return filterCandidates;
  }

  @SuppressWarnings({"unchecked", "squid:S135"})
  private static Map<Class<? extends DomainResource>, QueryMetadata> toMetadata(
      DomainResourceContext domainContext,
      Map<Class<? extends DomainResource>, Class<? extends RestQuery<?>>> queryClasses)
      throws NoSuchFieldException {
    final Map<Class<? extends DomainResource>, QueryMetadata> metadataCandidates = new HashMap<>();

    for (final Class<? extends DomainResource> domainType : domainContext.getResourceGraph()
        .collect(DomainResourceGraphCollectors.toTypesSet())) {
      if (!queryClasses.containsKey(domainType)) {
        continue;
      }

      final NonBatchingQueryAccessors nonBatchingQueryAccessors = new NonBatchingQueryAccessors(
          domainType, metadataCandidates);
      final BatchingQueryAccessors batchingQueryAccessors = new BatchingQueryAccessors(domainType,
          metadataCandidates);
      final DomainResourceAttributesMetadata<? extends DomainResource> domainMetadata = domainContext
          .getMetadata(domainType).require(DomainResourceAttributesMetadata.class);
      final Class<? extends RestQuery<?>> queryClass = queryClasses.get(domainType);

      for (final Field field : queryClass.getDeclaredFields()) {
        if (!RestQuery.class.isAssignableFrom(field.getType())) {
          continue;
        }

        final Class<? extends RestQuery<?>> associationQueryClass = (Class<? extends RestQuery<?>>) field
            .getType();
        final String associationName = resolveAssociationName(domainMetadata, field);

        assertAssociationType(domainMetadata, queryClass, associationQueryClass, associationName);

        final QueryAccessor associationAccessor = new QueryAccessor(queryClass, associationName);

        if (domainMetadata.getAssociationForm(associationName) == AssociationForm.SINGULAR) {
          nonBatchingQueryAccessors.add(associationAccessor);
          continue;
        }

        batchingQueryAccessors.add(associationAccessor);
      }

      metadataCandidates.put(
          domainType,
          new QueryMetadata(ReflectionUtils.locateNoArgsConstructor(queryClass),
              nonBatchingQueryAccessors, batchingQueryAccessors));
    }

    return metadataCandidates;
  }

  @SuppressWarnings("unchecked")
  private static void assertAssociationType(
      DomainResourceAttributesMetadata<? extends DomainResource> domainMetadata,
      Class<? extends RestQuery<?>> queryClass,
      Class<? extends RestQuery<?>> associationQueryClass,
      String associationName) throws NoSuchFieldException {
    final Class<? extends DomainResource> associationType = ReflectionUtils.requireAnnotation(
        associationQueryClass, Domain.class, Domain::value);

    if (domainMetadata.getAssociationForm(associationName) == AssociationForm.SINGULAR) {
      if (associationType.equals(domainMetadata.getAttributeType(associationName))) {
        return;
      }

      throw new IllegalArgumentException(String.format(
          "[%s.%s] type mismatch between rest query generic type and registered resource type: [%s><%s]",
          queryClass.getSimpleName(), associationName, associationType,
          domainMetadata.getAttributeType(associationName)));
    }

    final Class<? extends DomainResource> genericAssociationType = (Class<? extends DomainResource>) TypeUtils
        .getGenericType(domainMetadata.getResourceType().getDeclaredField(associationName));

    if (associationType.equals(genericAssociationType)) {
      return;
    }

    throw new IllegalArgumentException(String.format(
        "[%s.%s] type mismatch between rest query generic type and registered resource type: [%s><%s]",
        queryClass, associationName, associationType, genericAssociationType));
  }

  private static String resolveAssociationName(
      DomainResourceAttributesMetadata<? extends DomainResource> metadata, Field field) {
    final String fieldName = field.getName();

    if (metadata.isAssociation(fieldName)) {
      return fieldName;
    }

    throw new IllegalArgumentException(
        String.format("[%s.%s] is not an association", field.getDeclaringClass(), fieldName));
  }

  @SuppressWarnings("unchecked")
  private static Map<Class<? extends DomainResource>, Class<? extends RestQuery<?>>> scanForQueryClassess(
      String contextPackage) throws ClassNotFoundException {
    final Map<Class<? extends DomainResource>, Class<? extends RestQuery<?>>> queryClasses = new HashMap<>();

    for (final BeanDefinition beanDefinition : scan(contextPackage)) {
      final Class<? extends RestQuery<?>> queryType = (Class<? extends RestQuery<?>>) Class
          .forName(beanDefinition.getBeanClassName());

      queryClasses.put(ReflectionUtils.requireAnnotation(queryType, Domain.class, Domain::value),
          queryType);
    }

    return queryClasses;
  }

  private static Set<BeanDefinition> scan(String contextPackage) {
    return SpringUtils.scan(RestQuery.class, contextPackage,
        Function.identity(),
        scanner -> scanner.addExcludeFilter(new AssignableTypeFilter(
            ComposedRestQuery.class)));
  }


  private record QueryMetadata(Constructor<?> constructor,
                               NonBatchingQueryAccessors nonBatchingQueryAccessors,
                               BatchingQueryAccessors batchingQueryAccessors) {

    @SuppressWarnings("rawtypes")
    public Constructor getConstructor() {
      return constructor;
    }

  }

  private static class NonBatchingQueryAccessors extends QueryAccessors {

    protected <D extends DomainResource> NonBatchingQueryAccessors(Class<D> domainType,
        Map<Class<? extends DomainResource>, QueryMetadata> onGoingMetadata) {
      super(domainType, onGoingMetadata, QueryMetadata::nonBatchingQueryAccessors);
    }
  }

  private static class BatchingQueryAccessors extends QueryAccessors {

    protected <D extends DomainResource> BatchingQueryAccessors(Class<D> domainType,
        Map<Class<? extends DomainResource>, QueryMetadata> onGoingMetadata) {
      super(domainType, onGoingMetadata, QueryMetadata::batchingQueryAccessors);
    }
  }

  private static class QueryAccessors extends ArrayList<QueryAccessor> {

    protected <D extends DomainResource> QueryAccessors(Class<D> domainType,
        Map<Class<? extends DomainResource>, QueryMetadata> onGoingMetadata,
        Function<QueryMetadata, List<QueryAccessor>> qualifier) {
      final Class<? super D> parentType = domainType.getSuperclass();
      final Deque<Class<? super D>> classQueue = TypeUtils.getClassQueue(domainType);

      while (!classQueue.isEmpty()) {
        if (onGoingMetadata.containsKey(parentType)) {
          addAll(qualifier.apply(onGoingMetadata.get(parentType)));
          return;
        }
      }
    }

  }

  private static class QueryAccessor implements Entry<String, Accessor> {

    private final String associationName;
    private final Accessor accessor;

    private QueryAccessor(Class<? extends RestQuery<?>> owningQueryClass, String associationName) {
      this.associationName = associationName;
      this.accessor = AccessorFactory.basic(owningQueryClass, associationName);
    }

    @Override
    public String getKey() {
      return associationName;
    }

    @Override
    public Accessor getValue() {
      return accessor;
    }

    @Override
    public Accessor setValue(Accessor value) {
      throw new UnsupportedOperationException();
    }
  }

  private static class FilterAccessors extends HashMap<String, Accessor> {

    public <D extends DomainResource> FilterAccessors(Class<D> domainType,
        Map<Class<? extends DomainResource>, FilterAccessors> onGoingAccessors) {
      final Class<? super D> parentType = domainType.getSuperclass();
      final Deque<Class<? super D>> classQueue = TypeUtils.getClassQueue(domainType);

      while (!classQueue.isEmpty()) {
        if (onGoingAccessors.containsKey(parentType)) {
          putAll(onGoingAccessors.get(parentType));
          return;
        }
      }
    }

  }

  private record QueryAssociations(
      List<RestQuery<? extends DomainResource>> nonBatchingQueries,
      List<RestQuery<? extends DomainResource>> batchingQueries) {

    public static <D extends DomainResource> QueryAssociations from(RestQuery<D> query,
        QueryMetadata metadata, boolean isBatching) {
      return new QueryAssociations(
          locateQueries(query, metadata.nonBatchingQueryAccessors()),
          isBatching ? Collections.emptyList()
              : locateQueries(query, metadata.batchingQueryAccessors()));
    }

    private static <D extends DomainResource> List<RestQuery<? extends DomainResource>> locateQueries(
        RestQuery<D> restQuery,
        List<QueryAccessor> accessors) {
      final List<RestQuery<? extends DomainResource>> associationQueries = new ArrayList<>();

      for (final Entry<String, Accessor> accessorEntry : accessors) {
        final RestQuery<? extends DomainResource> associationQuery =
            (RestQuery<? extends DomainResource>) accessorEntry.getValue().get(restQuery);

        if (associationQuery == null) {
          continue;
        }

        associationQuery.setAssociationName(accessorEntry.getKey());
        associationQueries.add(associationQuery);
      }

      return associationQueries;
    }

  }

  private class AttributesProcessor<D extends DomainResource> {

    private final List<String> processedBasicAttributes;
    private final QueryAssociations processedAssociations;

    @SuppressWarnings({"unchecked", "rawtypes"})
    private AttributesProcessor(RestQuery<D> query,
        QueryAssociations associations,
        GrantedAuthority authority) throws ReflectiveOperationException,
        IllegalArgumentException, UnauthorizedDomainAccessException {
      final Class<D> domainType = query.getResourceType();

      final DomainResourceAttributesMetadata attributesMetadata = domainContext.getMetadata(
              domainType)
          .require(DomainResourceAttributesMetadata.class);
      final List<String> processedBasicAttributeCandidates = processBasicAttributes(
          query.getAttributes(), associations);

      processedAssociations = of(processedBasicAttributeCandidates,
          associations, attributesMetadata)
          .tryMap(this::processAssociations)
          .get();
      processedBasicAttributes = of(processedBasicAttributeCandidates, attributesMetadata)
          .map(this::filterAssociationsFromBasicAttributes)
          .map(Collections::unmodifiableList)
          .get();

      commence(associations, authority, domainType);
    }

    private void commence(QueryAssociations associations, GrantedAuthority authority,
        Class<D> domainType) throws UnauthorizedDomainAccessException {
      final List<String> allAttributes =
          Stream.concat(processedBasicAttributes.stream(),
                  Stream.concat(
                      associations.nonBatchingQueries()
                          .stream().map(RestQuery::getAssociationName),
                      associations.batchingQueries()
                          .stream().map(RestQuery::getAssociationName)))
              .toList();

      securityManager.assertAuthority(domainType, allAttributes, authority);
    }

    private List<String> filterAssociationsFromBasicAttributes(
        List<String> basicAttributes,
        DomainResourceAttributesMetadata<D> attributesMetadata) {
      return basicAttributes.stream()
          .filter(basicAttribute -> !attributesMetadata.isAssociation(basicAttribute))
          .toList();
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    private QueryAssociations processAssociations(
        List<String> basicAttributes,
        QueryAssociations associations,
        DomainResourceAttributesMetadata<D> attributesMetadata)
        throws ReflectiveOperationException {
      for (final String basicAttribute : basicAttributes) {
        if (!attributesMetadata.isAssociation(basicAttribute)) {
          continue;
        }

        final RestQuery<?> emptyRestQuery = of(attributesMetadata.getAttributeType(basicAttribute))
            .map(metadataMap::get)
            .map(QueryMetadata::getConstructor)
            .tryMap(Constructor::newInstance)
            .map(RestQuery.class::cast)
            .consume(emptyQuery -> emptyQuery.setAssociationName(basicAttribute))
            .get();

        addToAssociations(associations, emptyRestQuery,
            attributesMetadata.getAssociationForm(basicAttribute));
      }

      return new QueryAssociations(unmodifiableList(associations.nonBatchingQueries()),
          unmodifiableList(associations.batchingQueries()));
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void addToAssociations(
        QueryAssociations associations,
        RestQuery query,
        AssociationForm associationForm) {
      if (associationForm == AssociationForm.SINGULAR) {
        associations.nonBatchingQueries().add(query);
        return;
      }

      associations.batchingQueries().add(query);
    }

    private List<String> processBasicAttributes(
        List<String> basicAttributes,
        QueryAssociations associations) {
      final Set<String> associationNames = Stream.of(associations.nonBatchingQueries(),
              associations.batchingQueries())
          .flatMap(Collection::stream)
          .map(RestQuery::getAssociationName)
          .collect(Collectors.toSet());

      return basicAttributes.stream()
          .filter(basicAttribute -> !associationNames.contains(basicAttribute))
          .toList();
    }

    public List<String> getProcessedBasicAttributes() {
      return processedBasicAttributes;
    }

    public QueryAssociations getProcessedAssociations() {
      return processedAssociations;
    }
  }

  private class AssociationComposingUnit {

    private final GrantedAuthority authority;
    private final boolean isBatching;

    private final DomainResourceAttributesMetadata<?> attributesMetadata;

    private int associationIndex = 0;
    private final List<ComposedNonBatchingRestQuery<?>> composedNonBatchingQueries;
    private final List<ComposedRestQuery<?>> composedBatchingQueries;

    @SuppressWarnings("unchecked")
    private AssociationComposingUnit(RestQuery<?> query, QueryAssociations associations,
        GrantedAuthority authority, boolean isBatching)
        throws UnauthorizedDomainAccessException, ReflectiveOperationException {
      this.authority = authority;
      this.isBatching = isBatching;

      attributesMetadata = domainContext.getMetadata(query.getResourceType())
          .require(DomainResourceAttributesMetadata.class);

      composedNonBatchingQueries = composeAssociation(
          associations.nonBatchingQueries(),
          composedQuery -> associationIndex += ((ComposedNonBatchingRestQuery<?>) composedQuery).getPropertySpan());
      composedBatchingQueries = isBatching
          ? Collections.emptyList()
          : composeAssociation(associations.batchingQueries(),
              this::increaseAssociationIndexByOne);
    }

    private void increaseAssociationIndexByOne(RestQuery<?> any) {
      associationIndex++;
    }

    @SuppressWarnings("unchecked")
    public <Q extends ComposedRestQuery<?>> List<Q> composeAssociation(
        List<RestQuery<? extends DomainResource>> rawAssociationQueries,
        Consumer<ComposedRestQuery<?>> composedQueryConsumer)
        throws UnauthorizedDomainAccessException, ReflectiveOperationException {
      final List<ComposedRestQuery<?>> composedAssociationQueries = new ArrayList<>();

      for (final RestQuery<? extends DomainResource> rawAssociationQuery : rawAssociationQueries) {
        final ComposedRestQuery<?> composedNonBatchingQuery = compose(
            rawAssociationQuery,
            authority,
            associationIndex,
            determineBatching(attributesMetadata, rawAssociationQuery.getAssociationName(),
                isBatching));

        composedQueryConsumer.accept(composedNonBatchingQuery);
        composedAssociationQueries.add(composedNonBatchingQuery);
      }

      return (List<Q>) composedAssociationQueries;
    }

    private static boolean determineBatching(
        DomainResourceAttributesMetadata<?> attributesMetadata, String associationName,
        boolean isQueryBatched) {
      return isQueryBatched
          && attributesMetadata.getAssociationForm(associationName) == AssociationForm.PLURAL;
    }

    @SuppressWarnings("squid:S1452")
    public List<ComposedNonBatchingRestQuery<?>> getComposedNonBatchingQueries() {
      return composedNonBatchingQueries;
    }

    @SuppressWarnings("squid:S1452")
    public List<ComposedRestQuery<?>> getComposedBatchingQueries() {
      return composedBatchingQueries;
    }
  }

}
