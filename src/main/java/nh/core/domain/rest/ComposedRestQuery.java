/**
 *
 */
package nh.core.domain.rest;

import java.util.List;
import java.util.Map;
import nh.core.domain.DomainResource;
import nh.core.domain.rest.filter.Filter;

/**
 * @author Ngoc Huy
 *
 */
public interface ComposedRestQuery<D extends DomainResource> extends RestQuery<D> {

	List<ComposedRestQuery<?>> getBatchingAssociationQueries();

	List<ComposedNonBatchingRestQuery<?>> getNonBatchingAssociationQueries();

	Map<String, Filter> getFilters();

}
