/**
 *
 */
package nh.core.domain.rest;

import java.util.List;
import nh.core.domain.DomainResource;

/**
 * @author Ngoc Huy
 *
 */
public interface RestQuery<D extends DomainResource> {

	Class<D> getResourceType();

	List<String> getAttributes();

	void setAttributes(List<String> attributes);

	RestPageable getPage();

	void setPage(RestPageable pageable);

	String getAssociationName();

	void setAssociationName(String name);

}
