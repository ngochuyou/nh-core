/**
 *
 */
package nh.core.domain.rest;

import nh.core.domain.DomainResource;

/**
 * @author Ngoc Huy
 */
public interface ComposedNonBatchingRestQuery<D extends DomainResource> extends
    ComposedRestQuery<D> {

  Integer getAssociatedPosition();

  int getPropertySpan();

}
