package nh.core.domain.rest.filter;

public interface Plural<T> extends Filter {

  T[] getIn();

  T[] getNi();

}
