package nh.core.domain.rest.filter;

public interface Matchable extends Filter {

  String getLike();

}
