package nh.core.domain.rest.filter;

public interface Singular<T> extends Filter {

  T getEqual();

  T getNot();

}
