package nh.core.domain.rest.filter;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

public abstract class AbstractFilter implements Filter {

  protected final List<BiFunction<Path<?>, CriteriaBuilder, Predicate>> expressionProducers = new ArrayList<>();

  @Override
  @SuppressWarnings("squid:S1452")
  public List<BiFunction<Path<?>, CriteriaBuilder, Predicate>> getExpressionProducers() {
    return expressionProducers;
  }

}
