package nh.core.domain.rest.filter;

public interface Ranged<T> extends Filter {

  T getFrom();

  T getTo();

}
