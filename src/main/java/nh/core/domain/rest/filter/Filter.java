/**
 *
 */
package nh.core.domain.rest.filter;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import java.util.List;
import java.util.function.BiFunction;

/**
 * @author Ngoc Huy
 *
 */
public interface Filter {

	@SuppressWarnings("squid:S1452")
	List<BiFunction<Path<?>, CriteriaBuilder, Predicate>> getExpressionProducers();

}
