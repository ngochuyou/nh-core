package nh.core.domain;

/**
 * @author Ngoc Huy
 */
public interface PermanentResource extends DomainResource {

  boolean isDeleted();

  void setDeleted(boolean isDeleted);

}
