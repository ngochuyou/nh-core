package nh.core.domain;

/**
 * @author Ngoc Huy
 */
public interface NamedResource extends DomainResource {

  String getName();

  void setName(String name);

}
