package nh.core.domain.sf;

import nh.core.domain.DomainResource;
import nh.core.domain.graph.DomainResourceGraph;
import org.hibernate.engine.spi.SessionFactoryImplementor;

/**
 * @author Ngoc Huy
 */
public interface SessionFactoryEntry<D extends DomainResource> {

  SessionFactoryImplementor getSessionFactory();

  DomainResourceGraph<D> getGraph();

}
