package nh.core.domain.sf;

import nh.core.domain.DomainResource;
import nh.core.domain.graph.DomainResourceGraph;
import org.springframework.context.ApplicationContext;

/**
 * @author Ngoc Huy
 */
public interface SessionFactoryEntryRegistrar<D extends DomainResource> {

  SessionFactoryEntry<D> register(
      ApplicationContext applicationContext,
      DomainResourceGraph<DomainResource> resourceGraph);

}
