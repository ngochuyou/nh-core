package nh.core.domain.sf.impl;

import static nh.core.declaration.Declarations.of;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import nh.core.constants.Strings;
import nh.core.declaration.Declarations;
import nh.core.domain.DomainResource;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.graph.impl.DomainResourceGraphCollectors;
import nh.core.domain.sf.SessionFactoryEntry;
import nh.core.domain.sf.SessionFactoryEntryRegistrar;
import nh.core.domain.sf.SessionFactoryManager;
import nh.core.util.SpringUtils;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.util.Assert;

/**
 * @author Ngoc Huy
 */
public class SessionFactoryManagerImpl implements SessionFactoryManager {

  private static final Logger logger = LoggerFactory.getLogger(SessionFactoryManagerImpl.class);

  private final Map<Class<? extends DomainResource>, SessionFactoryImplementor> sfiMap;

  public SessionFactoryManagerImpl(
      String basePackage,
      ApplicationContext applicationContext,
      DomainResourceGraph<DomainResource> resourceGraph) throws ClassNotFoundException {
    sfiMap = of(scan(basePackage), applicationContext, resourceGraph)
        .tryMap(this::instantiate)
        .map(this::transform)
        .consume(this::validate)
        .map(this::flatten)
        .consume(this::log)
        .get();
  }

  private void log(
      Map<Class<? extends DomainResource>, SessionFactoryImplementor> sfiMap) {
    Declarations.of(
            sfiMap.entrySet().stream()
                .reduce(
                    new HashMap<Class<? extends SessionFactoryImplementor>, Set<Class<? extends DomainResource>>>(),
                    (map, entry) -> {
                      final Class<? extends SessionFactoryImplementor> sfiClass =
                          entry.getValue().getClass();

                      map.computeIfAbsent(sfiClass, key -> new HashSet<>());
                      map.get(sfiClass).add(entry.getKey());

                      return map;
                    },
                    (a, b) -> a)
                .entrySet()
                .stream()
                .map(
                    entry -> String.format(
                        "%s: %s", entry.getKey(),
                        entry.getValue().stream().map(Class::getSimpleName)
                            .collect(Collectors.joining(Strings.COMMON_JOINER)))))
        .map(stream -> stream.collect(Collectors.joining("\n\t")))
        .consume(info -> logger.debug("\n\t{}", info));
  }

  private List<Map.Entry<LinkedHashSet<Class<? extends DomainResource>>, SessionFactoryImplementor>> transform(
      List<SessionFactoryEntry<? extends DomainResource>> entries) {
    return entries.stream()
        .map(
            entry -> Map.entry(
                new LinkedHashSet<>(
                    entry.getGraph().collect(DomainResourceGraphCollectors.toTypesSet())
                        .stream()
                        .filter(
                            resourceType -> entry.getGraph().getResourceType()
                                .isAssignableFrom(resourceType))
                        .collect(Collectors.toSet())),
                entry.getSessionFactory()))
        .toList();
  }

  private void validate(
      List<Entry<LinkedHashSet<Class<? extends DomainResource>>, SessionFactoryImplementor>> entries) {
    logger.trace("Validating entries");

    final int summedSize = entries.stream().map(Entry::getKey)
        .map(LinkedHashSet::size)
        .reduce(Integer::sum)
        .orElse(0);
    final int joinedSize = entries.stream().map(Map.Entry::getKey)
        .flatMap(Collection::stream)
        .collect(HashSet::new, Set::add, Set::addAll)
        .size();

    Assert.isTrue(summedSize == joinedSize, "Entity manager scope collided");
  }

  private Map<Class<? extends DomainResource>, SessionFactoryImplementor> flatten(
      List<Map.Entry<LinkedHashSet<Class<? extends DomainResource>>, SessionFactoryImplementor>> entries) {
    return entries.stream()
        .flatMap(
            sfiEntry -> sfiEntry.getKey()
                .stream()
                .map(resourceType -> Map.entry(resourceType, sfiEntry.getValue())))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  private List<SessionFactoryEntry<? extends DomainResource>> instantiate(
      Set<BeanDefinition> beanDefinitions,
      ApplicationContext context,
      DomainResourceGraph<DomainResource> resourceGraph) throws ClassNotFoundException {
    final List<SessionFactoryEntry<? extends DomainResource>> entries = new ArrayList<>();

    for (final BeanDefinition beanDefinition : beanDefinitions) {
      logger.trace(
          "Instantiating an {} instance of type {}",
          SessionFactoryEntryRegistrar.class.getSimpleName(),
          beanDefinition.getBeanClassName());

      of(SpringUtils.<SessionFactoryEntryRegistrar<?>>instantiate(beanDefinition,
          context.getAutowireCapableBeanFactory()))
          .second(context)
          .third(resourceGraph)
          .tryMap(SessionFactoryEntryRegistrar::register)
          .consume(entries::add);
    }

    return Collections.unmodifiableList(entries);
  }

  private Set<BeanDefinition> scan(String basePackage) {
    logger.trace("Scanning for {} definitions", SessionFactoryEntryRegistrar.class.getSimpleName());

    return of(createScanner())
        .consume(this::setTargets)
        .second(basePackage)
        .map(this::startScanning)
        .get();
  }

  private ClassPathScanningCandidateComponentProvider createScanner() {
    return new ClassPathScanningCandidateComponentProvider(false);
  }

  private void setTargets(ClassPathScanningCandidateComponentProvider scanner) {
    scanner.addIncludeFilter(new AssignableTypeFilter(SessionFactoryEntryRegistrar.class));
  }

  private Set<BeanDefinition> startScanning(
      ClassPathScanningCandidateComponentProvider scanner,
      String basePackage) {
    return scanner.findCandidateComponents(basePackage);
  }

  @Override
  public <D extends DomainResource> SessionFactoryImplementor locateSessionFactory(
      Class<D> resourceType) {
    return sfiMap.get(resourceType);
  }

}
