package nh.core.domain.sf;

import nh.core.domain.DomainResource;
import org.hibernate.engine.spi.SessionFactoryImplementor;

/**
 * @author Ngoc Huy
 */
public interface SessionFactoryManager {

  /**
   * Locate the {@link SessionFactoryImplementor} which manages an persistent entity of type
   * {@link D}
   *
   * @param <D>          the persistent entity generic type
   * @param resourceType the persistent entity type
   * @return the {@link SessionFactoryImplementor} accordingly
   */
  <D extends DomainResource> SessionFactoryImplementor locateSessionFactory(
      Class<D> resourceType);

}
