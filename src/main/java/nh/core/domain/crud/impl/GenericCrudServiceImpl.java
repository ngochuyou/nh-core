package nh.core.domain.crud.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Tuple;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import nh.core.constants.Pageables;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.IdentifiableResource;
import nh.core.domain.builder.DomainBuilder;
import nh.core.domain.builder.DomainBuilderFactory;
import nh.core.domain.crud.GenericCrudServiceImplementor;
import nh.core.domain.crud.event.impl.EventListenerGroups;
import nh.core.domain.metadata.IdentifiableResourceMetadata;
import nh.core.domain.repository.GenericRepository;
import nh.core.domain.repository.Selector;
import nh.core.domain.rest.RestQuery;
import nh.core.domain.rest.impl.RestQueryComposer;
import nh.core.domain.security.SecurityEntryManager;
import nh.core.domain.validation.DomainValidation;
import nh.core.domain.validation.DomainValidator;
import nh.core.domain.validation.DomainValidatorFactory;
import nh.core.exception.DomainExceptionTranslator;
import nh.core.exception.DomainValidationException;
import nh.core.exception.UnauthorizedDomainAccessException;
import nh.core.util.HibernateUtils;
import org.hibernate.SharedSessionContract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.GrantedAuthority;

public class GenericCrudServiceImpl implements GenericCrudServiceImplementor<Map<String, Object>> {

  private static final Logger logger = LoggerFactory.getLogger(GenericCrudServiceImpl.class);

  private final DomainResourceContext resourceContext;

  private final GenericRepository genericRepository;
  private final DomainBuilderFactory builderFactory;
  private final DomainValidatorFactory validatorFactory;

  private final EventListenerGroups eventListenerGroups;

  private final SecurityEntryManager securityManager;
  private final RestQueryComposer restQueryComposer;
  private final SelectionProvidersFactory selectionProvidersFactory;

  private final DomainExceptionTranslator domainExceptionTranslator;

  @SuppressWarnings("squid:S107")
  public GenericCrudServiceImpl(GenericRepository genericRepository,
      DomainBuilderFactory builderFactory,
      DomainValidatorFactory validatorFactory,
      DomainResourceContext resourceContext,
      EventListenerGroups eventListenerGroups,
      SecurityEntryManager securityManager,
      RestQueryComposer restQueryComposer,
      SelectionProvidersFactory selectionProvidersFactory,
      DomainExceptionTranslator domainExceptionTranslator) {
    this.genericRepository = genericRepository;
    this.builderFactory = builderFactory;
    this.validatorFactory = validatorFactory;
    this.resourceContext = resourceContext;
    this.eventListenerGroups = eventListenerGroups;
    this.securityManager = securityManager;
    this.restQueryComposer = restQueryComposer;
    this.selectionProvidersFactory = selectionProvidersFactory;
    this.domainExceptionTranslator = domainExceptionTranslator;
  }

  @Override
  public <S extends Serializable, E extends IdentifiableResource<S>> E create(Class<E> domainType,
      S id, E model, EntityManager entityManager) throws DomainValidationException {
    if (logger.isDebugEnabled()) {
      logger.debug("Creating {}#{}", domainType.getName(), id);
    }

    checkIdentity(model.getId(), model, entityManager);

    final DomainBuilder<E> resourceBuilder = builderFactory.getBuilder(domainType);

    resourceBuilder.buildInsertion(model, entityManager);

    final DomainValidator<S, E> validator = validatorFactory.getValidator(domainType);
    final DomainValidation validation = validator.isSatisfiedBy(entityManager, id, model);

    if (!validation.isOk()) {
      throw new DomainValidationException(domainType, validation.getErrors());
    }

    eventListenerGroups.firePrePersist(domainType, model);
    entityManager.persist(model);
    eventListenerGroups.firePostPersist(domainType, model);

    return model;
  }

  /**
   * Do an existence check for any non-auto-generated-identifier {@link DomainResource}.
   * <p>
   * Notice: This check may also pass in case the {@link jakarta.persistence.Inheritance} applied to
   * the resource is {@link jakarta.persistence.InheritanceType#JOINED}.
   * </p>
   */
  @SuppressWarnings("unchecked")
  private <T extends DomainResource> void checkIdentity(Serializable id, T model,
      EntityManager entityManager) {
    final Class<IdentifiableResource<?>> domainType = (Class<IdentifiableResource<?>>) model.getClass();
    // if this resource's id is auto-generated, we skip
    if (resourceContext.getMetadata(domainType).require(IdentifiableResourceMetadata.class)
        .isIdentifierAutoGenerated()) {
      return;
    }

    if (!(entityManager instanceof SharedSessionContract session)) {
      throw new UnsupportedOperationException();
    }

    if (genericRepository.doesExist(domainType, HibernateUtils.hasId(domainType, id, session),
        session)) {
      throw domainExceptionTranslator.existsFrom(id, domainType);
    }
  }

  @Override
  public <S extends Serializable, E extends IdentifiableResource<S>> E update(Class<E> domainType,
      S id, E model, EntityManager entityManager) throws DomainValidationException {
    if (logger.isDebugEnabled()) {
      logger.debug("Updating a resource of domainType {} with identifier {}", domainType.getName(),
          id);
    }

    final S actualId = Optional.ofNullable(id).orElse(model.getId());
    final E persistence = findPersistence(domainType, entityManager, actualId);
    final DomainBuilder<E> resourceBuilder = builderFactory.getBuilder(domainType);

    resourceBuilder.buildUpdate(model, persistence, entityManager);

    final DomainValidator<S, E> validator = validatorFactory.getValidator(domainType);
    final DomainValidation validation = validator.isSatisfiedBy(entityManager, actualId,
        persistence);

    if (!validation.isOk()) {
      throw new DomainValidationException(domainType, validation.getErrors());
    }

    return entityManager.merge(persistence);
  }

  private <S extends Serializable, E extends IdentifiableResource<S>> E findPersistence(
      Class<E> domainType, EntityManager entityManager, S actualId) {
    return Optional.ofNullable(entityManager.find(domainType, actualId))
        .orElseThrow(() -> domainExceptionTranslator.notFoundFrom(actualId, domainType));
  }

  @Override
  public <S extends Serializable, E extends IdentifiableResource<S>> List<Map<String, Object>> readAll(
      Class<E> domainType,
      List<String> attributes,
      Pageable pageable,
      GrantedAuthority authority,
      EntityManager entityManager) throws UnauthorizedDomainAccessException {
    return readAll(domainType, attributes, HibernateUtils.any(), pageable, authority,
        entityManager);
  }

  @Override
  public <S extends Serializable, E extends IdentifiableResource<S>> List<Map<String, Object>> readAll(
      Class<E> domainType,
      List<String> attributes,
      GrantedAuthority authority,
      EntityManager entityManager) throws UnauthorizedDomainAccessException {
    return readAll(domainType, attributes, Pageables.TEN, authority, entityManager);
  }

  @Override
  public <S extends Serializable, E extends IdentifiableResource<S>> List<Map<String, Object>> readAll(
      Class<E> domainType,
      List<String> attributes,
      Specification<E> specification,
      GrantedAuthority authority,
      EntityManager entityManager) throws UnauthorizedDomainAccessException {
    return readAll(domainType, attributes, specification, Pageables.TEN, authority, entityManager);
  }

  @Override
  public <S extends Serializable, E extends IdentifiableResource<S>> List<Map<String, Object>> readAll(
      Class<E> domainType,
      List<String> attributes,
      Specification<E> specification,
      Pageable pageable,
      GrantedAuthority authority,
      EntityManager entityManager) throws UnauthorizedDomainAccessException {
    securityManager.assertAuthority(domainType, attributes, authority);

    if (!(entityManager instanceof SharedSessionContract session)) {
      throw new UnsupportedOperationException();
    }

    final List<Tuple> tuples = genericRepository.findAll(domainType, toSelector(attributes),
        specification,
        pageable, session);

    return resolveRows(tuples, attributes);
  }

  private List<Map<String, Object>> resolveRows(List<Tuple> tuples,
      List<String> attributes) {
    final int span = attributes.size();

    return tuples.stream()
        .map(tuple -> IntStream.range(0, span)
            .boxed()
            .collect(Collectors.toMap(attributes::get, tuple::get)))
        .toList();
  }

  @SuppressWarnings("squid:S6204")
  private static <D extends DomainResource, E> Selector<D, E> toSelector(List<String> attributes) {
    return (root, query, builder) -> attributes.stream().map(root::get)
        .collect(Collectors.toList());
  }

  @Override
  public <S extends Serializable, E extends IdentifiableResource<S>> Map<String, Object> readOne(
      Class<E> domainType,
      List<String> attributes,
      Specification<E> specification,
      GrantedAuthority authority,
      EntityManager entityManager) throws UnauthorizedDomainAccessException {
    return readAll(domainType,
        attributes,
        specification,
        Pageables.ONE,
        authority,
        entityManager).get(0);
  }

  @Override
  public <S extends Serializable, E extends IdentifiableResource<S>> Map<String, Object> readById(
      Class<E> domainType,
      Serializable id,
      List<String> attributes,
      GrantedAuthority authority,
      EntityManager entityManager) throws UnauthorizedDomainAccessException {
    if (!(entityManager instanceof SharedSessionContract session)) {
      throw new UnsupportedOperationException();
    }

    return readOne(domainType,
        attributes,
        HibernateUtils.hasId(domainType, id, session),
        authority,
        entityManager);
  }

  @Override
  public <D extends DomainResource> List<Map<String, Object>> readAll(
      RestQuery<D> restQuery,
      GrantedAuthority authority,
      EntityManager entityManager)
      throws ReflectiveOperationException, UnauthorizedDomainAccessException {
    if (!(entityManager instanceof SharedSessionContract session)) {
      throw new UnsupportedOperationException();
    }

    return new RestQueryProcessingUnit<>(genericRepository,
        selectionProvidersFactory,
        entityManager.getCriteriaBuilder(),
        restQueryComposer.compose(restQuery, authority, true))
        .readAll(session);
  }

  @Override
  public <D extends DomainResource> Map<String, Object> readOne(
      RestQuery<D> restQuery,
      GrantedAuthority authority,
      EntityManager entityManager)
      throws ReflectiveOperationException, UnauthorizedDomainAccessException {
    if (!(entityManager instanceof SharedSessionContract session)) {
      throw new UnsupportedOperationException();
    }

    return new RestQueryProcessingUnit<>(genericRepository,
        selectionProvidersFactory,
        entityManager.getCriteriaBuilder(),
        restQueryComposer.compose(restQuery, authority, false))
        .read(session);
  }
}
