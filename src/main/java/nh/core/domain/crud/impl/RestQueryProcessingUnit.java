package nh.core.domain.crud.impl;

import static nh.core.declaration.Declarations.of;

import jakarta.persistence.Tuple;
import jakarta.persistence.TupleElement;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Selection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import nh.core.constants.Pageables;
import nh.core.constants.Strings;
import nh.core.declaration.BiDeclaration;
import nh.core.domain.DomainResource;
import nh.core.domain.repository.GenericRepository;
import nh.core.domain.repository.Selector;
import nh.core.domain.rest.ComposedNonBatchingRestQuery;
import nh.core.domain.rest.ComposedRestQuery;
import nh.core.domain.rest.filter.Filter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SharedSessionContract;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public class RestQueryProcessingUnit<D extends DomainResource> {

  private static final String ROOT_KEY_IN_CACHE = "<ROOT>";

  private final GenericRepository genericRepository;
  private final SelectionProvidersFactory selectionProvidersFactory;
  private final CriteriaBuilder criteriaBuilder;

  private final ComposedRestQuery<D> query;
  private final Class<D> domainType;
  private final List<String> basicAttributes;
  private final List<ComposedNonBatchingRestQuery<?>> nonBatchingQueries;
  private final List<ComposedRestQuery<?>> batchingQueries;
  private final Pageable pageable;

  private final Map<String, From<?, ?>> fromsCache = new HashMap<>();

  public RestQueryProcessingUnit(GenericRepository genericRepository,
      SelectionProvidersFactory selectionProvidersFactory,
      CriteriaBuilder criteriaBuilder,
      ComposedRestQuery<D> query) {
    this.genericRepository = genericRepository;
    this.selectionProvidersFactory = selectionProvidersFactory;
    this.criteriaBuilder = criteriaBuilder;

    this.query = query;
    domainType = query.getResourceType();
    basicAttributes = query.getAttributes();
    nonBatchingQueries = query.getNonBatchingAssociationQueries();
    batchingQueries = query.getBatchingAssociationQueries();
    pageable = Optional.<Pageable>ofNullable(query.getPage()).orElse(Pageables.TEN);
  }

  public List<Map<String, Object>> readAll(SharedSessionContract session) {
    final List<Tuple> tuples = genericRepository.findAll(domainType, resolveSelector(),
        resolveSpecification(), pageable, session);

    if (tuples.isEmpty()) {
      return Collections.emptyList();
    }

    return transformRows(tuples);
  }

  public Map<String, Object> read(SharedSessionContract session) {
			// @formatter:off
			final Optional<Tuple> optionalTuple = genericRepository.findOne(
					domainType,
					resolveSelector(),
					resolveSpecification(),
					session);
			// @formatter:on
			if (optionalTuple.isEmpty()) {
				return Collections.emptyMap();
			}

			final Map<String, Object> result = transformRow(optionalTuple.get());

			for (final ComposedRestQuery<?> batchingQuery : batchingQueries) {
				result.put(batchingQuery.getAssociationName(),
            new RestQueryProcessingUnit<>(genericRepository, selectionProvidersFactory,
                criteriaBuilder, batchingQuery).readAll(session));
			}

			return result;
		}

  private Specification<D> resolveSpecification() {
    return (root, cq, builder) -> Optional.ofNullable(resolvePredicates(root, Strings.EMPTY, query))
        .orElse(builder.conjunction());
  }

  private Predicate resolvePredicates(From<?, ?> from, String joinRole,
      ComposedRestQuery<?> composedQuery) {
    return of(composedQuery.getResourceType(), from, composedQuery.getFilters())
        .map(this::resolveBasicPredicates)
        .second(composedQuery)
        .third(Map.<From<?, ?>, String>entry(from, joinRole))
        .triInverse()
        .map(this::addAssociationPredicates)
        .map(predicates -> predicates.isEmpty() ? null
            : criteriaBuilder.and(predicates.toArray(Predicate[]::new)))
        .get();
  }

  private List<Predicate> addAssociationPredicates(Entry<From<?, ?>, String> fromEntry,
      ComposedRestQuery<?> composedAssociationQuery,
      List<Predicate> basicPredicates) {
    for (final ComposedRestQuery<?> nonBatchingQuery : composedAssociationQuery.getNonBatchingAssociationQueries()) {
      final String nextJoinRole = resolveJoinRole(fromEntry.getValue(),
          nonBatchingQuery.getAssociationName());
      final Predicate associationPredicate = resolvePredicates(
          fromsCache.containsKey(nextJoinRole) ? fromsCache.get(nextJoinRole)
              : fromEntry.getKey().join(composedAssociationQuery.getAssociationName()),
          nextJoinRole,
          nonBatchingQuery);

      if (associationPredicate == null) {
        continue;
      }

      basicPredicates.add(associationPredicate);
    }

    return basicPredicates;
  }

  private <E extends DomainResource> List<Predicate> resolveBasicPredicates(
      Class<E> resourceType,
      From<?, ?> from,
      Map<String, Filter> filters) {

    if (filters.isEmpty()) {
      return new ArrayList<>();
    }

    final List<Predicate> predicates = new ArrayList<>();

    for (Map.Entry<String, Filter> filterEntry : filters.entrySet()) {

      final Predicate filterPredicate = extractFilterPredicate(
          resourceType,
          from,
          filterEntry.getKey(),
          filterEntry.getValue());

      if (filterPredicate == null) {
        continue;
      }

      predicates.add(filterPredicate);
    }

    return predicates;
  }

  private <E extends DomainResource> Predicate extractFilterPredicate(Class<E> resourceType,
      From<?, ?> from, String attributeName, Filter filter) {
    final List<BiFunction<Path<?>, CriteriaBuilder, Predicate>> expressionProducers = filter
        .getExpressionProducers();
    final Path<?> attributePath = selectionProvidersFactory
        .getSelectionProviders(resourceType)
        .get(attributeName)
        .apply(from);
    final int size = expressionProducers.size();

    Predicate predicate = expressionProducers.get(0).apply(attributePath, criteriaBuilder);

    for (int i = 1; i < size; i++) {
      predicate = criteriaBuilder.or(predicate,
          expressionProducers.get(i).apply(attributePath, criteriaBuilder));
    }

    return predicate;
  }

  private Selector<D, Tuple> resolveSelector() {
    return (root, cq, builder) -> of(root, ROOT_KEY_IN_CACHE)
        .consume(this::cache)
        .second(selectionProvidersFactory.getSelectionProviders(domainType))
        .third(this::provideBasicSelections)
        .map(this::addAssociationSelections)
        .get();
  }

  private <T extends DomainResource> List<Selection<?>> addAssociationSelections(Root<T> root,
      Map<String, Function<Path<?>, Path<?>>> selectionProviders,
      List<Selection<?>> basicSelections) {
    for (final ComposedRestQuery<?> nonBatchingQuery : nonBatchingQueries) {
      of(nonBatchingQuery.getAssociationName())
          .second(joinName -> (Join<?, ?>) selectionProviders.get(joinName).apply(root))
          .biInverse()
          .consume(this::cache)
          .third(nonBatchingQuery)
          .map(this::resolveJoinedSelections)
          .consume(basicSelections::addAll);
    }

    return basicSelections;
  }

  private List<Selection<?>> resolveJoinedSelections(Join<?, ?> join, String joinRole,
      ComposedRestQuery<?> composedAssociationQuery) {
    return of(join)
        .second(composedAssociationQuery.getAttributes())
        .third(composedAssociationQuery.getNonBatchingAssociationQueries())
        .map(this::provideAssociationBasicSelections)
        .second(composedAssociationQuery.getNonBatchingAssociationQueries())
        .third(Map.<Join<?, ?>, String>entry(join, joinRole))
        .triInverse()
        .map(this::addNestedAssociationSelections)
        .get();
  }

  private List<Selection<?>> addNestedAssociationSelections(
      Entry<Join<?, ?>, String> joinEntry,
      List<ComposedNonBatchingRestQuery<?>> joinedNonBatchingQueries,
      List<Selection<?>> associationBasicSelections) {
    for (final ComposedRestQuery<?> nonBatchingAssociationQuery : joinedNonBatchingQueries) {
      final String associationName = nonBatchingAssociationQuery.getAssociationName();
      final Join<?, ?> join = joinEntry.getKey().join(associationName);
      final String joinRole = resolveJoinRole(joinEntry.getValue(), associationName);

      cache(join, joinRole);

      associationBasicSelections.addAll(
          resolveJoinedSelections(join, joinRole, nonBatchingAssociationQuery));
    }

    return associationBasicSelections;
  }

  private String resolveJoinRole(String joinRole, String nextJoinName) {
    return StringUtils.isBlank(joinRole) ? String.join(Strings.DOT, List.of(joinRole, nextJoinName))
        : nextJoinName;
  }

  @SuppressWarnings("unchecked")
  private List<Selection<?>> provideAssociationBasicSelections(Join<?, ?> join,
      List<String> joinedAttributes,
      List<ComposedNonBatchingRestQuery<?>> joinedNonBatchingQueries) {
    final List<Selection<?>> selections = new ArrayList<>(
        joinedAttributes.size() + joinedNonBatchingQueries.size());

    for (String joinedAttribute : joinedAttributes) {
      of(join.getJavaType())
          .map(type -> (Class<DomainResource>) type)
          .map(selectionProvidersFactory::getSelectionProviders)
          .map(selectionsProducers -> selectionsProducers.get(joinedAttribute))
          .map(producer -> producer.apply(join))
          .consume(selections::add);
    }

    return selections;
  }

  @SuppressWarnings("squid:S6204")
  private <T extends DomainResource> List<Selection<?>> provideBasicSelections(Root<T> root,
      Map<String, Function<Path<?>, Path<?>>> selectionProviders) {
    return basicAttributes
        .stream()
        .map(selectionProviders::get)
        .map(producer -> producer.apply(root))
        .collect(Collectors.toList());
  }

  private void cache(From<?, ?> from, String key) {
    fromsCache.put(key, from);
  }

  private List<Map<String, Object>> transformRows(List<Tuple> tuples) {
    return tuples.stream().map(this::transformRow).toList();
  }

  private Map<String, Object> transformRow(Tuple tuple) {
    return transformRow(query, basicAttributes, tuple);
  }

  private Map<String, Object> transformRow(ComposedRestQuery<?> composedQuery,
      List<String> attributes, Tuple tuple) {
    final Map<String, Object> result = IntStream.range(0, attributes.size())
        .mapToObj(index -> of(attributes.get(index), tuple.get(index)))
        .collect(Collectors.toMap(BiDeclaration::get, BiDeclaration::getSecond));

    for (final ComposedNonBatchingRestQuery<?> nonBatchingAssociation : composedQuery
        .getNonBatchingAssociationQueries()) {
      result.put(
          nonBatchingAssociation.getAssociationName(),
          transformRow(nonBatchingAssociation, nonBatchingAssociation.getAttributes(),
              new AssociationTuple(nonBatchingAssociation, tuple)));
    }

    return result;
  }

  private record AssociationTuple(ComposedNonBatchingRestQuery<?> composedQuery,
                                  Tuple owningTuple) implements Tuple {

    @Override
    public <X> X get(TupleElement<X> tupleElement) {
      return owningTuple.get(tupleElement);
    }

    @Override
    public <X> X get(String alias, Class<X> type) {
      return owningTuple.get(alias, type);
    }

    @Override
    public Object get(String alias) {
      return owningTuple.get(alias);
    }

    private int getActualIndex(int i) {
      return composedQuery.getAssociatedPosition() + i;
    }

    @Override
    public <X> X get(int i, Class<X> type) {
      return owningTuple.get(getActualIndex(i), type);
    }

    @Override
    public Object get(int i) {
      return owningTuple.get(getActualIndex(i));
    }

    @Override
    public Object[] toArray() {
      return IntStream.range(composedQuery.getAssociatedPosition(), composedQuery.getPropertySpan())
          .mapToObj(owningTuple::get)
          .toArray();
    }

    @Override
    public List<TupleElement<?>> getElements() {
      final List<TupleElement<?>> elements = owningTuple.getElements();

      return IntStream.range(composedQuery.getAssociatedPosition(), composedQuery.getPropertySpan())
          .mapToObj(elements::get)
          .collect(Collectors.toList());
    }

  }

}
