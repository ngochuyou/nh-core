package nh.core.domain.crud.impl;

import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Path;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.function.Function;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.graph.impl.DomainResourceGraphCollectors;
import nh.core.domain.metadata.ComponentPath;
import nh.core.domain.metadata.DomainResourceAttributesMetadata;
import nh.core.domain.metadata.DomainResourceMetadata;

public class SelectionProvidersFactory {

  @SuppressWarnings("rawtypes")
  private final Map<Class, Map<String, Function<Path<?>, Path<?>>>> selectionProducers;

  @SuppressWarnings({"unchecked", "rawtypes", "squid:S135"})
  public SelectionProvidersFactory(DomainResourceContext domainContext) {
    final Map<Class, Map<String, Function<Path<?>, Path<?>>>> selectionProducerCandidates =
        new HashMap<>();

    for (final Class domainType : domainContext.getResourceGraph()
        .collect(DomainResourceGraphCollectors.toTypesSet())) {
      final DomainResourceMetadata metadata = domainContext.getMetadata(domainType);

      if (Objects.isNull(metadata)) {
        continue;
      }

      final DomainResourceAttributesMetadata attributesMetadata = (DomainResourceAttributesMetadata)
          metadata.require(DomainResourceAttributesMetadata.class);
      final List<String> attributes = attributesMetadata.getAttributeNames();
      final Map<String, Function<Path<?>, Path<?>>> producers = new HashMap<>();

      for (final String attribute : attributes) {
        if (attributesMetadata.isComponent(attribute)) {
          producers.put(attribute, resolveComponentPathProducers(attributesMetadata, attribute));
          continue;
        }

        if (attributesMetadata.isAssociation(attribute)) {
          producers.put(attribute,
              attributesMetadata.isAssociationOptional(attribute)
                  ? path -> ((From) path).join(attribute, JoinType.LEFT)
                  : path -> ((From) path).join(attribute));
          continue;
        }

        producers.put(attribute, path -> path.get(attribute));
      }

      selectionProducerCandidates.put(domainType, producers);
    }

    this.selectionProducers = Collections.unmodifiableMap(selectionProducerCandidates);
  }

  @SuppressWarnings("rawtypes")
  private Function<Path<?>, Path<?>> resolveComponentPathProducers(
      DomainResourceAttributesMetadata metadata, String attributeName) {
    final List<Function<Path<?>, Path<?>>> elementalProducers = resolveElementalProducers(metadata,
        attributeName,
        (ComponentPath) metadata.getComponentPaths().get(attributeName));
    // this produces a chain of functions, eventually invoke the final product (a function chain)
    // with the root arg
    return elementalProducers.stream()
        .reduce((leadingFunction, followingFunction) ->
            currentPath -> followingFunction.apply(leadingFunction.apply(currentPath)))
        .orElseThrow(() -> new IllegalStateException("Unable to resolve elemental producers"));
  }

  @SuppressWarnings("rawtypes")
  private List<Function<Path<?>, Path<?>>> resolveElementalProducers(
      DomainResourceAttributesMetadata metadata,
      String attributeName,
      ComponentPath componentPath) {
    final Queue<String> elementNames = componentPath.getPath();

    if (metadata.isAssociation(attributeName)) {
      final List<Function<Path<?>, Path<?>>> elementalProducers = new ArrayList<>();
      final Queue<String> copiedNodeNames = new ArrayDeque<>(elementNames);

      elementalProducers.add((metadata.isAssociationOptional(attributeName)
          ? (Function<String, Function<Path<?>, Path<?>>>) nodeName -> (Function<Path<?>, Path<?>>) path -> ((From) path).join(
          nodeName, JoinType.LEFT)
          : (Function<String, Function<Path<?>, Path<?>>>) nodeName -> (Function<Path<?>, Path<?>>) path -> ((From) path).join(
              nodeName)).apply(
          copiedNodeNames.poll()));

      while (copiedNodeNames.size() > 1) {
        final String name = copiedNodeNames.poll();

        elementalProducers.add(join -> ((Join) join).join(name));
      }

      final String lastNode = copiedNodeNames.poll();

      elementalProducers.add(metadata.isAssociationOptional(lastNode)
          ? path -> ((Join) path).join(lastNode, JoinType.LEFT)
          : path -> ((Join) path).join(lastNode));

      return elementalProducers;
    }

    return elementNames.stream().map(nodeName -> (Function<Path<?>, Path<?>>) path -> path.get(nodeName))
        .toList();
  }

  @SuppressWarnings({"squid:S1452"})
  public <D extends DomainResource> Map<String, Function<Path<?>, Path<?>>> getSelectionProviders(
      Class<D> resourceType) {
    return selectionProducers.get(resourceType);
  }

}
