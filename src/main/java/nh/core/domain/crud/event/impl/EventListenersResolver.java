/**
 *
 */
package nh.core.domain.crud.event.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import nh.core.domain.DomainResource;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.EncryptedIdentifierResource;
import nh.core.domain.crud.event.CrudEventListener;
import nh.core.domain.crud.event.persist.PostPersistEventListener;
import nh.core.domain.crud.event.persist.PrePersistEventListener;
import nh.core.domain.crud.event.persist.impl.EncryptedIdentifiableDomainPostPersistEventListener;
import nh.core.domain.crud.event.persist.impl.EncryptedIdentifiableDomainPrePersistEventListener;
import nh.core.domain.graph.impl.DomainResourceGraphCollectors;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.util.ClassUtils;
import org.springframework.util.CollectionUtils;

/**
 * @author Ngoc Huy
 */
public class EventListenersResolver {

  private final LinkedHashSet<Class<? extends DomainResource>> domainTypeSet;
  private final AutowireCapableBeanFactory beanFactory;

  private final Map<Class<? extends DomainResource>, BiFunction<Class<? extends DomainResource>, AutowireCapableBeanFactory, List<CrudEventListener>>> postPersistListeners;
  private final Map<Class<? extends DomainResource>, BiFunction<Class<? extends DomainResource>, AutowireCapableBeanFactory, List<CrudEventListener>>> prePersistListeners;

  private final Map<Class<? extends CrudEventListener>, Map<Class<? extends DomainResource>, BiFunction<Class<? extends DomainResource>, AutowireCapableBeanFactory, List<CrudEventListener>>>> listenerResolverMappings;

  public EventListenersResolver(DomainResourceContext domainContext,
      AutowireCapableBeanFactory beanFactory,
      EncryptedIdentifiableDomainPostPersistEventListener encryptedIdentifiableDomainPostPersistEventListener,
      EncryptedIdentifiableDomainPrePersistEventListener encryptedIdentifiableDomainPrePersistEventListener) {
    domainTypeSet = domainContext.getResourceGraph()
        .collect(DomainResourceGraphCollectors.toTypesSet());
    this.beanFactory = beanFactory;
    postPersistListeners = Map.of(
        EncryptedIdentifierResource.class, (domainType, factory) -> List.of(
            encryptedIdentifiableDomainPostPersistEventListener));
    prePersistListeners = Map.of(
        EncryptedIdentifierResource.class, (domainType, factory) -> List.of(
            encryptedIdentifiableDomainPrePersistEventListener));
    listenerResolverMappings = Map.of(
        PrePersistEventListener.class, prePersistListeners,
        PostPersistEventListener.class, postPersistListeners);
  }

  @SuppressWarnings("unchecked")
  public <E extends CrudEventListener> Map<Class<DomainResource>, List<E>> resolveListeners(
      Class<E> listenerType) {
    if (!listenerResolverMappings.containsKey(listenerType)) {
      throw new IllegalArgumentException(
          String.format("Unsupported listener type %s", listenerType));
    }

    final Map<Class<? extends DomainResource>, BiFunction<Class<? extends DomainResource>, AutowireCapableBeanFactory, List<CrudEventListener>>> listenerResolvers = listenerResolverMappings.get(
        listenerType);
    final Map<Class<? extends DomainResource>, List<CrudEventListener>> listeners = new HashMap<>();

    for (final Class<? extends DomainResource> domainType : domainTypeSet) {
      for (final Class<?> interfaceType : ClassUtils.getAllInterfacesForClassAsSet(domainType)) {
        if (!listenerResolvers.containsKey(interfaceType)) {
          continue;
        }

        listeners.compute(domainType, (key, currentListeners) -> {
          final List<CrudEventListener> nextListeners = listenerResolvers.get(interfaceType)
              .apply(domainType, beanFactory);

          if (CollectionUtils.isEmpty(nextListeners)) {
            return currentListeners;
          }

          if (CollectionUtils.isEmpty(currentListeners)) {
            return nextListeners;
          }

          return Stream.of(currentListeners, nextListeners)
              .flatMap(Collection::stream)
              .toList();
        });
      }
    }

    return listeners.entrySet()
        .stream()
        .collect(Collectors.toMap(
            entry -> (Class<DomainResource>) entry.getKey(),
            entry -> (List<E>) entry.getValue()));
  }

}
