package nh.core.domain.crud.event.persist.impl;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Map;
import nh.core.domain.DomainResource;
import nh.core.domain.EncryptedIdentifierResource;
import nh.core.domain.IdentifiableResource;
import nh.core.domain.crud.event.persist.PostPersistEventListener;
import nh.core.function.HandledFunction;
import nh.core.util.Base32;
import nh.core.util.TypeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncryptedIdentifiableDomainPostPersistEventListener implements
    PostPersistEventListener {

  private static final Logger logger = LoggerFactory.getLogger(
      EncryptedIdentifiableDomainPostPersistEventListener.class);

  private static final Map<Class<?>, HandledFunction<Object, BigInteger, Exception>> CASTING_FUNCTIONS = Map.of(
      int.class, EncryptedIdentifiableDomainPostPersistEventListener::fromInt,
      Integer.class, EncryptedIdentifiableDomainPostPersistEventListener::fromInt,
      long.class, EncryptedIdentifiableDomainPostPersistEventListener::fromLong,
      Long.class, EncryptedIdentifiableDomainPostPersistEventListener::fromLong);

  private static BigInteger fromLong(Object longVal) throws Exception {
    return TypeUtils.cast(long.class, BigInteger.class, (long) longVal);
  }

  private static BigInteger fromInt(Object intVal) throws Exception {
    return TypeUtils.cast(int.class, BigInteger.class, (int) intVal);
  }

  @SuppressWarnings("rawtypes")
  @Override
  public <D extends DomainResource> void doPostPersist(D resource) {
    if (!(resource instanceof EncryptedIdentifierResource encryptedDomain)) {
      throw new IllegalArgumentException(String.format("An instance of type %s is required",
          IdentifiableResource.class));
    }

    final Serializable idCandidate = encryptedDomain.getId();
    final String code = Base32.crockfords.format(resolveId(idCandidate));

    encryptedDomain.setCode(code);

    if (logger.isDebugEnabled()) {
      logger.trace("Generated code {} from identifier {}", code, idCandidate);
    }
  }

  private BigInteger resolveId(Serializable id) {
    final Class<? extends Serializable> idType = id.getClass();

    if (BigInteger.class.isAssignableFrom(idType)) {
      return (BigInteger) id;
    }

    if (!CASTING_FUNCTIONS.containsKey(idType)) {
      throw new IllegalArgumentException(String.format("Unsupported identifier type %s", idType));
    }

    try {
      return CASTING_FUNCTIONS.get(idType).apply(id);
    } catch (Exception e) {
      throw new IllegalStateException(e);
    }
  }

}
