package nh.core.domain.crud.event.impl;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import nh.core.domain.DomainResource;
import nh.core.domain.crud.event.CrudEventListener;
import nh.core.domain.crud.event.persist.PostPersistEventListener;
import nh.core.domain.crud.event.persist.PrePersistEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventListenerGroups {

  private static final Logger logger = LoggerFactory.getLogger(EventListenerGroups.class);

  private final Map<Class<DomainResource>, List<PostPersistEventListener>> postPersistListeners;
  private final Map<Class<DomainResource>, List<PrePersistEventListener>> prePersistListeners;

  public EventListenerGroups(EventListenersResolver resolver) {
    postPersistListeners = resolver.resolveListeners(PostPersistEventListener.class);
    prePersistListeners = resolver.resolveListeners(PrePersistEventListener.class);
  }

  public <D extends DomainResource> void firePostPersist(Class<D> resourceType, D domain) {
    fireEvent(PostPersistEventListener.class,
        postPersistListeners,
        PostPersistEventListener::doPostPersist,
        resourceType,
        domain);
  }

  public <D extends DomainResource> void firePrePersist(Class<D> resourceType, D domain) {
    fireEvent(PrePersistEventListener.class,
        prePersistListeners,
        PrePersistEventListener::doPrePersist,
        resourceType,
        domain);
  }

  private <D extends DomainResource, E extends CrudEventListener> void fireEvent(
      Class<E> listenerType,
      Map<Class<DomainResource>, List<E>> listeners,
      BiConsumer<E, D> invoker,
      Class<D> resourceType,
      D domain) {
    if (!listeners.containsKey(resourceType)) {
      return;
    }

    logger.debug("Firing event on resource type {}, listeners type {}", resourceType.getName(), listenerType);

    for (final E listener : listeners.get(resourceType)) {
      invoker.accept(listener, domain);
    }
  }

}
