/**
 *
 */
package nh.core.domain.crud.event.persist;

import nh.core.domain.DomainResource;
import nh.core.domain.crud.event.CrudEventListener;

/**
 * @author Ngoc Huy
 *
 */
public interface PrePersistEventListener extends CrudEventListener {

	<D extends DomainResource> void doPrePersist(D resource);

}
