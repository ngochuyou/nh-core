package nh.core.domain.crud.event.persist.impl;

import nh.core.domain.DomainResource;
import nh.core.domain.EncryptedIdentifierResource;
import nh.core.domain.crud.event.persist.PrePersistEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncryptedIdentifiableDomainPrePersistEventListener implements
    PrePersistEventListener {

  private static final Logger logger = LoggerFactory.getLogger(
      EncryptedIdentifiableDomainPrePersistEventListener.class);
  private static final String PLACEHOLDER = "PLACEHOLDER";

  @SuppressWarnings("rawtypes")
  @Override
  public <D extends DomainResource> void doPrePersist(D resource) {
    logger.debug("Assigning temporary code");

    if (!(resource instanceof EncryptedIdentifierResource encryptedDomain)) {
      throw new IllegalArgumentException(String.format("An instance of type %s is required",
          EncryptedIdentifierResource.class));
    }

    encryptedDomain.setCode(PLACEHOLDER);
  }

}
