/**
 *
 */
package nh.core.domain.crud;

import jakarta.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;
import nh.core.domain.IdentifiableResource;
import nh.core.exception.UnauthorizedDomainAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public interface GenericReadService<R> {

  <S extends Serializable, E extends IdentifiableResource<S>> List<R> readAll(Class<E> type,
      List<String> properties,
      Pageable pageable,
      GrantedAuthority credential,
      EntityManager entityManager) throws UnauthorizedDomainAccessException;

  <S extends Serializable, E extends IdentifiableResource<S>> List<R> readAll(Class<E> type,
      List<String> properties,
      GrantedAuthority credential,
      EntityManager entityManager) throws UnauthorizedDomainAccessException;

  <S extends Serializable, E extends IdentifiableResource<S>> List<R> readAll(Class<E> type,
      List<String> properties,
      Specification<E> specification,
      Pageable pageable,
      GrantedAuthority credential,
      EntityManager entityManager) throws UnauthorizedDomainAccessException;

  <S extends Serializable, E extends IdentifiableResource<S>> List<R> readAll(Class<E> type,
      List<String> properties,
      Specification<E> specification,
      GrantedAuthority credential,
      EntityManager entityManager) throws UnauthorizedDomainAccessException;

  /* ==================== */
  <S extends Serializable, E extends IdentifiableResource<S>> R readOne(Class<E> type,
      List<String> properties,
      Specification<E> specification,
      GrantedAuthority credential,
      EntityManager entityManager) throws UnauthorizedDomainAccessException;

  /* ==================== */
  <S extends Serializable, E extends IdentifiableResource<S>> R readById(Class<E> type,
      Serializable id,
      List<String> properties,
      GrantedAuthority credential,
      EntityManager entityManager) throws UnauthorizedDomainAccessException;
}
