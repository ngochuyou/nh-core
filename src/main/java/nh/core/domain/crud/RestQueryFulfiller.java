/**
 *
 */
package nh.core.domain.crud;

import jakarta.persistence.EntityManager;
import java.util.List;
import nh.core.domain.DomainResource;
import nh.core.domain.rest.RestQuery;
import nh.core.exception.UnauthorizedDomainAccessException;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ngoc Huy
 */
public interface RestQueryFulfiller<R> {

  <D extends DomainResource> List<R> readAll(RestQuery<D> restQuery, GrantedAuthority authority,
      EntityManager entityManager)
      throws UnauthorizedDomainAccessException, ReflectiveOperationException;

  <D extends DomainResource> R readOne(RestQuery<D> restQuery, GrantedAuthority authority,
      EntityManager entityManager)
      throws UnauthorizedDomainAccessException, ReflectiveOperationException;

}
