/**
 *
 */
package nh.core.domain.crud;

import jakarta.persistence.EntityManager;
import java.io.Serializable;
import nh.core.domain.IdentifiableResource;
import nh.core.exception.DomainValidationException;

/**
 * @author Ngoc Huy
 */
public interface GenericCUDService {

  <S extends Serializable, E extends IdentifiableResource<S>> E create(Class<E> type,
      S id,
      E model, EntityManager entityManager) throws DomainValidationException;

  <S extends Serializable, E extends IdentifiableResource<S>> E update(Class<E> type,
      S id,
      E model, EntityManager entityManager) throws DomainValidationException;

}
