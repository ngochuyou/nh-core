package nh.core.domain.crud;

public interface GenericCrudServiceImplementor<R>
    extends GenericCUDService, GenericReadService<R>, RestQueryFulfiller<R> {

}
