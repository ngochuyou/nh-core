/**
 *
 */
package nh.core.util;

import static nh.core.constants.Strings.SPACE;
import static org.apache.logging.log4j.util.Strings.EMPTY;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import nh.core.constants.Strings;

/**
 * @author Ngoc Huy
 */
@SuppressWarnings("squid:S1610")
public abstract class StringUtils {

  private StringUtils() {
    throw new UnsupportedOperationException();
  }

  public static <T> String join(Collection<T> elements) {
    return join(
        value -> Optional.ofNullable(value).map(Object::toString).orElse(Strings.NULL), elements);
  }

  public static <T> String join(Function<T, String> stringMapper, Collection<T> elements) {
    return join(Strings.COMMON_JOINER, stringMapper, elements);
  }

  public static <T> String join(CharSequence joiner, Collection<T> elements) {
    return join(joiner, Object::toString, elements);
  }

  public static <T> String join(
      CharSequence joiner,
      Function<T, String> stringMapper,
      Collection<T> elements) {
    return elements.stream().map(stringMapper).collect(Collectors.joining(joiner));
  }

  public static String combineIntoCamel(String first, String second) {
    return toCamel(String.format("%s%s%s", first, SPACE, second), SPACE);
  }

  public static String toCamel(String sample, CharSequence seperator) {
    if (Objects.isNull(sample)) {
      return null;
    }

    final String input = sample.trim();

    if (Objects.isNull(seperator)) {
      return toCamel(input);
    }

    final String[] parts = input.split(seperator.toString());

    if (parts.length < 2) {
      return toCamel(input);
    }

    final String firstWord = parts[0];
    final StringBuilder builder = new StringBuilder(
        (EMPTY + firstWord.charAt(0)).toLowerCase() + firstWord.substring(1));

    for (int i = 1; i < parts.length; i++) {
      final String currentWord = parts[i];

      builder.append((EMPTY + currentWord.charAt(0)).toUpperCase())
          .append(currentWord.substring(1));
    }

    return builder.toString();
  }

  private static String toCamel(final String input) {
    return (EMPTY + input.charAt(0)).toLowerCase() + input.substring(1);
  }

  public static final String WHITESPACE_CHARS = "\\u0009" // CHARACTER TABULATION
			+ "\\u000A" // LINE FEED (LF)
			+ "\\u000B" // LINE TABULATION
			+ "\\u000C" // FORM FEED (FF)
			+ "\\u000D" // CARRIAGE RETURN (CR)
			+ "\\u0020" // SPACE
			+ "\\u0085" // NEXT LINE (NEL)4
			+ "\\u00A0" // NO-BREAK SPACE
			+ "\\u1680" // OGHAM SPACE MARK
			+ "\\u180E" // MONGOLIAN VOWEL SEPARATOR
			+ "\\u2000" // EN QUAD
			+ "\\u2001" // EM QUAD
			+ "\\u2002" // EN SPACE
			+ "\\u2003" // EM SPACE
			+ "\\u2004" // THREE-PER-EM SPACE
			+ "\\u2005" // FOUR-PER-EM SPACE
			+ "\\u2006" // SIX-PER-EM SPACE
			+ "\\u2007" // FIGURE SPACE
			+ "\\u2008" // PUNCTUATION SPACE
			+ "\\u2009" // THIN SPACE
			+ "\\u200A" // HAIR SPACE
			+ "\\u2028" // LINE SEPARATOR
			+ "\\u2029" // PARAGRAPH SEPARATOR
			+ "\\u202F" // NARROW NO-BREAK SPACE
			+ "\\u205F" // MEDIUM MATHEMATICAL SPACE
			+ "\\u3000"; // IDEOGRAPHIC SPACE

	public static final String WHITESPACE_CHAR_CLASS = "[" + WHITESPACE_CHARS + "]";

	public static String normalizeString(String string) {
		return !org.apache.commons.lang3.StringUtils.isBlank(string) ? string
        : string.trim().replaceAll(WHITESPACE_CHAR_CLASS + "+", SPACE);
	}

}
