package nh.core.util;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import nh.core.declaration.BiDeclaration;
import nh.core.declaration.Declarations;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisCommands;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @author Ngoc Huy
 */
public abstract class RedisUtils {

  private RedisUtils() {
    throw new UnsupportedOperationException();
  }

  public static boolean flushDb(RedisConnection connection) throws DataAccessException {
    connection.serverCommands().flushDb();
    return true;
  }

  @SuppressWarnings("unchecked")
  public static <K, V> void bulkExpire(
      RedisOperations<K, V> redisOperations,
      Map<K, Duration> expirationPairs) {
    redisOperations.executePipelined((RedisCallback<Boolean>) connection -> {
      final RedisSerializer<K> keySerializer =
          (RedisSerializer<K>) redisOperations.getKeySerializer();
      final RedisCommands commands = connection.commands();

      expirationPairs
          .forEach((key, value) -> commands.pExpire(
              Objects.requireNonNull(keySerializer.serialize(key)),
              value.toMillis()));

      return null;
    });
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  public static <K, V> void multiHSet(
      RedisOperations<K, V> redisOperations,
      Map<K, V> values) {
    redisOperations.execute(
        (RedisCallback<Boolean>) connection -> multiSet(
            connection,
            values,
            ((RedisSerializer) redisOperations.getKeySerializer())::serialize,
            ((RedisSerializer) redisOperations.getHashValueSerializer())::serialize));
  }
  
  public static <K, V> boolean multiSet(
      RedisConnection connection,
      Map<K, V> pairs,
      Function<K, byte[]> keySerializer,
      Function<V, byte[]> valueSerializer) {
    return Boolean.TRUE.equals(connection.stringCommands().mSet(
        pairs.entrySet()
            .stream()
            .map(
                pair -> Declarations.of(
                    keySerializer.apply(pair.getKey()),
                    valueSerializer.apply(pair.getValue())))
            .collect(Collectors.toMap(BiDeclaration::get, BiDeclaration::getSecond))));
  }

  @SuppressWarnings("unchecked")
  public static <K, V> List<V> multiHGet(
      RedisOperations<K, V> redisOperations,
      List<K> keys) {
    return redisOperations.execute(
        (RedisCallback<List<V>>) connection -> multiGet(
            connection,
            keys,
            ((RedisSerializer<K>) redisOperations.getKeySerializer())::serialize,
            ((RedisSerializer<V>) redisOperations.getHashValueSerializer())::deserialize));
  }

  public static <K, V> List<V> multiGet(
      RedisConnection connection,
      List<K> keys,
      Function<K, byte[]> keySerializer,
      Function<byte[], V> valueDeserializer) {
    final List<byte[]> results = connection.commands()
        .mGet(keys.stream().map(keySerializer).toArray(byte[][]::new));

    if (Objects.isNull(results)) {
      return Collections.emptyList();
    }

    return results
        .stream()
        .map(valueDeserializer)
        .toList();
  }

}
