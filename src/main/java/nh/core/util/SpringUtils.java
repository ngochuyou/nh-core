package nh.core.util;

import static java.util.Objects.isNull;

import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;

/**
 * @author Ngoc Huy
 */
@SuppressWarnings("squid:S1610")
public abstract class SpringUtils {

  private SpringUtils() {
    throw new UnsupportedOperationException();
  }

  public static String resolvePluralEnvKey(String path, String key) {
    return String.format("%s[%s]", path, key);
  }

  public static <T> T locate(
      ApplicationContext applicationContext,
      Class<T> beanType) {
    return applicationContext.getBean(beanType);
  }

  public static void register(
      BeanDefinitionRegistry registry,
      Class<?> beanClass,
      String beanIdentifier) {
    registry.registerBeanDefinition(beanIdentifier, createBeanDef(beanClass));
  }

  public static GenericBeanDefinition createBeanDef(Class<?> beanClass) {
    final GenericBeanDefinition beanDef = new GenericBeanDefinition();

    beanDef.setBeanClass(beanClass);
    beanDef.setScope(BeanDefinition.SCOPE_SINGLETON);
    beanDef.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);

    return beanDef;
  }

  public static <T> T instantiate(BeanDefinition beanDef, AutowireCapableBeanFactory autowireCapableBeanFactory)
      throws BeansException, IllegalStateException, ClassNotFoundException {
    return instantiate(Class.forName(beanDef.getBeanClassName()), autowireCapableBeanFactory);
  }

  @SuppressWarnings("unchecked")
  public static <T> T instantiate(Class<?> beanType, AutowireCapableBeanFactory autowireCapableBeanFactory)
      throws BeansException, IllegalStateException {
    return (T) autowireCapableBeanFactory.createBean(beanType);
  }

  public static <C> Set<BeanDefinition> scan(Class<C> type, String packageName) {
    return scan(type, packageName, Function.identity(), scanner -> {});
  }

  public static <T, C> T scan(
      Class<C> type,
      String packageName,
      Function<Set<BeanDefinition>, T> mapper,
      Consumer<ClassPathScanningCandidateComponentProvider> scannerConsumer) {
    if (isNull(type) || isNull(packageName) || isNull(mapper)) {
      return null;
    }

    final ClassPathScanningCandidateComponentProvider scanner =
        new ClassPathScanningCandidateComponentProvider(false);

    scanner.addIncludeFilter(new AssignableTypeFilter(type));
    scannerConsumer.accept(scanner);

    return mapper.apply(scanner.findCandidateComponents(packageName));
  }

}
