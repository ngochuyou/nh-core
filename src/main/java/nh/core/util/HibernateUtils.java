package nh.core.util;

import java.io.Serializable;
import nh.core.constants.Hibernates;
import nh.core.domain.DomainResource;
import org.hibernate.SharedSessionContract;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author Ngoc Huy
 */
@SuppressWarnings("squid:S1610")
public abstract class HibernateUtils {

  private HibernateUtils() {
    throw new UnsupportedOperationException();
  }

  @SuppressWarnings("unchecked")
  public static <T> Specification<T> any() {
    return Hibernates.ANY;
  }

  public static <D extends DomainResource> Specification<D> hasId(
      Class<D> type,
      Serializable id,
      SharedSessionContract session) {
    return (root, query, builder) -> builder
        .equal(root.get(locateIdPropertyName(type, session)), id);
  }

  public static <D extends DomainResource> String locateIdPropertyName(
      Class<D> type,
      SharedSessionContract session) {
    return ((SharedSessionContractImplementor) session).getFactory()
        .unwrap(SessionFactoryImplementor.class)
        .getMappingMetamodel()
        .getEntityDescriptor(type)
        .getEntityMetamodel()
        .getIdentifierProperty()
        .getName();
  }

}
