package nh.core.util;

import java.util.Optional;
import nh.core.declaration.Declarations;
import nh.core.domain.DomainResource;
import nh.core.domain.annotation.Domain;
import nh.core.domain.mapping.DomainMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;

@SuppressWarnings("squid:S1610")
public abstract class DomainMappingUtils {

  private static final Logger logger = LoggerFactory.getLogger(DomainMappingUtils.class);

  private DomainMappingUtils() {
    throw new UnsupportedOperationException();
  }

  public static <C extends DomainResource, T> Optional<DomainMapping<C, T>> getMapping(
      BeanDefinition beanDef,
      AutowireCapableBeanFactory beanFactory) {
    try {
      return Declarations.of(beanDef.getBeanClassName())
          .tryMap(Class::forName)
          .map(domainType -> ReflectionUtils.requireAnnotation(domainType, Domain.class,
              Domain::value))
          .map(DomainMappingUtils::<C>cast)
          .second(SpringUtils.<T>instantiate(beanDef, beanFactory))
          .map(DomainMapping::of)
          .map(Optional::ofNullable)
          .get();
    } catch (Exception e) {
      logger.warn("Unable to resolve mapping for type {} due to {}", beanDef.getBeanClassName(),
          e.getMessage());

      if (logger.isDebugEnabled()) {
        e.printStackTrace();
      }

      return Optional.empty();
    }
  }

  @SuppressWarnings("unchecked")
  private static <C extends DomainResource> Class<C> cast(Class<?> uncheckedDomainType) {
    return (Class<C>) uncheckedDomainType;
  }

}
