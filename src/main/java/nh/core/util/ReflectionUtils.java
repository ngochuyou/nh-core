package nh.core.util;

import static java.util.Objects.isNull;
import static java.util.Optional.ofNullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import nh.core.domain.DomainResource;
import org.springframework.util.Assert;

@SuppressWarnings("squid:S1610")
public abstract class ReflectionUtils {

  private ReflectionUtils() {
    throw new UnsupportedOperationException();
  }

  /**
   * @see ReflectionUtils#locateAnnotation(Class, Class, Function)
   */
  public static <C, A extends Annotation> A requireAnnotation(
      Class<C> type,
      Class<A> annotationType) {
    return requireAnnotation(type, annotationType, Function.identity());
  }

  /**
   * @see ReflectionUtils#locateAnnotation(Class, Class, Function)
   */
  public static <C, A extends Annotation, V> V requireAnnotation(
      Class<C> type,
      Class<A> annotationType,
      Function<A, V> valueExtractor) {
    return ofNullable(locateAnnotation(type, annotationType, Function.identity()))
        .map(valueExtractor)
        .orElseThrow(() -> new IllegalArgumentException(
            String.format("Unable to locate annotation type %s in type %s", annotationType, type)));
  }

  /**
   * @see ReflectionUtils#locateAnnotation(Class, Class, Function)
   */
  public static <C, A extends Annotation> A locateAnnotation(
      Class<C> type,
      Class<A> annotationType) {
    return locateAnnotation(type, annotationType, Function.identity());
  }

  /**
   * Locates an annotation from within a specific type. Additionally transform the annotation into
   * some value
   *
   * @param type           the owning type
   * @param annotationType the target annotation type
   * @param valueExtractor the function which turns the annotation into some value
   * @param <C>            generic type of the {@link Annotation}
   * @param <A>            which owns the annotation
   * @param <V>            generic type of the value
   * @return the transformed value
   */
  public static <C, A extends Annotation, V> V locateAnnotation(
      Class<C> type,
      Class<A> annotationType,
      Function<A, V> valueExtractor) {
    if (isNull(type)
        || isNull(annotationType)
        || !type.isAnnotationPresent(annotationType)) {
      return null;
    }

    return ofNullable(valueExtractor)
        .map(extractor -> extractor.apply(type.getDeclaredAnnotation(annotationType)))
        .orElse(null);
  }

  public static <C, A extends Annotation> List<Field> locateFields(Class<C> type,
      Class<A> annotationType) {
    if (isNull(type) || Object.class.equals(type) || isNull(annotationType)) {
      return Collections.emptyList();
    }

    return Stream.concat(Stream.of(type.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(annotationType)),
            locateFields(type.getSuperclass(), annotationType).stream())
        .toList();
  }

  public static <C> List<Field> getFields(Class<C> type) {
    return getFields(type, any -> true);
  }

  public static <C> List<Field> getFields(Class<C> type, Predicate<Field> qualifier) {
    if (isNull(type) || Object.class.equals(type)) {
      return Collections.emptyList();
    }

    return Stream.concat(
            Stream.of(type.getDeclaredFields()).filter(qualifier),
            getFields(type.getSuperclass(), qualifier).stream())
        .toList();
  }

  public static <C, A extends Annotation> Field requireField(Class<C> type,
      Class<A> annotationType) {
    return locateField(type, annotationType)
        .orElseThrow(() -> new IllegalArgumentException(
            String.format("At least one element of type %s is required on type %s", annotationType,
                type)));
  }

  public static <A extends Annotation> A requireAnnotation(Field field,
      Class<A> annotationType) {
    return requireAnnotation(field, annotationType, Function.identity());
  }

  public static <V, A extends Annotation> V requireAnnotation(Field field,
      Class<A> annotationType,
      Function<A, V> mapper) {
    return locateAnnotation(field, annotationType)
        .map(mapper)
        .orElseThrow(() -> getMissingOnFieldException(field, annotationType));
  }

  public static <A extends Annotation> Optional<A> locateAnnotation(Field field,
      Class<A> annotationType) {
    return Optional.ofNullable(field.getDeclaredAnnotation(annotationType));
  }

  public static <V, A extends Annotation> Optional<V> locateAnnotation(Field field,
      Class<A> annotationType,
      Function<A, V> mapper) {
    return Optional.ofNullable(field.getDeclaredAnnotation(annotationType))
        .map(mapper);
  }

  private static <A extends Annotation> IllegalArgumentException getMissingOnFieldException(
      Field field, Class<A> annotationType) {
    return new IllegalArgumentException(
        String.format("At least one element of type %s is required on field %s in type %s",
            annotationType,
            field.getName(), field.getDeclaringClass()));
  }

  public static <C, A extends Annotation> Optional<Field> locateField(Class<C> type,
      Class<A> annotationType) {
    if (isNull(type) || Object.class.equals(type) || isNull(annotationType)) {
      return Optional.empty();
    }

    return Optional.ofNullable(Stream.of(type.getDeclaredFields())
        .filter(field -> field.isAnnotationPresent(annotationType))
        .collect(Collectors.collectingAndThen(
            Collectors.toList(),
            list -> singletonFrom(list, type, annotationType)))
        .orElse(locateField(type.getSuperclass(), annotationType)
            .orElse(null)));
  }

  private static <C, R, A extends Annotation> Optional<R> singletonFrom(
      List<R> list,
      Class<C> type,
      Class<A> annotationType) {
    if (list.isEmpty()) {
      return Optional.empty();
    }

    if (list.size() > 1) {
      throw new IllegalArgumentException(
          String.format("Only one element of type %s is allowed on type %s", annotationType, type));
    }

    return Optional.ofNullable(list.get(0));
  }

  public static <D extends DomainResource> List<Field> locateFields(
      Class<D> resourceType,
      Class<? extends Annotation> annotation,
      Function<Class<D>, String> messageProducer) {
    final List<Field> candidates = ReflectionUtils.locateFields(resourceType, annotation);

    Assert.isTrue(!candidates.isEmpty(), messageProducer.apply(resourceType));

    return candidates;
  }

  @SuppressWarnings("rawtypes")
  public static Constructor locateNoArgsConstructor(Class<?> type) {

    try {
      return type.getConstructor();
    } catch (NoSuchMethodException | SecurityException any) {
      throw new IllegalArgumentException(
          String.format(
              "Unable to locateFields %s whose argument is empty in type [%s]",
              Constructor.class.getSimpleName(), type.getName()));
    }
  }
}
