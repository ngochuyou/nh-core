/**
 *
 */
package nh.core.util;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import nh.core.Loggable;
import nh.core.declaration.Declarations;
import nh.core.function.HandledFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

/**
 * @author Ngoc Huy
 */
@SuppressWarnings("squid:S1610")
public abstract class TypeUtils {

  static {
    make(with(String.class, Integer.class).toA(Object::toString).toB(Integer::valueOf));

    make(with(Integer.class, BigInteger.class).toA(BigInteger::intValue).toB(BigInteger::valueOf));
    make(with(int.class, BigInteger.class).toA(BigInteger::intValue).toB(BigInteger::valueOf));
    make(with(Long.class, BigInteger.class).toA(BigInteger::longValue).toB(BigInteger::valueOf));
    make(with(long.class, BigInteger.class).toA(BigInteger::longValue).toB(BigInteger::valueOf));

    make(with(AtomicInteger.class, BigInteger.class)
        .toA(bigInt -> new AtomicInteger(cast(BigInteger.class, Integer.class, bigInt)))
        .toB(atomicInt -> cast(Integer.class, BigInteger.class, atomicInt.get())));
    make(with(AtomicLong.class, BigInteger.class)
        .toA(bigInt -> new AtomicLong(cast(BigInteger.class, Long.class, bigInt)))
        .toB(atomicLong -> cast(Long.class, BigInteger.class, atomicLong.get())));

    try {
      TypeGraph.INSTANCE.close();
    } catch (IOException any) {
      throw new IllegalStateException(any);
    }
  }

  private TypeUtils() {
    throw new UnsupportedOperationException();
  }

  private static <A, B> void make(TypeGraphEntry<A, B> entry) {
    TypeGraph.INSTANCE.add(entry);
  }

  public static <A, B> B cast(Class<A> typeA, Class<B> typeB, A value) throws Exception {
    return TypeGraph.INSTANCE.locate(typeA, typeB).apply(value);
  }

  public static <A> Set<Class<?>> getAlternatives(Class<A> typeA) {
    return TypeGraph.INSTANCE.alternatives.get(typeA);
  }

  private static <A, B> TypeGraphEntry<A, B> with(Class<A> typeA, Class<B> typeB) {
    return new TypeGraphEntry<A, B>().a(typeA).b(typeB);
  }

  public static <T> Deque<Class<? super T>> getClassStack(Class<T> clazz) {
    return asDeque(clazz, Deque::push);
  }

  public static <T> Deque<Class<? super T>> getClassQueue(Class<T> clazz) {
    return asDeque(clazz, Deque::add);
  }

  private static <T> Deque<Class<? super T>> asDeque(
      Class<T> clazz,
      BiConsumer<Deque<Class<? super T>>, Class<? super T>> consumer) {
    final Deque<Class<? super T>> deque = new ArrayDeque<>();
    Class<? super T> superClass = clazz;

    while (superClass != null && !superClass.equals(Object.class)) {
      consumer.accept(deque, superClass);
      superClass = superClass.getSuperclass();
    }

    return deque;
  }

  public static boolean isImplementedFrom(Class<?> type, Class<?> superType) {

    for (final Class<?> i : type.getInterfaces()) {

      if (i.equals(superType)) {
        return true;
      }
    }

    return false;
  }

  public static Type getGenericType(Field field) {
    return ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
  }

  public static <T> T constructFromNoArgs(Class<T> clazz)
      throws InstantiationException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
      SecurityException {
    return clazz.getConstructor().newInstance();
  }

  public static class TypeGraph implements Closeable, Loggable {

    private static final TypeGraph INSTANCE = new TypeGraph();

    private static final int PRIME = 37;

    private static final BiFunction<Class<?>, Integer, Integer> HASH_CODE_GENERATOR = (
        anyType,
        salt) -> (PRIME * salt) + anyType.hashCode();
    private final AtomicBoolean isClosed = new AtomicBoolean(false);
    private final Map<Class<?>, Set<Class<?>>> alternatives = new HashMap<>();
    private Map<Integer, Map<Integer, HandledFunction<?, ?, Exception>>> convertersMap =
        new HashMap<>();

    private TypeGraph() {
    }

    private <A, B> int[] hash(Class<A> typeA, Class<B> typeB) {
      int aHash = HASH_CODE_GENERATOR.apply(typeA, 1);
      int bHash = HASH_CODE_GENERATOR.apply(typeB, aHash);

      return new int[]{
          aHash, bHash
      };
    }

    @SuppressWarnings("unchecked")
    private <A, B> HandledFunction<A, B, Exception> locate(Class<A> typeA, Class<B> typeB) {
      int[] hashPair = hash(typeA, typeB);

      if (!convertersMap.containsKey(hashPair[0])
          || !convertersMap.get(hashPair[0]).containsKey(hashPair[1])) {
        throw new IllegalArgumentException(
            String.format("Unable to find any converter that supports [%s => %s]", typeA, typeB));
      }

      return (HandledFunction<A, B, Exception>) convertersMap.get(hashPair[0])
          .get(hashPair[1]);
    }

    private <A, B> void add(TypeGraphEntry<A, B> entry) {
      Assert.isTrue(!isClosed.get(), "Access to this resource was closed");
      int[] hashPair = hash(entry.typeA, entry.typeB);

      addBy(hashPair[0], hashPair[1], entry.toBFunction);
      addBy(hashPair[1], hashPair[0], entry.toAFunction);
      addAlternatives(entry.typeA, entry.typeB);
      addAlternatives(entry.typeB, entry.typeA);
    }

    private <A, B> void addAlternatives(Class<A> classOne, Class<B> classTwo) {

      alternatives.computeIfAbsent(classOne, key -> new HashSet<>(List.of(classTwo)));
      alternatives.get(classOne).add(classTwo);
    }

    private void addBy(int aHash, int bHash, HandledFunction<?, ?, Exception> converter) {

      if (convertersMap.containsKey(aHash)) {
        convertersMap.get(aHash).put(bHash, converter);
        return;
      }

      Map<Integer, HandledFunction<?, ?, Exception>> descriptorsByA = new HashMap<>();

      descriptorsByA.put(bHash, converter);
      convertersMap.put(aHash, descriptorsByA);
    }

    @Override
    public synchronized void close() throws IOException {
      Logger logger = LoggerFactory.getLogger(TypeUtils.TypeGraph.class);

      isClosed.set(true);
      logger.trace("Closing access");

      try {
        convertersMap = Declarations.of(convertersMap).map(Map::entrySet).map(Set::stream)
            .map(
                stream -> stream
                    .map(
                        entry -> Map
                            .entry(entry.getKey(), Collections.unmodifiableMap(entry.getValue())))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)))
            .map(Collections::unmodifiableMap).get();
      } catch (Exception any) {
        throw new IOException(any);
      }
    }

  }

  private static class TypeGraphEntry<A, B> {

    private Class<A> typeA;
    private Class<B> typeB;
    private HandledFunction<A, B, Exception> toBFunction;
    private HandledFunction<B, A, Exception> toAFunction;

    private TypeGraphEntry() {
    }

    public TypeGraphEntry<A, B> a(Class<A> typeA) {
      this.typeA = typeA;
      return this;
    }

    public TypeGraphEntry<A, B> b(Class<B> typeB) {
      this.typeB = typeB;
      return this;
    }

    public TypeGraphEntry<A, B> toB(HandledFunction<A, B, Exception> toBFunction) {
      this.toBFunction = toBFunction;
      return this;
    }

    public TypeGraphEntry<A, B> toA(HandledFunction<B, A, Exception> toAFunction) {
      this.toAFunction = toAFunction;
      return this;
    }

  }

}
