package nh.core.util;

public abstract class RegexUtils {

  private RegexUtils() {
    throw new UnsupportedOperationException();
  }

  public static final String VIETNAMESE_CHARACTERS = ""
      + "ÁáÀàẢảÃãẠạĂăẮắẰằẲẳẴẵẶặÂâẤấẦầẨẩẪẫẬậ"
      + "Đđ"
      + "ÉéÈèẺẻẼẽẸẹÊêỂểẾếỀềỄễỆệ"
      + "ÍíÌìỊịỈỉĨĩỊị"
      + "ÓóÒòỎỏÕõỌọÔôỐốỒồỔổỖỗỘộƠơỚớỜờỞởỠỡỢợ"
      + "ÚùÙùỦủŨũỤụƯưỨứỪừỬửỮữỰự"
      + "ÝýỲỳỶỷỸỹỴỵ";

}
