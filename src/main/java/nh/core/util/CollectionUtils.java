/**
 *
 */
package nh.core.util;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author Ngoc Huy
 */
@SuppressWarnings("squid:S1610")
public abstract class CollectionUtils {

  private CollectionUtils() {
    throw new UnsupportedOperationException();
  }

  public static <K, V> Collector<Map.Entry<K, V>, ?, Map<K, V>> toMap() {
    return Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue);
  }

  public static <K, V> Collector<Map.Entry<K, V>, ?, Map<V, K>> mirror() {
    return Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey);
  }

  public static <T, C extends Collection<T>> C join(
      Collection<Collection<T>> elements,
      Collector<T, ?, C> collector) {
    return elements.stream().flatMap(Collection::stream).collect(collector);
  }

}
