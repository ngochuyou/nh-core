/**
 *
 */
package nh.core.function;

/**
 * @author Ngoc Huy
 *
 */
@FunctionalInterface
public interface HandledSupplier<RETURN, EXCEPTION extends Exception> {

  RETURN get() throws EXCEPTION;

}
