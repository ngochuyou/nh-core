/**
 *
 */
package nh.core.function;

/**
 * @author Ngoc Huy
 *
 */
@FunctionalInterface
public interface HandledBiFunction<FIRST, SECOND, RETURN, EXCEPTION extends Exception> {

  RETURN apply(FIRST fisrt, SECOND second) throws EXCEPTION;

}
