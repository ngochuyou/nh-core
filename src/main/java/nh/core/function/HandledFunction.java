/**
 *
 */
package nh.core.function;

/**
 * @author Ngoc Huy
 *
 */
@FunctionalInterface
public interface HandledFunction<FIRST, RETURN, EXCEPTION extends Exception> {

  static <F, E extends Exception> HandledFunction<F, F, E> identity() {
    return first -> first;
  }

  RETURN apply(FIRST input) throws EXCEPTION;

}
