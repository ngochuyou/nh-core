/**
 *
 */
package nh.core.function;

/**
 * @author Ngoc Huy
 *
 */
@FunctionalInterface
public interface TriFunction<FIRST, SECOND, THIRD, RETURN> {

  RETURN apply(FIRST first, SECOND second, THIRD third);

}
