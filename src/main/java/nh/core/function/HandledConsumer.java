/**
 *
 */
package nh.core.function;

/**
 * @author Ngoc Huy
 *
 */
@FunctionalInterface
public interface HandledConsumer<FIRST, EXCEPTION extends Exception> {

  void accept(FIRST input) throws EXCEPTION;

}
