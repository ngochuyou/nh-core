/**
 *
 */
package nh.core.function;

/**
 * @author Ngoc Huy
 *
 */
@FunctionalInterface
public interface TriConsumer<FIRST, SECOND, THIRD> {

  void accept(FIRST first, SECOND second, THIRD third);

}
