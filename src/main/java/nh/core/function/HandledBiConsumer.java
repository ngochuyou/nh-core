/**
 *
 */
package nh.core.function;

/**
 * @author Ngoc Huy
 *
 */
@FunctionalInterface
public interface HandledBiConsumer<FIRST, SECOND, EXCEPTION extends Exception> {

  void accept(FIRST first, SECOND second) throws EXCEPTION;

}
