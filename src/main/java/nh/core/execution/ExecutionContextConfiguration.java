package nh.core.execution;

import jakarta.servlet.http.HttpServletRequest;
import nh.core.execution.impl.ExecutionContextImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.annotation.RequestScope;

public class ExecutionContextConfiguration {

  @Bean
  @RequestScope
  public ExecutionContext executionContext(HttpServletRequest request) {
    return new ExecutionContextImpl(request);
  }

}
