package nh.core.execution.impl;

import static nh.core.constants.Authentications.ANONYMOUS;

import jakarta.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import nh.core.constants.AcceptedHeaders;
import nh.core.execution.ExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class ExecutionContextImpl implements ExecutionContext {

  private static final Logger logger = LoggerFactory.getLogger(ExecutionContextImpl.class);

  private final Locale locale;
  private final Authentication authentication;

  public ExecutionContextImpl(HttpServletRequest request) {
    logger.debug("Resolving request resources");

    locale = Locale.forLanguageTag(request.getHeader(AcceptedHeaders.LANG));
    authentication = resolveAuthentication();
  }

  private static Authentication resolveAuthentication() {
    if (isPrincipalAnonymous()) {
      return ANONYMOUS;
    }

    return SecurityContextHolder.getContext().getAuthentication();
  }

  private static boolean isPrincipalAnonymous() {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    return Objects.isNull(authentication) || authentication instanceof AnonymousAuthenticationToken;
  }

  @Override
  public Locale getLocale() {
    return locale;
  }

  @Override
  public List<GrantedAuthority> getAuthorities() {
    return new ArrayList<>(authentication.getAuthorities());
  }
}
