package nh.core.execution;

import java.util.List;
import java.util.Locale;
import org.springframework.security.core.GrantedAuthority;

public interface ExecutionContext {

  Locale getLocale();

  List<GrantedAuthority> getAuthorities();

}
