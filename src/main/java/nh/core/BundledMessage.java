package nh.core;

import org.apache.commons.lang3.StringUtils;

public interface BundledMessage {

  Object[] EMPTY_ARGS = new Object[0];

  default String getCode() {
    return null;
  }

  default Object[] getArguments() {
    return EMPTY_ARGS;
  }

  default String getDefault() {
    return StringUtils.EMPTY;
  }

}
