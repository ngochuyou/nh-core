package nh.core.reflect.impl;

import java.lang.reflect.Method;

/**
 * @author Ngoc Huy
 */
public interface MethodAdvisorChain {

  <T> Method adviceGetter(Class<T> owningClass, String fieldName);

  <T> Method adviceSetter(Class<T> owningClass, String fieldName);

}
