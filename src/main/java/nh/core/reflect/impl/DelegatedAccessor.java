package nh.core.reflect.impl;

import nh.core.reflect.Getter;
import nh.core.reflect.Setter;

/**
 * @author Ngoc Huy
 */
public class DelegatedAccessor extends AbstractAccessor {

  public DelegatedAccessor(Getter getter, Setter setter) {
    super(getter, setter);
  }

}
