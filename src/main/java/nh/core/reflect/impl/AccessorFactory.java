package nh.core.reflect.impl;

import nh.core.reflect.Accessor;
import nh.core.reflect.Getter;
import nh.core.reflect.Setter;

/**
 * @author Ngoc Huy
 */
public interface AccessorFactory {

  static <T> Accessor basic(Class<T> type, String propertyName) {
    return new BasicAccessor(propertyName, type);
  }

  static Accessor delegate(Getter getter, Setter setter) {
    return new DelegatedAccessor(getter, setter);
  }

}
