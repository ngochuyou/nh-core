package nh.core.reflect.impl;

import java.lang.reflect.Member;
import java.lang.reflect.Method;
import nh.core.reflect.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ngoc Huy
 */
class SetterImpl implements Setter {

  private static final Logger logger = LoggerFactory.getLogger(SetterImpl.class);

  private final boolean isPrimitive;
  private final Method setterMethod;

  SetterImpl(boolean isPrimitive, Method invoker) {
    this.isPrimitive = isPrimitive;
    this.setterMethod = invoker;
  }

  @Override
  public void set(Object source, Object val) {

    if (val == null && isPrimitive) {
      throw new IllegalArgumentException("A null value was used for a primitive property");
    }

    try {
      setterMethod.invoke(source, val);
    } catch (Exception any) {
      logger.error(any.getMessage());
    }
  }

  @Override
  public Member getMember() {
    return setterMethod;
  }

  @Override
  public String toString() {
    return String.format(
        "%s(%s)", setterMethod.getName(),
        setterMethod.getReturnType().getName());
  }

}