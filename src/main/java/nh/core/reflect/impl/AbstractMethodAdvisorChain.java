package nh.core.reflect.impl;

import java.lang.reflect.Method;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ngoc Huy
 */
public abstract class AbstractMethodAdvisorChain implements MethodAdvisorChain {

  private static final Logger logger = LoggerFactory.getLogger(AbstractMethodAdvisorChain.class);

  protected final MethodAdvisorChain next;

  protected AbstractMethodAdvisorChain(MethodAdvisorChain next) {
    this.next = next;
  }

  protected static <T> void logException(Class<T> owningClass, String fieldName, Exception ex) {

    if (logger.isDebugEnabled()) {
      logger.debug(
          String.format(
              "Failed to locate method named [%s] for property [%s]. Exception message %s",
              fieldName,
              owningClass, ex.getMessage()));
    }
  }

  @Override
  public <T> Method adviceGetter(Class<T> owningClass, String fieldName) {
    return Optional.ofNullable(next)
        .map(nextAdvisor -> nextAdvisor.adviceGetter(owningClass, fieldName)).orElse(null);
  }

  @Override
  public <T> Method adviceSetter(Class<T> owningClass, String fieldName) {
    return Optional.ofNullable(next)
        .map(nextAdvisor -> nextAdvisor.adviceSetter(owningClass, fieldName)).orElse(null);
  }

}
