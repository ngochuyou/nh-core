package nh.core.reflect.impl;

import nh.core.reflect.Accessor;
import nh.core.reflect.Getter;
import nh.core.reflect.Setter;

/**
 * @author Ngoc Huy
 */
abstract class AbstractAccessor implements Accessor {

  protected final Getter getter;
  protected final Setter setter;

  protected AbstractAccessor(Getter getter, Setter setter) {
    this.getter = getter;
    this.setter = setter;
  }

  @Override
  public Object get(Object source) {
    return getter.get(source);
  }

  @Override
  public void set(Object source, Object val) {
    setter.set(source, val);
  }

  @Override
  public String toString() {
    return String
        .format("%s(getter=%s, setter=%s)", this.getClass().getSimpleName(), getter, setter);
  }

  @Override
  public Getter getGetter() {
    return getter;
  }

  @Override
  public Setter getSetter() {
    return setter;
  }

}
