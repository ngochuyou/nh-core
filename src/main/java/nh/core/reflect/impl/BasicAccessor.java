package nh.core.reflect.impl;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import nh.core.declaration.Declarations;
import nh.core.function.HandledSupplier;
import nh.core.reflect.Getter;
import nh.core.reflect.Setter;
import nh.core.util.StringUtils;
import nh.core.util.TypeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ngoc Huy
 */
public class BasicAccessor extends AbstractAccessor {

  private static final Logger logger = LoggerFactory.getLogger(BasicAccessor.class);

  private static final MethodAdvisorChain METHOD_ADVISOR_CHAIN =
      new StandardMethodAdvisor(new IsBooleanMethodAdvisor(new IsBooleanFieldAdvisor(null)));

  <T> BasicAccessor(String propName, Class<T> owningType) {
    super(locateGetter(propName, owningType), locateSetter(propName, owningType));
  }

  private static <T> Getter locateGetter(String propName, Class<T> owningType) {
    return new Getter() {

      private final Method getterMethod = Optional
          .ofNullable(METHOD_ADVISOR_CHAIN.adviceGetter(owningType, propName))
          .orElseThrow(
              () -> new IllegalArgumentException(
                  String.format(
                      "Unable to locate %s for field name [%s] in type [%s]",
                      Getter.class.getSimpleName(), propName, owningType.getName())));

      @Override
      public Member getMember() {
        return getterMethod;
      }

      @Override
      public Class<?> getReturnedType() {
        return getterMethod.getReturnType();
      }

      @Override
      public Object get(Object source) {

        try {
          return getterMethod.invoke(source);
        } catch (IllegalAccessException | IllegalArgumentException
                 | InvocationTargetException any) {
          logger.error(any.getMessage());
          return null;
        }
      }

      @Override
      public String toString() {
        return String.format("%s()", getterMethod.getName());
      }

    };
  }

  private static <T> Setter locateSetter(String propName, Class<T> owningType) {
    final Method setterMethod =
        Optional.ofNullable(METHOD_ADVISOR_CHAIN.adviceSetter(owningType, propName))
            .orElseThrow(
                () -> new IllegalArgumentException(
                    String.format(
                        "Unable to locate %s for field name [%s] in type [%s]",
                        Setter.class.getSimpleName(), propName, owningType.getName())));

    return new SetterImpl(setterMethod.getParameterTypes()[0].isPrimitive(), setterMethod);
  }

  private static class StandardMethodAdvisor extends AbstractMethodAdvisorChain {

    private StandardMethodAdvisor(MethodAdvisorChain next) {
      super(next);
    }

    @Override
    public <T> Method adviceGetter(Class<T> owningClass, String fieldName) {

      try {
        return Declarations.of("get", fieldName)
            .map(StringUtils::combineIntoCamel)
            .tryMap(owningClass::getDeclaredMethod)
            .get();
      } catch (RuntimeException | NoSuchMethodException any) {
        logException(owningClass, fieldName, any);
        return super.adviceGetter(owningClass, fieldName);
      }
    }

    @SuppressWarnings("squid:S1612")
    @Override
    public <T> Method adviceSetter(Class<T> owningClass, String fieldName) {

      try {
        return Declarations.of("set", fieldName)
            .map(StringUtils::combineIntoCamel)
            .second(owningClass.getDeclaredField(fieldName).getType())
            .tryMap((setterName, paramType) -> owningClass.getDeclaredMethod(setterName, paramType))
            .get();
      } catch (RuntimeException | NoSuchMethodException | NoSuchFieldException any) {
        logException(owningClass, fieldName, any);
        return super.adviceSetter(owningClass, fieldName);
      }
    }

  }

  private abstract static class AbstractIsBooleanMemberAdvisor extends AbstractMethodAdvisorChain {

    protected AbstractIsBooleanMemberAdvisor(MethodAdvisorChain next) {
      super(next);
    }

    protected <T> boolean isFieldBoolean(Class<T> owningClass, String fieldName)
        throws NoSuchFieldException, SecurityException {
      return isFieldBoolean(() -> owningClass.getDeclaredField(fieldName));
    }

    protected <E extends Exception> boolean isFieldBoolean(HandledSupplier<Field, E> fieldSupplier)
        throws E {
      final Class<?> fieldType = fieldSupplier.get().getType();

      return Stream.of(Boolean.class, boolean.class)
          .map(type -> type.isAssignableFrom(fieldType))
          .reduce(Boolean::logicalOr)
          .orElse(false);
    }

  }

  private static class IsBooleanMethodAdvisor extends AbstractIsBooleanMemberAdvisor {

    private static final Logger logger = LoggerFactory.getLogger(IsBooleanMethodAdvisor.class);

    private IsBooleanMethodAdvisor(MethodAdvisorChain next) {
      super(next);
    }

    @Override
    public <T> Method adviceGetter(Class<T> owningClass, String fieldName) {

      try {

        if (!isFieldBoolean(owningClass, fieldName)) {
          return super.adviceGetter(owningClass, fieldName);
        }

        return Declarations.of(fieldName)
            .map(this::produceNameWithLeadingIs)
            .tryMap(owningClass::getDeclaredMethod)
            .get();
      } catch (NoSuchFieldException | SecurityException | NoSuchMethodException ex) {
        logException(owningClass, fieldName, ex);
        return super.adviceGetter(owningClass, fieldName);
      }
    }

    private String produceNameWithLeadingIs(String fieldName) {
      return StringUtils.combineIntoCamel("is", fieldName);
    }

    @Override
    public <T> Method adviceSetter(Class<T> owningClass, String fieldName) {
      final Class<?> parameterType;

      try {
        final Field field = owningClass.getDeclaredField(fieldName);

        if (!isFieldBoolean(() -> field)) {
          return super.adviceSetter(owningClass, fieldName);
        }

        parameterType = field.getType();
      } catch (RuntimeException | NoSuchFieldException ex) {
        logException(owningClass, fieldName, ex);
        return super.adviceSetter(owningClass, fieldName);
      }

      final String setterName =
          StringUtils.combineIntoCamel("set", produceNameWithLeadingIs(fieldName));

      try {
        return owningClass.getDeclaredMethod(setterName, parameterType);
      } catch (SecurityException se) {
        logException(owningClass, fieldName, se);
        return super.adviceSetter(owningClass, fieldName);
      } catch (NoSuchMethodException nsme) {
        final Set<Class<?>> alternativeTypes = TypeUtils.getAlternatives(parameterType);

        if (alternativeTypes == null || alternativeTypes.isEmpty()) {
          logException(owningClass, fieldName, nsme);
          return super.adviceSetter(owningClass, fieldName);
        }

        for (Class<?> alternative : alternativeTypes) {

          try {
            return owningClass.getDeclaredMethod(setterName, alternative);
          } catch (Exception any) {
            logger.error(any.getMessage());
          }
        }

        return super.adviceSetter(owningClass, fieldName);
      }
    }

  }

  private static class IsBooleanFieldAdvisor extends AbstractIsBooleanMemberAdvisor {

    private static final Pattern BOOLEAN_FIELD_NAME_PATTERN =
        Pattern.compile("^is([A-Z][\\p{L}\\p{N}_$]*)$");

    private IsBooleanFieldAdvisor(MethodAdvisorChain next) {
      super(next);
    }

    @Override
    public <T> Method adviceGetter(Class<T> owningClass, String fieldName) {

      try {

        if (!isFieldBoolean(owningClass, fieldName)) {
          return super.adviceGetter(owningClass, fieldName);
        }

        return owningClass.getDeclaredMethod(fieldName);
      } catch (NoSuchFieldException | SecurityException | NoSuchMethodException ex) {
        logException(owningClass, fieldName, ex);
        return super.adviceGetter(owningClass, fieldName);
      }
    }

    @SuppressWarnings("squid:S1612")
    @Override
    public <T> Method adviceSetter(Class<T> owningClass, String fieldName) {

      try {
        final Field field = owningClass.getDeclaredField(fieldName);

        if (!isFieldBoolean(() -> field)) {
          return super.adviceSetter(owningClass, fieldName);
        }

        final Matcher matcher = BOOLEAN_FIELD_NAME_PATTERN.matcher(fieldName);

        if (!matcher.matches() || matcher.groupCount() != 1) {
          return super.adviceSetter(owningClass, fieldName);
        }

        return Declarations.of(matcher.group(1))
            .map(this::produceLeadingSetMethodName)
            .second(field.getType())
            .tryMap((methodName, paramType) -> owningClass.getDeclaredMethod(methodName, paramType))
            .get();
      } catch (NoSuchFieldException | SecurityException | NoSuchMethodException ex) {
        logException(owningClass, fieldName, ex);
        return super.adviceSetter(owningClass, fieldName);
      }
    }

    private String produceLeadingSetMethodName(String setterMethodTargetName) {
      return String.format("set%s", setterMethodTargetName);
    }

  }

}
