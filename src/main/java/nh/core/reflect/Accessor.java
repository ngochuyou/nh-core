package nh.core.reflect;

/**
 * @author Ngoc Huy
 */
public interface Accessor {

  Object get(Object source);

  void set(Object source, Object val);

  Getter getGetter();

  Setter getSetter();

}
