/**
 *
 */
package nh.core.reflect;

/**
 * @author Ngoc Huy
 */
public interface Getter extends Access {

  Object get(Object source);

  Class<?> getReturnedType();

}
