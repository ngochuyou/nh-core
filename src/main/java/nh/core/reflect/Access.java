/**
 *
 */
package nh.core.reflect;

import java.lang.reflect.Member;

/**
 * @author Ngoc Huy
 */
public interface Access {

  Member getMember();

}
