/**
 *
 */
package nh.core.reflect;

/**
 * @author Ngoc Huy
 */
public interface Setter extends Access {

  void set(Object source, Object val);

}
