package nh.core.config;

import nh.core.constants.Configurations;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.impl.DomainResourceContextImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

public class DomainResourceContextConfiguration {

  @Bean
  public DomainResourceContext domainResourceContext(
      ApplicationContext applicationContext,
      @Value(Configurations.CONTEXT_BASE_PACKAGE_INJECTION) String contextPackage)
      throws Exception {
    return new DomainResourceContextImpl(applicationContext, contextPackage);
  }

}
