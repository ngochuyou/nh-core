package nh.core.config;

import nh.core.constants.Configurations;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.builder.DomainBuilderFactory;
import nh.core.domain.builder.impl.DomainBuilderFactoryImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.Bean;

public class DomainBuildingConfiguration {

  @Bean
  public DomainBuilderFactory domainBuilderFactory(
      AutowireCapableBeanFactory beanFactory,
      DomainResourceContext domainContext,
      @Value(Configurations.CONTEXT_BASE_PACKAGE_INJECTION) String basePackage) {
    return new DomainBuilderFactoryImpl(beanFactory,
        domainContext,
        basePackage);
  }

}
