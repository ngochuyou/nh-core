package nh.core.config;

import nh.core.domain.DomainResourceContext;
import nh.core.domain.repository.GenericRepository;
import nh.core.domain.repository.impl.GenericRepositoryConfigurationProperties;
import nh.core.domain.repository.impl.GenericRepositoryImpl;
import org.springframework.context.annotation.Bean;

public class GenericRepositoryConfiguration {

  @Bean
  public GenericRepository genericRepository(DomainResourceContext resourceContext,
      GenericRepositoryConfigurationProperties configurationProperties) {
    return new GenericRepositoryImpl(resourceContext, configurationProperties);
  }

}
