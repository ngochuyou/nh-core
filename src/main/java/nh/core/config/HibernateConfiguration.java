/**
 *
 */
package nh.core.config;

import java.util.Properties;
import java.util.stream.Stream;
import javax.sql.DataSource;
import nh.core.constants.Configurations;
import nh.core.constants.Hibernates;
import nh.core.declaration.Declarations;
import nh.core.util.SpringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.boot.model.naming.CamelCaseToUnderscoresNamingStrategy;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Ngoc Huy
 */
@EnableTransactionManagement
public class HibernateConfiguration {

  @Bean(Hibernates.ROOT_SESSION_FACTORY)
  public FactoryBean<SessionFactory> sessionFactory(DataSource dataSource, Environment env) {
    final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();

    sessionFactory.setDataSource(dataSource);
    sessionFactory
        .setPackagesToScan(
            env.getRequiredProperty(Configurations.CONTEXT_ENTITY_PACKAGE, String[].class));
    // snake_case for naming
    sessionFactory.setPhysicalNamingStrategy(new CamelCaseToUnderscoresNamingStrategy());

    final Properties sessionFactoryProperties = new Properties();

    Stream.of(
            AvailableSettings.DIALECT,
            AvailableSettings.SHOW_SQL,
            AvailableSettings.FORMAT_SQL,
            AvailableSettings.HBM2DDL_AUTO,
            AvailableSettings.STATEMENT_BATCH_SIZE,
            AvailableSettings.ORDER_INSERTS,
            AvailableSettings.ORDER_UPDATES)
        .map(
            key -> Declarations.of(
                key,
                env.getRequiredProperty(
                    SpringUtils.resolvePluralEnvKey("spring.jpa.properties", key))))
        .forEach(declaration -> declaration.consume(sessionFactoryProperties::put));

    sessionFactory.setHibernateProperties(sessionFactoryProperties);

    return sessionFactory;
  }

  @Bean
  public TransactionManager transactionManager(SessionFactory sessionFactory) {
    return new HibernateTransactionManager(sessionFactory);
  }

}
