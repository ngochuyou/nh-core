package nh.core.config;

import nh.core.domain.security.DomainSecurityContext;
import nh.core.domain.security.impl.RedisDomainSecurityContext;
import nh.core.domain.security.impl.RedisSecurityContextConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

public class DomainSecurityContextConfiguration {

  @Bean
  public DomainSecurityContext domainSecurityContext(
      ApplicationContext applicationContext,
      RedisSecurityContextConfigurationProperties redisSecurityContextConfigurationProperties)
      throws ClassNotFoundException {
    return new RedisDomainSecurityContext(applicationContext,
        redisSecurityContextConfigurationProperties);
  }

}
