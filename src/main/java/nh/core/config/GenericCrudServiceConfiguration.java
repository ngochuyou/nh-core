package nh.core.config;

import java.util.Map;
import nh.core.constants.Configurations;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.builder.DomainBuilderFactory;
import nh.core.domain.crud.GenericCrudServiceImplementor;
import nh.core.domain.crud.event.impl.EventListenerGroups;
import nh.core.domain.crud.event.impl.EventListenersResolver;
import nh.core.domain.crud.event.persist.impl.EncryptedIdentifiableDomainPostPersistEventListener;
import nh.core.domain.crud.event.persist.impl.EncryptedIdentifiableDomainPrePersistEventListener;
import nh.core.domain.crud.impl.GenericCrudServiceImpl;
import nh.core.domain.crud.impl.SelectionProvidersFactory;
import nh.core.domain.repository.GenericRepository;
import nh.core.domain.rest.impl.RestQueryComposer;
import nh.core.domain.security.SecurityEntryManager;
import nh.core.domain.validation.DomainValidatorFactory;
import nh.core.exception.DomainExceptionTranslator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.Bean;

public class GenericCrudServiceConfiguration {

  @Bean
  public GenericCrudServiceImplementor<Map<String, Object>> genericCRUDService(
      GenericRepository genericRepository,
      DomainBuilderFactory builderFactory,
      DomainValidatorFactory validatorFactory,
      DomainResourceContext resourceContext,
      EventListenerGroups eventListenerGroups,
      DomainExceptionTranslator domainExceptionTranslator,
      SecurityEntryManager securityManager,
      RestQueryComposer restQueryComposer,
      SelectionProvidersFactory selectionProvidersFactory) {
    return new GenericCrudServiceImpl(genericRepository, builderFactory, validatorFactory,
        resourceContext,
        eventListenerGroups,
        securityManager,
        restQueryComposer,
        selectionProvidersFactory,
        domainExceptionTranslator);
  }

  @Bean
  public EncryptedIdentifiableDomainPostPersistEventListener encryptedIdentifiableDomainPostPersistEventListener() {
    return new EncryptedIdentifiableDomainPostPersistEventListener();
  }

  @Bean
  public EncryptedIdentifiableDomainPrePersistEventListener encryptedIdentifiableDomainPrePersistEventListener() {
    return new EncryptedIdentifiableDomainPrePersistEventListener();
  }

  @Bean
  public EventListenersResolver eventListenerResolver(DomainResourceContext domainContext,
      AutowireCapableBeanFactory beanFactory,
      EncryptedIdentifiableDomainPostPersistEventListener encryptedIdentifiableDomainPostPersistEventListener,
      EncryptedIdentifiableDomainPrePersistEventListener encryptedIdentifiableDomainPrePersistEventListener) {
    return new EventListenersResolver(domainContext, beanFactory,
        encryptedIdentifiableDomainPostPersistEventListener,
        encryptedIdentifiableDomainPrePersistEventListener);
  }

  @Bean
  public EventListenerGroups eventListenerGroups(EventListenersResolver eventListenerResolver) {
    return new EventListenerGroups(eventListenerResolver);
  }

  @Bean
  public RestQueryComposer restQueryComposer(DomainResourceContext domainContext,
      SecurityEntryManager securityManager,
      @Value(Configurations.CONTEXT_BASE_PACKAGE_INJECTION) String contextPackage)
      throws NoSuchFieldException, ClassNotFoundException {
    return new RestQueryComposer(domainContext, securityManager, contextPackage);
  }

  @Bean
  public SelectionProvidersFactory selectionProvidersFactory(DomainResourceContext domainContext) {
    return new SelectionProvidersFactory(domainContext);
  }

}
