package nh.core.config;

import nh.core.constants.Configurations;
import nh.core.domain.DomainResourceContext;
import nh.core.domain.validation.DomainValidatorFactory;
import nh.core.domain.validation.impl.DomainValidatorFactoryImpl;
import nh.core.domain.validation.impl.named.NamedDomainValidatorFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.Bean;

public class DomainValidationConfiguration {

  @Bean
  public DomainValidatorFactory domainValidatorFactory(
      AutowireCapableBeanFactory beanFactory,
      DomainResourceContext domainContext,
      @Value(Configurations.CONTEXT_BASE_PACKAGE_INJECTION) String basePackage) {
    return new DomainValidatorFactoryImpl(beanFactory, domainContext, basePackage);
  }

  @Bean
  public NamedDomainValidatorFactory namedDomainValidatorFactory(
      DomainResourceContext domainContext) {
    return new NamedDomainValidatorFactory(domainContext);
  }

}
